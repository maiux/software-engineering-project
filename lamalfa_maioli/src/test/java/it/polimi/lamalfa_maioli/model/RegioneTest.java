package it.polimi.lamalfa_maioli.model;

import static org.junit.Assert.*;
import it.polimi.lamalfa_maioli.controller.exceptions.MapSyntaxException;
import it.polimi.lamalfa_maioli.model.Agnello;
import it.polimi.lamalfa_maioli.model.Giocatore;
import it.polimi.lamalfa_maioli.model.Regione;
import it.polimi.lamalfa_maioli.model.Terreno;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class RegioneTest {
	private int id;
	private Terreno type;
	private int pecore = 1;
	private int arieti = 1;
	private List<Agnello> agnelli = new ArrayList<>();
	private boolean pecoraNera = false;
	private boolean lupo = false;
	private Regione regione;
	
	@Before
	public void setUpClass() {
		id = (int)(Math.random()*20+1);
		type = Terreno.getRandom();
		try {
			regione = new Regione(id, type.getId());
		} catch (MapSyntaxException e) {
			fail("Errore nell'inizializzazione!");
		}
		Agnello tmp = new Agnello(1, new Giocatore(1, Colore.getRandom()), regione);
		regione.getAgnelli().add(tmp);
		agnelli.add(tmp);
		tmp = new Agnello(2, new Giocatore(1, Colore.getRandom()), regione);
		regione.getAgnelli().add(tmp);
		agnelli.add(tmp);
	}

	@Test
	public void testRegione() {
		for(int i = 0; i < Terreno.values().length; i++) {
			type = Terreno.values()[i];
			try {
				regione = new Regione(id, type.getId());
			} catch (MapSyntaxException e) {
				fail("In questo range la regione deve essere definita!");
			}
			Terreno actual = regione.getType();
			assertEquals(type, actual);
			int actid = regione.getId();
			assertEquals(id, actid);
		}
		try {
			regione = new Regione(id, Terreno.values().length+1);
		} catch (MapSyntaxException e) {
			return;
		}
		fail("Regione definita dove non dovrebbe!");
	}

	@Test
	public final void testToString() {
		String expected = "Regione("+id+"): "+type;
		String actual = regione.toString();
		assertEquals(expected, actual);
	}
	
	@Test
	public final void testGetClassString() {
		String expected = "Regione";
		String actual = regione.getClassString();
		assertEquals(expected, actual);
	}

	@Test
	public void testGetSetPecore() {
		int actual = regione.getPecore();
		assertEquals(pecore, actual);
		pecore = (int)(Math.random()*10);
		regione.setPecore(pecore);
		actual = regione.getPecore();
		assertEquals(pecore, actual);
	}

	@Test
	public void testSetHasPecoraNera() {
		boolean actual = regione.hasPecoraNera();
		assertEquals(pecoraNera, actual);
		pecoraNera = true;
		regione.setPecoraNera(pecoraNera);
		actual = regione.hasPecoraNera();
		assertEquals(pecoraNera, actual);
		pecoraNera = false;
		regione.setPecoraNera(pecoraNera);
		actual = regione.hasPecoraNera();
		assertEquals(pecoraNera, actual);
	}

	@Test
	public void testSetHasLupo() {
		boolean actual = regione.hasLupo();
		assertEquals(lupo, actual);
		lupo = true;
		regione.setLupo(lupo);
		actual = regione.hasLupo();
		assertEquals(lupo, actual);
		lupo = false;
		regione.setLupo(lupo);
		actual = regione.hasLupo();
		assertEquals(lupo, actual);
	}

	@Test
	public void testGetSetArieti() {
		int actual = regione.getArieti();
		assertEquals(arieti, actual);
		arieti = (int)(Math.random()*10);
		regione.setArieti(arieti);
		actual = regione.getArieti();
		assertEquals(arieti, actual);
	}

	@Test
	public void testGetAgnelli() {
		List<Agnello> actual = regione.getAgnelli();
		assertTrue(agnelli.equals(actual));
	}

}
