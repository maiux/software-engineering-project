package it.polimi.lamalfa_maioli.model;

import static org.junit.Assert.*;
import it.polimi.lamalfa_maioli.model.Casella;
import it.polimi.lamalfa_maioli.model.Giocatore;
import it.polimi.lamalfa_maioli.model.Pastore;

import org.junit.Before;
import org.junit.Test;

public class CasellaTest {
	private Pastore pastore;
	private Casella casella;
	private int id;
	private int value;
	
	@Before
	public void setUpClass() {
		Giocatore giocatore = new Giocatore(5, Colore.BLU);
		pastore = new Pastore(giocatore, 0);
		id = (int)Math.random()*20;
		value = (int)Math.random()*6;
		casella = new Casella(id, value);
	}
	
	@Test
	public final void testGetId() {
		int actual = casella.getId();
		assertEquals(id, actual);
	}

	@Test
	public final void testGetValue() {
		int actual = casella.getValue();
		assertEquals(value, actual);
	}

	@Test
	public final void testToString() {
		String expected = "Casella("+id+"): "+value;
		String actual = casella.toString();
		assertEquals(expected, actual);
	}

	@Test
	public final void testGetClassString() {
		String expected = "Casella";
		String actual = casella.getClassString();
		assertEquals(expected, actual);
	}

	@Test
	public final void testRecintoDefault() {
		boolean expected = false;
		boolean actual = casella.hasRecinto();
		assertEquals(expected, actual);
	}
	
	@Test
	public final void testRecintoSetTrue() {
		casella.setRecinto(false);
		boolean expected = true;
		casella.setRecinto(true);
		boolean actual = casella.hasRecinto();
		
		assertEquals(expected, actual);
	}
	
	@Test
	public final void testRecintoSetFalse() {
		casella.setRecinto(true);
		boolean expected = false;
		casella.setRecinto(false);
		boolean actual = casella.hasRecinto();
		
		assertEquals(expected, actual);
	}

	@Test
	public final void testHasPastoreDefault() {
		boolean expected = false;
		boolean actual = casella.hasPastore();
		
		assertEquals(expected, actual);
	}
	
	@Test
	public final void testHasPastoreTrue() {
		casella.setPastore(null);
		casella.setPastore(pastore);
		boolean expected = true;
		boolean actual = casella.hasPastore();
		
		assertEquals(expected, actual);
	}
	
	@Test
	public final void testHasPastoreFalse() {
		casella.setPastore(pastore);
		casella.setPastore(null);
		boolean expected = false;
		boolean actual = casella.hasPastore();
		
		assertEquals(expected, actual);
	}
	
	@Test
	public final void testPastoreDefault() {
		Pastore expected = null;
		Pastore actual = casella.getPastore();
		
		assertEquals(expected, actual);
	}
	@Test
	public final void testPastore() {
		casella.setPastore(null);
		casella.setPastore(pastore);
		Pastore actual = casella.getPastore();
		Pastore expected = pastore;
		
		assertEquals(expected, actual);
	}
	
	@Test
	public final void testPastoreNull() {
		casella.setPastore(pastore);
		casella.setPastore(null);
		Pastore actual = casella.getPastore();
		Pastore expected = null;
		assertEquals(expected, actual);
	}


}
