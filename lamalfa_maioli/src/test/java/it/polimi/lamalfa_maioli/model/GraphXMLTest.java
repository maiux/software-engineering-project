package it.polimi.lamalfa_maioli.model;

import static org.junit.Assert.*;
import it.polimi.lamalfa_maioli.controller.exceptions.MapSyntaxException;
import it.polimi.lamalfa_maioli.model.Arco;
import it.polimi.lamalfa_maioli.model.Casella;
import it.polimi.lamalfa_maioli.model.GraphXML;
import it.polimi.lamalfa_maioli.model.Regione;
import it.polimi.lamalfa_maioli.model.Terreno;
import it.polimi.lamalfa_maioli.model.Vertex;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.SimpleGraph;
import org.junit.Before;
import org.junit.Test;

public class GraphXMLTest {
	private UndirectedGraph<Vertex, Arco> grafo = new SimpleGraph<>(Arco.class);
	private GraphXML g;
	private	Casella[] caselle = new Casella[2];
	private Regione[] regioni = new Regione[3];
	private Object[][] adiacenzeRegioni = new Object[2][2];
	private Object[][] adiacenzeCaselle = new Object[2][3];
		
	public void setUp() throws MapSyntaxException {
		caselle[0] = new Casella(0,1);
		caselle[1] = new Casella(1,2);
		regioni[0] = new Regione(0, 0);
		regioni[1] = new Regione(1, 6);
		regioni[2] = new Regione(2, 0);
		for(int i = 0; i < caselle.length; i++) {
			grafo.addVertex(caselle[i]);
		}		
		for(int i = 0; i < regioni.length; i++) {
			grafo.addVertex(regioni[i]);
		}		
		grafo.addEdge(regioni[0], caselle[0]);
		grafo.addEdge(regioni[1], caselle[0]);
		grafo.addEdge(regioni[1], caselle[1]);
		grafo.addEdge(caselle[1], caselle[0]);
		adiacenzeRegioni[0][0] = caselle[0];
		adiacenzeRegioni[1][0] = caselle[0];
		adiacenzeRegioni[1][1] = caselle[1];
		adiacenzeCaselle[0][0] = regioni[0];
		adiacenzeCaselle[0][1] = regioni[1];
		adiacenzeCaselle[0][2] = caselle[1];
		adiacenzeCaselle[1][0] = regioni[1];
		adiacenzeCaselle[1][1] = caselle[0];
		g = new GraphXML("/it/polimi/lamalfa_maioli/model/mappaTest.xml");
	}
	
	@Before
	public void setUpClass() {
		try {
			setUp();
		} catch(MapSyntaxException e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testGraphXML() {
		assertNotNull(g.allVertex());		
		assertNotNull(g.getGraph());
	}

	@Test
	public void testTrovaCasella() {
		for(int i = 0; i < 2; i++) {
			assertNotNull(g.trovaCasella(i));
		}
	}

	@Test
	public void testTrovaRegione() {
		for(int i = 0; i < 3; i++) {
			assertNotNull(g.trovaRegione(i));
		}
	}

	@Test
	public void testAllVertex() {
		List<Vertex> all = g.allVertex();
		int c = 0, r = 0;
		if(all.size() != caselle.length+regioni.length) {
			fail("Numero di caselle e regioni errato!");
		}
		for(int i = 0; i < all.size(); i++) {
			Vertex tmp = all.get(i);
			if("Casella".equals(tmp.getClassString())) {
				assertTrue(tmp.equals(caselle[c]));
				c++;
			} else {
				assertTrue(tmp.equals(regioni[r]));
				r++;
			}
		}
	}

	@Test
	public void testAllCasella() {
		List<Casella> all = g.allCasella();
		for(int i = 0; i < all.size(); i++) {
			Casella tmp = all.get(i);
			assertTrue(tmp.equals(caselle[i]));
		}
	}

	@Test
	public void testAllRegione() {
		List<Regione> all = g.allRegione();
		for(int i = 0; i < all.size(); i++) {
			Regione tmp = all.get(i);
			assertTrue(tmp.equals(regioni[i]));
		}
	}

	@Test
	public void testGetGraph() {
		Vertex[] expected = new Vertex[g.getGraph().vertexSet().size()];
		Vertex[] actual = new Vertex[grafo.vertexSet().size()];
		grafo.vertexSet().toArray(expected);
		g.getGraph().vertexSet().toArray(actual);
		assertEquals(actual.length, expected.length);
		for(int i = 0; i < expected.length; i++) {
			assertTrue(expected[i].equals(actual[i]));
		}
		Arco[] edgesact = new Arco[4];
		g.getGraph().edgeSet().toArray(edgesact);
		Arco[] edgesexp = new Arco[4];
		grafo.edgeSet().toArray(edgesact);
		assertEquals(edgesact.length, edgesexp.length);
		for(int i = 0; i < edgesact.length; i++) {
			assertTrue(expected[i].equals(actual[i]));
		}
	}

	@Test
	public void testNearBy() {
		for(int i = 0; i < regioni.length; i++) {
			List<Vertex> nearRegione = g.nearBy(regioni[i]);
			int length = 0;
			if(i < adiacenzeRegioni.length) {
				for(int j = 0; j < adiacenzeRegioni[i].length; j++) {
					if(adiacenzeRegioni[i][j] != null) {
						length++;
					}
				}
				assertEquals(nearRegione.size(), length);
				int ok = 0;
				for(int j = 0; j < nearRegione.size(); j++) {
					for(int k = 0; k < adiacenzeRegioni[i].length; k++){
						if(nearRegione.get(j).equals(adiacenzeRegioni[i][k])) {
							ok++;
							break;
						}
					}
				}
				assertEquals(ok, length);
			} else {
				assertEquals(nearRegione.size(), 0);
			}
		}
		for(int i = 0; i < caselle.length; i++) {
			List<Vertex> nearCasella = g.nearBy(caselle[i]);
			int length = 0;
			if(i < adiacenzeCaselle.length) {
				for(int j = 0; j < adiacenzeCaselle[i].length; j++) {
					if(adiacenzeCaselle[i][j] != null) {
						length++;
					}
				}
				assertEquals(nearCasella.size(), length);
				int ok = 0;
				for(int j = 0; j < nearCasella.size(); j++) {
					for(int k = 0; k < adiacenzeCaselle[i].length; k++){
						if(nearCasella.get(j).equals(adiacenzeCaselle[i][k])) {
							ok++;
							break;
						}
					}
				}
				assertEquals(ok, length);
			} else {
				assertEquals(nearCasella.size(), 0);
			}
		}
	}

	@Test
	public void testCasellaNearByRegioneInt() {
		for(int i = 0; i < regioni.length; i++) {
			Casella actual = g.casellaNearBy(regioni[i], 1);
			int length = 0;
			if(i < adiacenzeRegioni.length) {
				for(int j = 0; j < adiacenzeRegioni[i].length; j++) {
					if(adiacenzeRegioni[i][j] != null) {
						length++;
					}
				}
				boolean pass = false;
				for(int j = 0; j < length; j++) {
					if(adiacenzeRegioni[i][j].equals(actual)) {
						pass = true;
						break;
					}
				}
				assertTrue(pass);
			} else {
				assertEquals(actual, null);
			}
		}
	}

	@Test
	public void testCasellaNearByRegione() {
		for(int i = 0; i < regioni.length; i++) {
			List<Casella> nearBy = g.casellaNearBy(regioni[i]);
			if(i < adiacenzeRegioni.length) {
				int ok = 0;
				for(int j = 0; j < nearBy.size(); j++) {
					for(int k = 0; k < adiacenzeRegioni[i].length; k++){
						if(nearBy.get(j).equals(adiacenzeRegioni[i][k])) {
							ok++;
							break;
						}
					}
				}
				assertEquals(ok, nearBy.size());
			} else {
				assertEquals(nearBy.size(), 0);
			}
		}
	}

	@Test
	public void testCasellaNearByCasella() {
		for(int i = 0; i < caselle.length; i++) {
			List<Casella> nearBy = g.casellaNearBy(caselle[i]);
			if(i < adiacenzeCaselle.length) {
				int ok = 0;
				for(int j = 0; j < nearBy.size(); j++) {
					for(int k = 0; k < adiacenzeCaselle[i].length; k++){
						if(nearBy.get(j).equals(adiacenzeCaselle[i][k])) {
							ok++;
							break;
						}
					}
				}
				assertEquals(ok, nearBy.size());
			} else {
				assertEquals(nearBy.size(), 0);
			}
		}
	}

	@Test
	public void testRegioneNearByCasellaRegione() {
		Regione actual = g.regioneNearBy(caselle[0],regioni[0]);
		assertEquals(regioni[1], actual);
		actual = g.regioneNearBy(caselle[0],regioni[1]);
		assertEquals(regioni[0], actual);		
	}

	@Test
	public void testRegioneNearByCasella() {
		for(int i = 0; i < caselle.length; i++) {
			List<Regione> nearBy = g.regioneNearBy(caselle[i]);
			if(i < adiacenzeCaselle.length) {
				int ok = 0;
				for(int j = 0; j < nearBy.size(); j++) {
					for(int k = 0; k < adiacenzeCaselle[i].length; k++){
						if(nearBy.get(j).equals(adiacenzeCaselle[i][k])) {
							ok++;
							break;
						}
					}
				}
				assertEquals(ok, nearBy.size());
			} else {
				assertEquals(nearBy.size(), 0);
			}
		}
	}

	@Test
	public void testAreNearVertexVertex() {
		for(int i = 0; i < adiacenzeRegioni.length; i++) {
			for(int j = 0; j < adiacenzeRegioni[i].length; j++) {
				if(adiacenzeRegioni[i][j] != null) {
					assertTrue(g.areNear((Vertex)adiacenzeRegioni[i][j], regioni[i]));
				}
			}
		}
		for(int i = 0; i < adiacenzeCaselle.length; i++) {
			for(int j = 0; j < adiacenzeCaselle[i].length; j++) {
				if(adiacenzeCaselle[i][j] != null) {
					assertTrue(g.areNear((Vertex)adiacenzeCaselle[i][j], caselle[i]));
				}
			}
		}
	}

	@Test
	public void testAreNearRegioneRegioneCasella() {
		assertTrue(g.areNear(regioni[0], regioni[1], caselle[0]));
		assertTrue(g.areNear(regioni[1], regioni[0], caselle[0]));
	}
	
	@Test
	public void testTrovaRegioni() {
		for(int r = 0; r < Terreno.values().length; r++) {
			List<Regione> all = g.trovaRegioni(Terreno.values()[r]);
			int ok = 0;
			for(int i = 0; i < all.size(); i++) {
				for(int j = 0; j < regioni.length; j++) {
					if(regioni[j].equals(all.get(i))) {
						ok++;
						break;
					}
				}
			}
			assertEquals(all.size(), ok);
		}
	}

	@Test
	public void testNumeroRegioni() {
		assertEquals(regioni.length, g.numeroRegioni());
	}
	
	@Test
	public void testCheckRegione() {
		Method method = null;
		try {
			method = GraphXML.class.getDeclaredMethod("checkRegione", int.class, int.class);
		} catch (NoSuchMethodException | SecurityException e) {
			fail("exception");
		}
		method.setAccessible(true);
		boolean output = false;
		try {
			// ID già esistente
			method.invoke(g, 0,0);
		} catch (IllegalAccessException e) {
			fail("exception");
		} catch (IllegalArgumentException e) {
			fail("exception");
		} catch (InvocationTargetException e) {
			output = true;
		}
		assertEquals(output, true);
		
		try {
			output = false;
			// ID non valido (<0)
			method.invoke(g, -1,0);
		} catch (IllegalAccessException e) {
			fail("exception");
		} catch (IllegalArgumentException e) {
			fail("exception");
		} catch (InvocationTargetException e) {
			output = true;
		}
		assertEquals(output, true);
		
		try {
			output = false;
			// Tipo regione non valido
			method.invoke(g, 30,-1);
		} catch (IllegalAccessException e) {
			fail("exception");
		} catch (IllegalArgumentException e) {
			fail("exception");
		} catch (InvocationTargetException e) {
			output = true;
		}
		assertEquals(output, true);
		
		try {
			output = true;
			// Operazione permessa
			method.invoke(g, 30, 1);
		} catch (IllegalAccessException e) {
			fail("exception");
		} catch (IllegalArgumentException e) {
			fail("exception");
		} catch (InvocationTargetException e) {
			output = false;
		}
		assertEquals(output, true);
	}
	
	@Test
	public void testCheckCasella() {
		Method method = null;
		try {
			method = GraphXML.class.getDeclaredMethod("checkCasella", int.class, int.class);
		} catch (NoSuchMethodException | SecurityException e) {
			fail("exception");
		}
		method.setAccessible(true);
		boolean output = false;
		try {
			// ID già esistente
			method.invoke(g, 0,0);
		} catch (IllegalAccessException e) {
			fail("exception");
		} catch (IllegalArgumentException e) {
			fail("exception");
		} catch (InvocationTargetException e) {
			output = true;
		}
		assertEquals(output, true);
		
		try {
			output = false;
			// ID non valido (<0)
			method.invoke(g, -1,0);
		} catch (IllegalAccessException e) {
			fail("exception");
		} catch (IllegalArgumentException e) {
			fail("exception");
		} catch (InvocationTargetException e) {
			output = true;
		}
		assertEquals(output, true);
		
		try {
			output = false;
			// Valore non valido (<0)
			method.invoke(g, 30,-1);
		} catch (IllegalAccessException e) {
			fail("exception");
		} catch (IllegalArgumentException e) {
			fail("exception");
		} catch (InvocationTargetException e) {
			output = true;
		}
		assertEquals(output, true);
		
		try {
			output = true;
			// Operazione ammessa
			method.invoke(g, 30,1);
		} catch (IllegalAccessException e) {
			fail("exception");
		} catch (IllegalArgumentException e) {
			fail("exception");
		} catch (InvocationTargetException e) {
			output = false;
		}
		assertEquals(output, true);
	}
	
	@Test
	public void testCheckConnessione() {
		Method method = null;
		try {
			method = GraphXML.class.getDeclaredMethod("checkConnessione", new Class[] { Vertex[].class } );
		} catch (NoSuchMethodException | SecurityException e) {
			fail("exception");
		}
		method.setAccessible(true);
		boolean output = false;
		Vertex[] conn = { regioni[0], regioni[1] };
		
		try {
			// Connessione tra due regioni non possibile
			method.invoke(g, new Object[] { conn });
		} catch (IllegalAccessException e) {
			fail("exception");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			fail("exception");
		} catch (InvocationTargetException e) {
			output = true;
		}
		assertEquals(output, true);
		
		try {
			output = false;
			conn[0] = regioni[0];
			conn[1] = null;
			// Null non ammesso in una connessione
			method.invoke(g, new Object[] { conn });
		} catch (IllegalAccessException e) {
			fail("exception");
		} catch (IllegalArgumentException e) {
			fail("exception");
		} catch (InvocationTargetException e) {
			output = true;
		}
		assertEquals(output, true);
		
		try {
			output = true;
			conn[0] = regioni[0];
			conn[1] = caselle[0];
			// Operazione ammessa
			method.invoke(g, new Object[] { conn });
		} catch (IllegalAccessException e) {
			fail("exception");
		} catch (IllegalArgumentException e) {
			fail("exception");
		} catch (InvocationTargetException e) {
			output = false;
		}
		assertEquals(output, true);
	}
}
