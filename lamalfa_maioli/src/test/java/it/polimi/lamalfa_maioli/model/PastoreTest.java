package it.polimi.lamalfa_maioli.model;

import static org.junit.Assert.*;
import it.polimi.lamalfa_maioli.model.Casella;
import it.polimi.lamalfa_maioli.model.Giocatore;
import it.polimi.lamalfa_maioli.model.Pastore;

import org.junit.Before;
import org.junit.Test;

public class PastoreTest {
	private Casella casella;
	private Giocatore giocatore;
	private int numeroMossa = 0;
	private boolean pastoreMosso = false;
	private boolean terrenoComprato = false;
	private boolean pecoraMossa = false;
	private boolean pecoraAccoppiata = false;
	private boolean haSparato = false;
	private Pastore pastore;
	
	@Before
	public void setUpClass() {
		casella = new Casella(1, 1);
		giocatore = new Giocatore(5, Colore.getRandom());
		pastore = new Pastore(giocatore,0);
		pastore.setCasella(casella);
	}

	@Test
	public void testPastore() {
		assertEquals(pastore.getCasella(), casella);
		assertEquals(pastore.getGiocatore(), giocatore);
		assertEquals(pastore.getNumeroMossa(), numeroMossa);
		assertEquals(pastore.isPastoreMosso(), pastoreMosso);
		assertEquals(pastore.isTerrenoComprato(), terrenoComprato);
		assertEquals(pastore.isPecoraMossa(), pecoraMossa);
		assertEquals(pastore.isPecoraAccoppiata(), pecoraAccoppiata);
		assertEquals(pastore.isHaSparato(), haSparato);
	}

	@Test
	public void testGetSetCasella() {
		casella = new Casella(2,2);
		pastore.setCasella(casella);
		Casella actual = pastore.getCasella();
		assertEquals(casella, actual);
	}

	@Test
	public void testGetSetNumeroMossa() {
		numeroMossa = (int)(Math.random()*15+1);
		pastore.setNumeroMossa(numeroMossa);
		int actual = pastore.getNumeroMossa();
		assertEquals(numeroMossa, actual);
	}

	@Test
	public void testSetIsPastoreMosso() {
		pastoreMosso = true;
		pastore.setPastoreMosso(pastoreMosso);
		boolean actual = pastore.isPastoreMosso();
		assertEquals(pastoreMosso, actual);
		pastoreMosso = false;
		pastore.setPastoreMosso(pastoreMosso);
		actual = pastore.isPastoreMosso();
		assertEquals(pastoreMosso, actual);		
	}

	@Test
	public void testSetIsTerrenoComprato() {
		terrenoComprato = true;
		pastore.setTerrenoComprato(terrenoComprato);
		boolean actual = pastore.isTerrenoComprato();
		assertEquals(terrenoComprato, actual);
		terrenoComprato = false;
		pastore.setTerrenoComprato(terrenoComprato);
		actual = pastore.isTerrenoComprato();
		assertEquals(terrenoComprato, actual);
	}

	@Test
	public void testSetIsPecoraMossa() {
		pecoraMossa = true;
		pastore.setPecoraMossa(pecoraMossa);
		boolean actual = pastore.isPecoraMossa();
		assertEquals(pecoraMossa, actual);
		pecoraMossa = false;
		pastore.setPecoraMossa(pecoraMossa);
		actual = pastore.isPecoraMossa();
		assertEquals(pecoraMossa, actual);
	}

	@Test
	public void testSetIsPecoraAccoppiata() {
		pecoraAccoppiata = true;
		pastore.setPecoraAccoppiata(pecoraAccoppiata);
		boolean actual = pastore.isPecoraAccoppiata();
		assertEquals(pecoraAccoppiata, actual);
		pecoraAccoppiata = false;
		pastore.setPecoraAccoppiata(pecoraAccoppiata);
		actual = pastore.isPecoraAccoppiata();
		assertEquals(pecoraAccoppiata, actual);
	}

	@Test
	public void testSetIsHaSparato() {
		haSparato = true;
		pastore.setHaSparato(haSparato);
		boolean actual = pastore.isHaSparato();
		assertEquals(haSparato, actual);
		haSparato = false;
		pastore.setHaSparato(haSparato);
		actual = pastore.isHaSparato();
		assertEquals(haSparato, actual);
	}
}
