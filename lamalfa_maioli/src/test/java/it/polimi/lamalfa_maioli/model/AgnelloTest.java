package it.polimi.lamalfa_maioli.model;

import static org.junit.Assert.*;
import it.polimi.lamalfa_maioli.controller.exceptions.MapSyntaxException;
import it.polimi.lamalfa_maioli.model.Agnello;
import it.polimi.lamalfa_maioli.model.Giocatore;
import it.polimi.lamalfa_maioli.model.Regione;

import org.junit.Before;
import org.junit.Test;

public class AgnelloTest {
	private Giocatore giocatore;
	private Agnello agnello;
	private int turnoCreazione;
	private Regione regione;
	
	@Before
	public void setUpClass() {
		giocatore = new Giocatore(5, Colore.getRandom());
		turnoCreazione = (int)(Math.random()*20);
		try {
			regione = new Regione(1, 0);
		} catch(MapSyntaxException e) {
			fail("Errore nel definire la regione!");
		}
		agnello = new Agnello(turnoCreazione, giocatore, regione);
	}
	
	@Test
	public void testGetAge() {
		int actual = agnello.getAge();
		assertEquals(turnoCreazione, actual);
	}
	
	@Test
	public void testGetGiocatore() {
		Giocatore actual = agnello.getGiocatore();
		assertEquals(giocatore,actual);
	}
	
	@Test
	public void testGetRegione() {
		Regione actual = agnello.getRegione();
		assertEquals(regione, actual);
	}

}
