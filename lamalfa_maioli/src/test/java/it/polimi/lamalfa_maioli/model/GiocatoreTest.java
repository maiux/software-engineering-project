package it.polimi.lamalfa_maioli.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import it.polimi.lamalfa_maioli.model.Giocatore;
import it.polimi.lamalfa_maioli.model.OggettoVendita;
import it.polimi.lamalfa_maioli.model.Pastore;
import it.polimi.lamalfa_maioli.model.Terreno;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class GiocatoreTest {
	private Giocatore giocatore;
	private List<Pastore> pastori = new ArrayList<>();
	private int monete;
	private Colore colore;
	private Pastore pastoreCorrente;
	private List<OggettoVendita> terreniVendita = new ArrayList<>();
	private Map<Terreno, Integer> terreniComprati = new HashMap<>();
	
	@Before
	public void setUpClass() {
		monete = (int)(Math.random()*20);
		colore =  Colore.getRandom();
		giocatore = new Giocatore(monete, colore);
		for(int i = 0; i < 5; i++) {
			Terreno terreno = Terreno.getRandom();
			OggettoVendita vendita = new OggettoVendita(giocatore, terreno, (int)(Math.random()*5), (int)(Math.random()*4));
			terreniVendita.add(vendita);
			giocatore.getTerreniVendita().add(vendita);
			if(i == 0) {
				giocatore.setTerrenoIniziale(terreno);
				for(Terreno tipo : Terreno.class.getEnumConstants()) {
					terreniComprati.put(tipo, 0);
				}
			}
			int num = giocatore.getTerreniComprati().get(terreno)+1;
			giocatore.getTerreniComprati().put(terreno, num);
			terreniComprati.put(terreno, num);
		}
		Pastore tmp = new Pastore(giocatore, 0);
		pastori.add(tmp);
		giocatore.getPastori().add(tmp);
		giocatore.setPastoreCorrente(tmp);
		pastoreCorrente = tmp;
		tmp = new Pastore(giocatore, 1);
		pastori.add(tmp);
		giocatore.getPastori().add(tmp);
	}
	
	@Test
	public void testGiocatore() {
		int actual = giocatore.getMonete();
		assertEquals(monete, actual);
		Colore act = giocatore.getColore();
		assertEquals(colore, act);
	}

	@Test
	public void testGetTerreniVendita() {
		Map<Terreno, Integer> actual = giocatore.getTerreniComprati();
		assertEquals(terreniComprati, actual);
	}

	@Test
	public void testSetTerrenoIniziale() {
		Terreno iniziale = Terreno.getRandom();
		Map<Terreno, Integer> expected = new HashMap<>();
		for(Terreno tipo : Terreno.class.getEnumConstants()) {
			if(tipo.equals(iniziale)) {
				expected.put(tipo, 1);
			} else {
				expected.put(tipo, 0);
			}
		}
		giocatore.setTerrenoIniziale(iniziale);
		Map<Terreno, Integer> actual = giocatore.getTerreniComprati(); 
		assertEquals(expected, actual);
	}

	@Test
	public void testGetSetMonete() {
		int actual = giocatore.getMonete();
		assertEquals(monete, actual);
		monete = (int)(Math.random()*20);
		giocatore.setMonete(monete);
		actual = giocatore.getMonete();
		assertEquals(monete, actual);
	}

	@Test
	public void testGetSetColore() {
		Colore actual = giocatore.getColore();
		assertEquals(colore, actual);
	}

	@Test
	public void testGetPastori() {
		List<Pastore> actual = giocatore.getPastori();
		assertTrue(pastori.equals(actual));
	}

	@Test
	public void testGetSetPastoreCorrente() {
		Pastore actual = giocatore.getPastoreCorrente();
		assertEquals(pastoreCorrente, actual);
		pastoreCorrente = new Pastore(giocatore, 0);
		giocatore.setPastoreCorrente(pastoreCorrente);
		actual = giocatore.getPastoreCorrente();
		assertEquals(pastoreCorrente, actual);
	}

	@Test
	public void testGetTerreniComprati() {
		Map<Terreno, Integer> actual = giocatore.getTerreniComprati();
		assertTrue(actual.equals(terreniComprati));
	}

	@Test
	public void testAddTerreno() {
		Terreno terreno = Terreno.getRandom();
		int num = (int)(Math.random()*5);
		giocatore.addTerreno(terreno, num);
		Map<Terreno, Integer> actual = giocatore.getTerreniComprati();
		int balance = terreniComprati.get(terreno)+num;
		terreniComprati.put(terreno, balance);
		assertEquals(terreniComprati, actual);
	}

}
