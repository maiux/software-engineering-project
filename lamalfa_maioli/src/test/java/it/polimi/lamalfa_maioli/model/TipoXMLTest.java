package it.polimi.lamalfa_maioli.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class TipoXMLTest {

	@Test
	public void testGetContainerName() {
		String[] expected = { "caselle", "regioni", "connessioni" };
		for(int i = 0; i < TipoXML.values().length; i++) {
			assertEquals(expected[i], TipoXML.values()[i].getContainerName());
		}
	}

	@Test
	public void testGetElementName() {
		String[] expected = { "casella", "regione", "connessione" };
		for(int i = 0; i < TipoXML.values().length; i++) {
			assertEquals(expected[i], TipoXML.values()[i].getElementName());
		}
	}

	@Test
	public void testGetElementData() {
		String expected[][] = { {"id", "value"}, { "id", "type" }, { "el", "id", "type", "casella", "regione" } };
		for(int i = 0; i < TipoXML.values().length; i++) {
			String[] actual = TipoXML.values()[i].getElementData();
			for(int j = 0; j < actual.length; j++) {
				assertEquals(expected[i][j], actual[j]);
			}
		}
	}

}
