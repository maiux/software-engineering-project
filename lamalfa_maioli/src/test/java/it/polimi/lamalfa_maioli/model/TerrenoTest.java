package it.polimi.lamalfa_maioli.model;

import static org.junit.Assert.*;
import it.polimi.lamalfa_maioli.model.Terreno;

import java.util.List;

import org.junit.Test;

public class TerrenoTest {
	
	@Test
	public void testGetId() {
		for(int i = 0; i < Terreno.values().length; i++) {
			int actual = Terreno.values()[i].getId();
			assertEquals(i, actual);
		}
	}

	@Test
	public void testGetSetCosto() {
		for(int i = 0; i < Terreno.values().length; i++) {
			Terreno terreno = Terreno.values()[i];
			int actual = terreno.getCosto();
			if(terreno.getId() != 6) {
				assertEquals(1, actual);
				int costo = (int)(Math.random()*6);
				terreno.setCosto(costo);
				actual = terreno.getCosto();
				assertEquals(costo, actual);
			} else {
				assertEquals(-1, actual);
				int costo = (int)(Math.random()*6);
				terreno.setCosto(costo);
				actual = terreno.getCosto();
				assertEquals(-1, actual);
			}
		}
	}

	@Test
	public void testGetRandoms() {
		List<Terreno> rand = Terreno.getRandoms();
		for(int i = 0; i < rand.size()-1; i++) {
			for(int j = 1; j < rand.size(); j++) {
				Terreno rn1 = rand.get(i);
				Terreno rn2 = rand.get(j);
				assertTrue(!(rn1.equals(rn2) ^ i == j));
			}
		}
	}
}
