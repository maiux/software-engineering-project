package it.polimi.lamalfa_maioli.model;

import static org.junit.Assert.*;
import it.polimi.lamalfa_maioli.controller.exceptions.MapSyntaxException;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class GameStatusTest {
	private final List<Giocatore> giocatori = new ArrayList<Giocatore>();
	private final List<Agnello> agnelli = new ArrayList<Agnello>();
	private Giocatore giocatoreIniziale;
	private Giocatore giocatoreCorrente;
	private int turno = 0;
	private Regione posizionePecoraNera;
	private boolean ultimoGiro = false;
	private int numeroRecinti = 20;
	private GraphXML mappa;
	private Regione posizioneLupo;
	private final List<OggettoVendita> compravendita = new ArrayList<>();
	private int numeroGiocatori = 0;
	private GameStatus status;
	
	@Before
	public void setUp() {
		try {
			mappa = new GraphXML("/it/polimi/lamalfa_maioli/mappa.xml");
		} catch (MapSyntaxException e) {
			fail("Exception");
		}
		status = new GameStatus(mappa);
		Giocatore player = new Giocatore(5, Colore.GIALLO);
		status.getGiocatori().add(player);
		giocatori.add(player);
		giocatoreIniziale = player;
		status.setGiocatoreIniziale(player);
		giocatoreCorrente = player;
		status.setGiocatoreCorrente(player);
		player = new Giocatore(4, Colore.BLU);
		status.getGiocatori().add(player);
		giocatori.add(player);
		numeroGiocatori = 2;
		status.setNumeroGiocatori(2);
		Regione regione = null;
		try {
			regione = new Regione(0,6);
		} catch (MapSyntaxException e) {
			fail("Exception");
		}
		Agnello agnello = new Agnello(1, player, regione);
		agnelli.add(agnello);
		status.getAgnelli().add(agnello);
		posizioneLupo = regione;
		status.setPosizioneLupo(posizioneLupo);
		posizionePecoraNera = regione;
		status.setPosizionePecoraNera(posizionePecoraNera);	
		OggettoVendita tmp = new OggettoVendita(giocatoreCorrente, Terreno.getRandom(), 1, 1);
		compravendita.add(tmp);
		status.getCompravendita().add(tmp);
	}

	@Test
	public void testGameStatus() {
		assertEquals(mappa, status.getMappa());
	}

	@Test
	public void testProssimoGiocatore() {
		assertEquals(status.prossimoGiocatore(), giocatori.get(1));
		status.setGiocatoreCorrente(giocatori.get(1));
		assertEquals(status.prossimoGiocatore(), giocatori.get(0));
	}

	@Test
	public void testSetGetNumeroGiocatori() {
		assertEquals(numeroGiocatori, status.getNumeroGiocatori());
		status.setNumeroGiocatori(3);
		assertEquals(3, status.getNumeroGiocatori());
	}

	@Test
	public void testGetGiocatori() {
		assertEquals(status.getGiocatori(), giocatori);
	}

	@Test
	public void testGetAgnelli() {
		assertEquals(status.getAgnelli(), agnelli);
		Regione t = null;
		try {
			t = new Regione(1,1);
		} catch (MapSyntaxException e) {
			fail("Exception");
		}
		Agnello tmp = new Agnello(2, status.getGiocatoreCorrente(), t);
		status.getAgnelli().add(tmp);
		agnelli.add(tmp);
		assertEquals(status.getAgnelli(), agnelli);
	}

	@Test
	public void testGetSetGiocatoreIniziale() {
		assertEquals(giocatoreIniziale, status.getGiocatoreIniziale());
		//NB: non si può cambiare una volta impostato
		status.setGiocatoreIniziale(giocatori.get(1));
		assertEquals(giocatori.get(0), status.getGiocatoreIniziale());
	}

	@Test
	public void testGetSetGiocatoreCorrente() {
		assertEquals(giocatoreCorrente, status.getGiocatoreCorrente());
		giocatoreCorrente = status.prossimoGiocatore();
		status.setGiocatoreCorrente(status.prossimoGiocatore());
		assertEquals(giocatoreCorrente, status.getGiocatoreCorrente());
	}

	@Test
	public void testGetSetTurno() {
		assertEquals(status.getTurno(), turno);
		turno = 2;
		status.setTurno(turno);
		assertEquals(status.getTurno(), turno);
	}

	@Test
	public void testGetSetPosizionePecoraNera() {
		assertEquals(posizionePecoraNera, status.getPosizionePecoraNera());
		Regione tmp = null;
		try {
			 tmp = new Regione(2,2);
		} catch (MapSyntaxException e) {
			fail("Exception");
		}
		posizionePecoraNera = tmp;
		status.setPosizionePecoraNera(tmp);
		assertEquals(posizionePecoraNera, status.getPosizionePecoraNera());		
	}

	@Test
	public void testIsSetUltimoGiro() {
		assertEquals(status.isUltimoGiro(), ultimoGiro);
		status.setUltimoGiro(true);
		ultimoGiro = true;
		assertEquals(status.isUltimoGiro(), ultimoGiro);
		status.setUltimoGiro(false);
		ultimoGiro = false;
		assertEquals(status.isUltimoGiro(), ultimoGiro);
		
	}

	@Test
	public void testGetSetNumeroRecinti() {
		assertEquals(numeroRecinti, status.getNumeroRecinti());
		for(int i = 0; i < 5; i++) {
			numeroRecinti = (int)(Math.random()*20);
			status.setNumeroRecinti(numeroRecinti);
			assertEquals(numeroRecinti, status.getNumeroRecinti());			
		}
	}

	@Test
	public void testGetMappa() {
		assertEquals(mappa, status.getMappa());
	}

	@Test
	public void testGetSetPosizioneLupo() {
		assertEquals(posizioneLupo, status.getPosizioneLupo());
		Regione tmp = null;
		try {
			 tmp = new Regione(2,2);
		} catch (MapSyntaxException e) {
			fail("Exception");
		}
		posizioneLupo = tmp;
		status.setPosizioneLupo(tmp);
		assertEquals(posizioneLupo, status.getPosizioneLupo());	
	}

	@Test
	public void testGetCompravendita() {
		assertEquals(status.getCompravendita(), compravendita);
		OggettoVendita tmp = new OggettoVendita(giocatoreCorrente, Terreno.getRandom(), 1, 1);
		status.getCompravendita().add(tmp);
		compravendita.add(tmp);
		assertEquals(status.getCompravendita(), compravendita);		
	}

}
