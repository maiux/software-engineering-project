package it.polimi.lamalfa_maioli.model;

import static org.junit.Assert.*;
import it.polimi.lamalfa_maioli.controller.exceptions.MapSyntaxException;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.SimpleGraph;
import org.junit.Before;
import org.junit.Test;

public class ArcoTest {

	private UndirectedGraph<Vertex, Arco> grafo = new SimpleGraph<>(Arco.class);
	private	Casella[] caselle = new Casella[2];
	private Regione[] regioni = new Regione[2];
	private Object[][] adiacenzeRegioni = new Object[2][3];
	private Object[][] adiacenzeCaselle = new Object[2][3];
		
	public void setUp() throws MapSyntaxException {
		caselle[0] = new Casella(0,1);
		caselle[1] = new Casella(1,2);
		regioni[0] = new Regione(0, 0);
		regioni[1] = new Regione(1, 6);
		for(int i = 0; i < caselle.length; i++) {
			grafo.addVertex(caselle[i]);
		}		
		for(int i = 0; i < regioni.length; i++) {
			grafo.addVertex(regioni[i]);
		}		
		grafo.addEdge(regioni[0], caselle[0]);
		grafo.addEdge(regioni[1], caselle[0]);
		grafo.addEdge(regioni[1], caselle[1]);
		grafo.addEdge(caselle[1], caselle[0]);
		adiacenzeRegioni[0][0] = caselle[0];
		adiacenzeRegioni[1][0] = caselle[0];
		adiacenzeRegioni[1][1] = caselle[1];
		adiacenzeCaselle[0][0] = regioni[0];
		adiacenzeCaselle[0][1] = regioni[1];
		adiacenzeCaselle[0][2] = caselle[1];
		adiacenzeCaselle[1][0] = regioni[1];
		adiacenzeCaselle[1][1] = caselle[0];
	}
	
	@Before
	public void setUpClass() {
		try {
			setUp();
		} catch(MapSyntaxException e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void testGetOther() {
		for(int k = 0; k < caselle.length; k++) {
			int ok = 0;
			Object[] nearbyElements = grafo.edgesOf(caselle[k]).toArray();
			for(int i = 0; i < nearbyElements.length; i++) {
				for(int j = 0; j < adiacenzeCaselle[k].length; j++) {
					Arco vertex = (Arco)nearbyElements[i];
					if(adiacenzeCaselle[k][j].equals(vertex.getOther(caselle[k]))) {
						ok++;
						break;
					}
				}
			}
			assertEquals(nearbyElements.length, ok);
		}
	}

}
