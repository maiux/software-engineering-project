package it.polimi.lamalfa_maioli.model;

import static org.junit.Assert.*;
import it.polimi.lamalfa_maioli.model.Giocatore;
import it.polimi.lamalfa_maioli.model.OggettoVendita;
import it.polimi.lamalfa_maioli.model.Terreno;

import org.junit.Before;
import org.junit.Test;

public class OggettoVenditaTest {
	private Terreno tipoTerreno;
	private int quantita;
	private int prezzo;
	private Giocatore giocatore;
	private OggettoVendita oggettoVendita;
	
	@Before
	public void setUpClass() {
		tipoTerreno = Terreno.getRandom();
		quantita = (int)(Math.random()*6);
		prezzo = (int)(Math.random()*4);
		giocatore = new Giocatore(20, Colore.getRandom());
		oggettoVendita = new OggettoVendita(giocatore, tipoTerreno, quantita, prezzo);
	}
	
	@Test
	public void testOggettoVendita() {
		Giocatore giocatoreActual = oggettoVendita.getGiocatore();
		assertEquals(giocatore, giocatoreActual);
		Terreno terrenoActual = oggettoVendita.getTipoTerreno();
		assertEquals(tipoTerreno, terrenoActual);
		int quantitaActual = oggettoVendita.getQuantita();
		assertEquals(quantita, quantitaActual);
		int prezzoActual = oggettoVendita.getPrezzo();
		assertEquals(prezzo, prezzoActual);
	}

	@Test
	public void testGetGiocatore() {
		Giocatore actual = oggettoVendita.getGiocatore();
		assertEquals(giocatore, actual);
	}

	@Test
	public void testGetTipoTerreno() {
		Terreno actual = oggettoVendita.getTipoTerreno();
		assertEquals(tipoTerreno, actual);
	}

	@Test
	public void testGetSetQuantita() {
		int actual = oggettoVendita.getQuantita();
		assertEquals(quantita, actual);
		quantita = (int)(Math.random()*6);
		oggettoVendita.setQuantita(quantita);
		actual = oggettoVendita.getQuantita();
		assertEquals(quantita, actual);
	}

	@Test
	public void testGetSetPrezzo() {
		int actual = oggettoVendita.getPrezzo();
		assertEquals(prezzo, actual);
		prezzo = (int)(Math.random()*4);
		oggettoVendita.setPrezzo(prezzo);
		actual = oggettoVendita.getPrezzo();
		assertEquals(prezzo, actual);
	}

}
