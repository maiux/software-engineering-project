package it.polimi.lamalfa_maioli.controller;

import static org.junit.Assert.*;
import it.polimi.lamalfa_maioli.controller.GameCore.Animale;
import it.polimi.lamalfa_maioli.controller.exceptions.ColorException;
import it.polimi.lamalfa_maioli.controller.exceptions.DadoException;
import it.polimi.lamalfa_maioli.model.Agnello;
import it.polimi.lamalfa_maioli.model.Casella;
import it.polimi.lamalfa_maioli.model.Colore;
import it.polimi.lamalfa_maioli.model.Giocatore;
import it.polimi.lamalfa_maioli.model.OggettoVendita;
import it.polimi.lamalfa_maioli.model.Pastore;
import it.polimi.lamalfa_maioli.model.Regione;
import it.polimi.lamalfa_maioli.model.Terreno;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class GameCoreTest {
	private GameCore core;
	private int numeroGiocatori;
	
	@Before
	public void setUp() {
		numeroGiocatori = (int)(Math.random()*3+1);
		core = new GameCore(numeroGiocatori);
		List<Colore> tmp = new ArrayList<>();
		for(int i = 0; i < numeroGiocatori; i++) {
			tmp.add(Colore.values()[i]);
		}
		try {
			core.creaGiocatori(tmp);
		} catch (ColorException e) {
			fail("Exception");
		}
		core.impostaGiocatoreIniziale();
	}

	@Test
	public void testGameCore() {
		Regione regioneLupo = core.getStatus().getPosizioneLupo();
		assertTrue(regioneLupo != null);
		assertTrue(core.getStatus() != null);
		assertEquals(core.getStatus().getNumeroGiocatori(), numeroGiocatori);
	}
	
	@Test
	public void testImpostaGiocatoreIniziale() {
		assertTrue(core.getStatus().getGiocatoreIniziale() != null);
	}
	
	@Test
	public void testMuoviPecora() {
		Giocatore giocatore = core.getStatus().getGiocatoreCorrente();
		Casella casella = (Casella)core.getStatus().getMappa().trovaCasella(0);
		core.posizionaPastore(giocatore, 0, casella);
		Pastore pastore = giocatore.getPastori().get(0);
		Regione regione1 = (Regione)core.getStatus().getMappa().trovaRegione(0);
		int reg1 = regione1.getPecore()-1;
		Regione regione2 = (Regione)core.getStatus().getMappa().trovaRegione(16);
		int reg2 = regione2.getPecore()+1;
		core.muoviPecora(pastore, regione1, regione2);
		assertEquals(regione1.getPecore(), reg1);
		assertEquals(regione2.getPecore(), reg2);
	}

	@Test
	public void testMuoviAriete() {
		Giocatore giocatore = core.getStatus().getGiocatoreCorrente();
		Casella casella = (Casella)core.getStatus().getMappa().trovaCasella(0);
		core.posizionaPastore(giocatore, 0, casella);
		Pastore pastore = giocatore.getPastori().get(0);
		Regione regione1 = (Regione)core.getStatus().getMappa().trovaRegione(0);
		int reg1 = regione1.getArieti()-1;
		Regione regione2 = (Regione)core.getStatus().getMappa().trovaRegione(16);
		int reg2 = regione2.getArieti()+1;
		core.muoviAriete(pastore, regione1, regione2);
		assertEquals(regione1.getArieti(), reg1);
		assertEquals(regione2.getArieti(), reg2);
	}

	@Test
	public void testMuoviPecoraNera() {
		Giocatore giocatore = core.getStatus().getGiocatoreCorrente();
		Casella casella = (Casella)core.getStatus().getMappa().trovaCasella(41);
		core.posizionaPastore(giocatore, 0, casella);
		Pastore pastore = giocatore.getPastori().get(0);
		Regione regione1 = (Regione)core.getStatus().getMappa().trovaRegione(18);
		boolean pn1 = regione1.hasPecoraNera();
		Regione regione2 = (Regione)core.getStatus().getMappa().trovaRegione(17);
		boolean pn2 = regione2.hasPecoraNera();
		core.muoviPecoraNera(pastore, regione1, regione2);
		assertEquals(core.getStatus().getPosizionePecoraNera(), regione2);
		assertEquals(regione1.hasPecoraNera(), !pn1);
		assertEquals(regione2.hasPecoraNera(), !pn2);
	}

	@Test
	public void testDadoPecoraNera() {
		Regione start = core.getStatus().getPosizionePecoraNera();
		core.dadoPecoraNera();
		assertTrue(!start.equals(core.getStatus().getPosizionePecoraNera()));
		start = core.getStatus().getPosizionePecoraNera();
		List<Casella> tmp = core.getStatus().getMappa().casellaNearBy(start);
		for(int i = 0; i < tmp.size(); i++) {
			tmp.get(i).setRecinto(true);
		}
		core.dadoPecoraNera();
		assertTrue(start.equals(core.getStatus().getPosizionePecoraNera()));		
	}

	@Test
	public void testDadoLupo() {
		Regione start = core.getStatus().getPosizioneLupo();
		core.dadoLupo();
		assertTrue(!start.equals(core.getStatus().getPosizioneLupo()));
		start = core.getStatus().getPosizioneLupo();
		List<Casella> tmp = core.getStatus().getMappa().casellaNearBy(start);
		for(int i = 0; i < tmp.size(); i++) {
			tmp.get(i).setRecinto(true);
		}
		core.dadoLupo();
		assertTrue(!start.equals(core.getStatus().getPosizioneLupo()));	
	}

	@Test
	public void testGetFreeMoves() {
		List<Casella> caselle = core.getStatus().getMappa().allCasella();
		for(int i = 0; i < caselle.size(); i++) {
			testGetFreemoves(caselle.get(i));
		}
		
	}

	public void posizionaPastore(Giocatore giocatore, int i, Casella casella) {
		Pastore pastore = giocatore.getPastori().get(i);
		pastore.setCasella(casella);
		casella.setPastore(pastore);
	}
	
	public void testGetFreemoves(Casella casella) {
		Giocatore giocatore = core.getStatus().getGiocatoreCorrente();
		posizionaPastore(giocatore, 0, casella);
		Pastore pastore = giocatore.getPastori().get(0);
		List<Casella> free = core.getFreeMoves(pastore);
		List<Casella> expected = core.getStatus().getMappa().casellaNearBy(casella);
		for(int i = 0; i < expected.size(); i++) {
			Casella tmp = expected.get(i);
			if(tmp.hasPastore() || tmp.hasRecinto()) {
				expected.remove(tmp);
				i--;
			}
		}
		assertEquals(free, expected);
	}
	
	@Test
	public void testGetMoveMoney() {
		List<Casella> caselle = core.getStatus().getMappa().allCasella();
		Giocatore giocatore = core.getStatus().getGiocatoreCorrente();
		for(int i = 0; i < caselle.size(); i++) {
			Casella casella = caselle.get(i);
			posizionaPastore(giocatore, 0, casella);
			Pastore pastore = giocatore.getPastori().get(0);
			List<Casella> free = core.getFreeMoves(pastore);
			free.add(casella);
			for(int j = 0; j < caselle.size(); j++) {
				Casella tmp = caselle.get(j);
				int money = core.getMoveMoney(pastore, tmp);
				if(free.contains(tmp)) {
					assertEquals(0, money);
				} else {
					assertEquals(1, money);
				}
			}
		}
	}

	@Test
	public void testCasellaLibera() {
		List<Casella> caselle = core.getStatus().getMappa().allCasella();
		Giocatore giocatore = core.getStatus().getGiocatoreCorrente();
		Pastore pastore = giocatore.getPastori().get(0);
		for(int i = 0; i < caselle.size(); i++) {
			Casella casella = caselle.get(i);
			assertEquals(true, core.casellaLibera(casella));
			casella.setPastore(pastore);
			assertEquals(false, core.casellaLibera(casella));
			casella.setPastore(null);
			assertEquals(true, core.casellaLibera(casella));
			casella.setRecinto(true);
			assertEquals(false, core.casellaLibera(casella));
			casella.setRecinto(false);
			assertEquals(true, core.casellaLibera(casella));
		}
	}

	@Test
	public void testMuoviPastore() {
		Casella casella = (Casella) core.getStatus().getMappa().trovaCasella(0);
		Giocatore giocatore = core.getStatus().getGiocatoreCorrente();
		posizionaPastore(giocatore, 0, casella);
		Pastore pastore = giocatore.getPastori().get(0);
		casella = (Casella) core.getStatus().getMappa().trovaCasella(1);
		int mb = giocatore.getMonete();
		boolean actual = core.muoviPastore(pastore, casella);
		assertEquals(true, actual);
		assertEquals(mb, giocatore.getMonete());
		casella = (Casella) core.getStatus().getMappa().trovaCasella(0);
		actual = core.muoviPastore(pastore, casella);
		assertEquals(false, actual);
		casella = (Casella) core.getStatus().getMappa().trovaCasella(10);
		actual = core.muoviPastore(pastore, casella);
		assertEquals(true, actual);
		mb--;
		assertEquals(mb, giocatore.getMonete());
		casella = (Casella) core.getStatus().getMappa().trovaCasella(1);
		actual = core.muoviPastore(pastore, casella);
		assertEquals(false, actual);
		casella = (Casella) core.getStatus().getMappa().trovaCasella(30);
		actual = core.muoviPastore(pastore, casella);
		assertEquals(true, actual);
		mb--;
		assertEquals(mb, giocatore.getMonete());
		casella = (Casella) core.getStatus().getMappa().trovaCasella(31);
		actual = core.muoviPastore(pastore, casella);
		assertEquals(false, actual);
	}

	@Test
	public void testTerreniComprabili() {
		List<Casella> caselle = core.getStatus().getMappa().allCasella();
		Giocatore giocatore = core.getStatus().getGiocatoreCorrente();
		for(int i = 0; i < caselle.size(); i++) {
			Casella casella = caselle.get(i);
			posizionaPastore(giocatore, 0, casella);
			Pastore pastore = giocatore.getPastori().get(0);
			List<Terreno> actual = core.terreniComprabili(pastore);
			List<Regione> tmp = core.getStatus().getMappa().regioneNearBy(pastore.getCasella());
			List<Terreno> expected = new ArrayList<>();
			for(int j = 0; j < tmp.size(); j++) {
				Terreno type = tmp.get(j).getType();
				if(!type.equals(Terreno.SHEEPSBURG) && !expected.contains(type)) {
					expected.add(type);
				}
			}
			assertEquals(expected, actual);
		}
		
	}

	@Test
	public void testIsTerrenoComprabile() {
		Casella casella = (Casella) core.getStatus().getMappa().trovaCasella(0);
		Giocatore giocatore = core.getStatus().getGiocatoreCorrente();
		posizionaPastore(giocatore, 0, casella);
		Pastore pastore = giocatore.getPastori().get(0);
		boolean actual = core.isTerrenoComprabile(pastore, Terreno.SHEEPSBURG);
		assertEquals(false, actual);
		// Simulo che siano stati comprati 6 terreni di questo tipo
		Terreno.COLLINA.setCosto(6);
		actual = core.isTerrenoComprabile(pastore, Terreno.COLLINA);
		assertEquals(false, actual);
		Terreno.COLLINA.setCosto(5);
		actual = core.isTerrenoComprabile(pastore, Terreno.COLLINA);
		assertEquals(true, actual);
		actual = core.isTerrenoComprabile(pastore, Terreno.PALUDE);
		assertEquals(false, actual);		
	}

	@Test
	public void testCompraTerreno() {
		Casella casella = (Casella) core.getStatus().getMappa().trovaCasella(0);
		Giocatore giocatore = core.getStatus().getGiocatoreCorrente();
		giocatore.setTerrenoIniziale(Terreno.PALUDE);
		posizionaPastore(giocatore, 0, casella);
		Pastore pastore = giocatore.getPastori().get(0);
		pastore.setTerrenoComprato(true);
		// non puo comprare
		boolean actual = core.compraTerreno(pastore, Terreno.COLLINA);
		assertEquals(false, actual);
		// puo comprare
		pastore.setTerrenoComprato(false);
		Terreno.COLLINA.setCosto(1);
		int balance = giocatore.getMonete();
		actual = core.compraTerreno(pastore, Terreno.COLLINA);
		assertEquals(true, actual);
		int numero = giocatore.getTerreniComprati().get(Terreno.COLLINA);
		assertEquals(1, numero);
		numero = giocatore.getMonete();
		assertEquals(balance-1, numero);
		assertEquals(1, pastore.getNumeroMossa());
		assertEquals(true, pastore.isTerrenoComprato());
		// non ha abbastanza soldi
		Terreno.COLLINA.setCosto(5);
		giocatore.setMonete(3);
		actual = core.compraTerreno(pastore, Terreno.COLLINA);
		assertEquals(false, actual);
		// non è vicino
		actual = core.compraTerreno(pastore, Terreno.PALUDE);
		assertEquals(false, actual);
		// sheepsburg
		actual = core.compraTerreno(pastore, Terreno.SHEEPSBURG);
		assertEquals(false, actual);
		pastore.setNumeroMossa(3);
		Terreno.COLLINA.setCosto(1);
		actual = core.compraTerreno(pastore, Terreno.COLLINA);
		assertEquals(false, actual);
	}

	@Test
	public void testPuoAccoppiare() {
		Casella casella = (Casella) core.getStatus().getMappa().trovaCasella(0);
		Giocatore giocatore = core.getStatus().getGiocatoreCorrente();
		posizionaPastore(giocatore, 0, casella);
		Pastore pastore = giocatore.getPastori().get(0);
		Regione regione = (Regione) core.getStatus().getMappa().trovaRegione(0);
		regione.setArieti(0);
		boolean actual = core.puoAccoppiare(pastore, regione);
		assertEquals(false, actual);
		regione.setArieti(1);
		regione.setPecore(0);
		actual = core.puoAccoppiare(pastore, regione);
		assertEquals(false, actual);
		regione.setPecore(1);
		pastore.setPecoraAccoppiata(true);
		actual = core.puoAccoppiare(pastore, regione);
		assertEquals(false, actual);
		pastore.setPecoraAccoppiata(false);
		pastore.setNumeroMossa(3);
		actual = core.puoAccoppiare(pastore, regione);
		assertEquals(false, actual);
		pastore.setNumeroMossa(0);
		actual = core.puoAccoppiare(pastore, regione);
		assertEquals(true, actual);
		pastore.setPecoraAccoppiata(false);
		regione = (Regione) core.getStatus().getMappa().trovaRegione(13);
		actual = core.puoAccoppiare(pastore, regione);
		assertEquals(false, actual);
		pastore.setPastoreMosso(false);
		pastore.setNumeroMossa(2);
		actual = core.puoAccoppiare(pastore, regione);
		assertEquals(false, actual);
	}

	@Test
	public void testAccoppia() {
		Casella casella = (Casella) core.getStatus().getMappa().trovaCasella(0);
		Giocatore giocatore = core.getStatus().getGiocatoreCorrente();
		posizionaPastore(giocatore, 0, casella);
		Pastore pastore = giocatore.getPastori().get(0);
		Regione regione = (Regione) core.getStatus().getMappa().trovaRegione(0);
		int accoppiamenti = 0;
		while(pastore.getNumeroMossa() < 3) {
			try {
				pastore.setPecoraAccoppiata(false);
				pastore.setPastoreMosso(true);
				boolean result = core.accoppia(pastore, regione);
				assertEquals(true, result);
				accoppiamenti++;
			} catch (DadoException e) {
				assertTrue(!e.equals(""+pastore.getCasella()));
			}
			assertEquals(accoppiamenti, regione.getAgnelli().size());
			assertEquals(accoppiamenti, core.getStatus().getAgnelli().size());
		}			
	}

	@Test
	public void testVerificaAgnelli() {
		Giocatore giocatore = core.getStatus().getGiocatoreCorrente();
		Regione regione = (Regione) core.getStatus().getMappa().trovaRegione(0);
		Regione regione2 = (Regione) core.getStatus().getMappa().trovaRegione(1);
		core.getStatus().setTurno(1);
		Agnello agnello = new Agnello(core.getStatus().getTurno(), giocatore, regione);
		regione.getAgnelli().add(agnello);
		core.getStatus().getAgnelli().add(agnello);
		agnello = new Agnello(core.getStatus().getTurno(), giocatore, regione);
		regione.getAgnelli().add(agnello);
		core.getStatus().getAgnelli().add(agnello);
		agnello = new Agnello(core.getStatus().getTurno()+1, giocatore, regione2);
		regione2.getAgnelli().add(agnello);
		core.getStatus().getAgnelli().add(agnello);
		core.verificaAgnelli();
		int number = regione.getArieti()+regione.getPecore();
		int number2 = regione2.getArieti()+regione2.getPecore();
		assertEquals(3, core.getStatus().getAgnelli().size());
		assertEquals(2, regione.getAgnelli().size());
		assertEquals(1, regione2.getAgnelli().size());
		assertEquals(number, regione.getArieti()+regione.getPecore());
		assertEquals(number2, regione2.getArieti()+regione2.getPecore());
		
		core.getStatus().setGiocatoreCorrente(giocatore);
		core.getStatus().setTurno(2);
		core.verificaAgnelli();
		assertEquals(3, core.getStatus().getAgnelli().size());
		assertEquals(2, regione.getAgnelli().size());
		assertEquals(1, regione2.getAgnelli().size());
		assertEquals(number, regione.getArieti()+regione.getPecore());
		assertEquals(number2, regione2.getArieti()+regione2.getPecore());
		core.getStatus().setTurno(3);
		core.verificaAgnelli();

		assertEquals(1, core.getStatus().getAgnelli().size());
		assertEquals(0, regione.getAgnelli().size());
		assertEquals(1, regione2.getAgnelli().size());
		assertEquals(number+2, regione.getArieti()+regione.getPecore());
		assertEquals(number2, regione2.getArieti()+regione2.getPecore());
		core.getStatus().setTurno(4);
		core.verificaAgnelli();
		assertEquals(0, core.getStatus().getAgnelli().size());
		assertEquals(0, regione.getAgnelli().size());
		assertEquals(0, regione2.getAgnelli().size());
		assertEquals(number2+1, regione2.getArieti()+regione2.getPecore());		
	}

	@Test
	public void testPuoEseguire() {
		Casella casella = (Casella) core.getStatus().getMappa().trovaCasella(0);
		Giocatore giocatore = core.getStatus().getGiocatoreCorrente();
		posizionaPastore(giocatore, 0, casella);
		Pastore pastore = giocatore.getPastori().get(0);
		pastore.setNumeroMossa(3);
		boolean actual = core.puoEseguire(pastore);
		assertEquals(false, actual);
		pastore.setNumeroMossa(2);
		actual = core.puoEseguire(pastore);
		assertEquals(false, actual);
		pastore.setPastoreMosso(true);
		actual = core.puoEseguire(pastore);
		assertEquals(true, actual);
		pastore.setNumeroMossa(0);
		pastore.setPastoreMosso(false);
		actual = core.puoEseguire(pastore);
		assertEquals(true, actual);
	}

	@Test
	public void testTestimoniSparatoria() {
		List<Pastore> pastori = new ArrayList<>();
		Casella casella = (Casella) core.getStatus().getMappa().trovaCasella(0);
		Giocatore giocatore = core.getStatus().getGiocatoreCorrente();
		Regione regione = (Regione) core.getStatus().getMappa().trovaRegione(0);
		posizionaPastore(giocatore, 0, casella);
		Pastore pastore = giocatore.getPastori().get(0);
		giocatore = new Giocatore(0, Colore.ROSSO);
		Pastore pastore2 = new Pastore(giocatore, 0);
		pastori.add(pastore2);
		Casella casella2 = (Casella) core.getStatus().getMappa().trovaCasella(1);
		casella2.setPastore(pastore2);
		casella2 = (Casella) core.getStatus().getMappa().trovaCasella(2);
		giocatore = new Giocatore(0, Colore.VERDE);
		pastore2 = new Pastore(giocatore, 1);
		pastori.add(pastore2);
		casella2.setPastore(pastore2);
		List<Pastore> actual = core.testimoniSparatoria(pastore, regione);
		for(int i = 0; i < actual.size(); i++) {
			Pastore tmp = actual.get(i);
			boolean ok = false;
			for(int j = 0; j < pastori.size(); j++) {
				if(tmp.equals(pastori.get(j))) {
					ok = true;
					break;
				}
			}
			assertTrue(ok);
		}
		
	}

	@Test
	public void testPuoSparare() {
		Casella casella = (Casella) core.getStatus().getMappa().trovaCasella(0);
		Giocatore giocatore = core.getStatus().getGiocatoreCorrente();
		posizionaPastore(giocatore, 0, casella);
		Pastore pastore = giocatore.getPastori().get(0);
		pastore.setHaSparato(true);
		Regione regione = (Regione) core.getStatus().getMappa().trovaRegione(15);
		boolean actual = core.puoSparare(pastore, regione, Animale.ARIETE);
		assertEquals(false, actual);
		pastore.setHaSparato(false);
		pastore.setNumeroMossa(2);
		actual = core.puoSparare(pastore, regione, Animale.ARIETE);
		assertEquals(false, actual);
		pastore.setNumeroMossa(0);
		assertEquals(false, actual);
		regione =  (Regione) core.getStatus().getMappa().trovaRegione(0);
		actual = core.puoSparare(pastore, regione, Animale.ARIETE);
		assertEquals(true, actual);
		regione.setArieti(0);
		actual = core.puoSparare(pastore, regione, Animale.ARIETE);
		assertEquals(false, actual);
		regione.setArieti(1);
		regione.setPecore(0);
		actual = core.puoSparare(pastore, regione, Animale.PECORA);
		assertEquals(false, actual);
		regione.setPecore(1);
		Giocatore n = new Giocatore(4, Colore.BLU);
		Pastore p = new Pastore(n, 0);
		Casella c = (Casella) core.getStatus().getMappa().trovaCasella(1);
		c.setPastore(p);
		giocatore.setMonete(30);
		actual = core.puoSparare(pastore, regione, Animale.PECORA);
		assertEquals(true, actual);
		giocatore.setMonete(0);
		actual = core.puoSparare(pastore, regione, Animale.PECORA);
		assertEquals(true, actual);
	}

	@Test
	public void testCorrompi() {
		try {
			Giocatore giocatore1 = new Giocatore(2, Colore.BLU);
			Pastore pastore1 = new Pastore(giocatore1, 0);
			Giocatore giocatore2 = new Giocatore(0, Colore.ROSSO);
			Pastore pastore2 = new Pastore(giocatore2, 0);
			int m1 = giocatore1.getMonete();
			int m2 = giocatore2.getMonete();
			core.corrompi(pastore1, pastore2);
			assertEquals(m1, giocatore1.getMonete());
			assertEquals(m2, giocatore2.getMonete());
		} catch(NullPointerException e) {
			core.getStatus().getMappa().trovaRegione(0);
		}
	}

	@Test
	public void testSparatoria() {
		Giocatore giocatore1 = new Giocatore(20, Colore.BLU);
		Pastore pastore1 = new Pastore(giocatore1, 0);
		Giocatore giocatore2 = new Giocatore(20, Colore.ROSSO);
		Pastore pastore2 = new Pastore(giocatore2, 0);
		Casella c1 = (Casella) core.getStatus().getMappa().trovaCasella(0);
		pastore1.setCasella(c1);
		c1.setPastore(pastore1);
		Casella c2 = (Casella) core.getStatus().getMappa().trovaCasella(1);
		pastore2.setCasella(c2);
		c2.setPastore(pastore2);
		Regione r = (Regione) core.getStatus().getMappa().trovaRegione(0);
		try {
			int actual = core.sparatoria(pastore1, r, Animale.PECORA);
			assertTrue(actual >= 0);
			assertEquals(0, r.getPecore());
		} catch (DadoException e) {
			assertTrue(!e.equals(c1.getValue()));
		} catch (NullPointerException e) {
			core.getStatus().getMappa().trovaRegione(0);
		}
	}

	@Test
	public void testPuoVendere() {
		Casella casella = (Casella) core.getStatus().getMappa().trovaCasella(0);
		Giocatore giocatore = core.getStatus().getGiocatoreCorrente();
		posizionaPastore(giocatore, 0, casella);
		Pastore pastore = giocatore.getPastori().get(0);
		boolean actual = core.puoVendere(pastore, Terreno.COLLINA, 1, 5);
		assertEquals(false, actual);
		actual = core.puoVendere(pastore, Terreno.COLLINA, 2, 1);
		assertEquals(false, actual);
		giocatore.setTerrenoIniziale(Terreno.COLLINA);
		actual = core.puoVendere(pastore, Terreno.COLLINA, 1, 1);
		assertEquals(true, actual);
		giocatore.addTerreno(Terreno.COLLINA, 1);
		actual = core.puoVendere(pastore, Terreno.COLLINA, 2, 2);
		assertEquals(true, actual);
	}

	@Test
	public void testVendi() {
		Giocatore giocatore = core.getStatus().getGiocatoreCorrente();
		Pastore pastore = giocatore.getPastori().get(0);
		giocatore.getTerreniComprati().put(Terreno.COLLINA, 2);
		giocatore.getTerreniComprati().put(Terreno.FORESTA, 2);
		boolean actual = core.vendi(pastore, Terreno.COLLINA, 1, 3);
		assertEquals(true, actual);
		OggettoVendita vendita = core.getStatus().getCompravendita().get(0);
		assertEquals(giocatore, vendita.getGiocatore());
		assertEquals(3, vendita.getPrezzo());
		assertEquals(Terreno.COLLINA, vendita.getTipoTerreno());
		assertEquals(1, vendita.getQuantita());
		actual = core.vendi(pastore, Terreno.COLLINA, 1, 3);
		assertEquals(true, actual);
		vendita = core.getStatus().getCompravendita().get(0);
		assertEquals(giocatore, vendita.getGiocatore());
		assertEquals(3, vendita.getPrezzo());
		assertEquals(Terreno.COLLINA, vendita.getTipoTerreno());
		assertEquals(2, vendita.getQuantita());
		
		actual = core.vendi(pastore, Terreno.FORESTA, 1, 3);
		assertEquals(true, actual);
		vendita = core.getStatus().getCompravendita().get(1);
		assertEquals(giocatore, vendita.getGiocatore());
		assertEquals(3, vendita.getPrezzo());
		assertEquals(Terreno.FORESTA, vendita.getTipoTerreno());
		assertEquals(1, vendita.getQuantita());
		actual = core.vendi(pastore, Terreno.FORESTA, 1, 3);
		assertEquals(true, actual);
		vendita = core.getStatus().getCompravendita().get(1);
		assertEquals(giocatore, vendita.getGiocatore());
		assertEquals(3, vendita.getPrezzo());
		assertEquals(Terreno.FORESTA, vendita.getTipoTerreno());
		assertEquals(2, vendita.getQuantita());
	}

	@Test
	public void testSearchVendita() {
		Giocatore giocatore = core.getStatus().getGiocatoreCorrente();
		OggettoVendita vendita1 = new OggettoVendita(giocatore, Terreno.COLLINA, 1, 1);
		core.getStatus().getCompravendita().add(vendita1);
		OggettoVendita vendita2 = new OggettoVendita(giocatore, Terreno.MONTAGNA, 1, 1);
		core.getStatus().getCompravendita().add(vendita2);
		OggettoVendita actual = core.searchVendita(giocatore, Terreno.COLLINA);
		assertEquals(vendita1, actual);
		actual = core.searchVendita(giocatore, Terreno.MONTAGNA);
		assertEquals(vendita2, actual);
		actual = core.searchVendita(giocatore, Terreno.PALUDE);
		assertEquals(null, actual);
	}

	@Test
	public void testCompra() {
		try {
			Giocatore giocatore1 = new Giocatore(20, Colore.BLU);
			Pastore pastore1 = new Pastore(giocatore1, 0);
			giocatore1.getPastori().add(pastore1);
			Giocatore giocatore2 = new Giocatore(20, Colore.ROSSO);
			giocatore2.setMonete(1);
			giocatore1.setTerrenoIniziale(Terreno.COLLINA);
			giocatore1.getTerreniComprati().put(Terreno.COLLINA, 2);
			giocatore2.setTerrenoIniziale(Terreno.COLLINA);
			core.vendi(pastore1, Terreno.COLLINA, 2, 3);
			boolean actual = core.compra(giocatore2, giocatore1, Terreno.MONTAGNA, 2);
			assertEquals(false, actual);
			actual = core.compra(giocatore2, giocatore1, Terreno.COLLINA, 2);
			assertEquals(false, actual);
			giocatore2.setMonete(20);
			actual = core.compra(giocatore2, giocatore1, Terreno.COLLINA, 2);
			assertEquals(false, actual);
			assertEquals(new Integer(1), giocatore2.getTerreniComprati().get(Terreno.COLLINA));
			assertEquals(new Integer(2), giocatore1.getTerreniComprati().get(Terreno.COLLINA));
			assertEquals(0, giocatore1.getTerreniVendita().size());
			assertEquals(0, core.getStatus().getCompravendita().size());
			assertEquals(20, giocatore2.getMonete());
			assertEquals(20, giocatore1.getMonete());
		} catch(NullPointerException e ) {
			core.getStatus().getMappa().trovaRegione(0);
		}
	}
	
	@Test
	public void testCreaGiocatori() {
		List<Giocatore> giocatori = core.getStatus().getGiocatori();
		for(int i = 0; i < giocatori.size(); i++) {
			giocatori.remove(i);
			i--;
		}
		core.getStatus().setNumeroGiocatori(2);
		List<Colore> colori = new ArrayList<>();
		colori.add(Colore.ROSSO);
		colori.add(Colore.ROSSO);
		colori.add(Colore.BLU);
		try {
			core.creaGiocatori(colori);
			fail("Genera eccezzione");
		} catch (ColorException e) {
			assertEquals("Colori giocatori non unici!", e.getMessage());
		}
		colori.remove(1);
		colori.add(Colore.GIALLO);
		try {
			core.creaGiocatori(colori);
			fail("Genera eccezzione");
		} catch (ColorException e) {
			assertEquals("Numero di giocatori errato!", e.getMessage());
		}
		colori.remove(1);
		try {
			core.creaGiocatori(colori);
			assertEquals(core.getStatus().getNumeroGiocatori(), giocatori.size());
		} catch (ColorException e) {
			fail("Exception");
		}
	}

	@Test
	public void testPosizionaPastore() {
		Casella casella = (Casella) core.getStatus().getMappa().trovaCasella(0);
		Giocatore giocatore = core.getStatus().getGiocatoreCorrente();
		core.posizionaPastore(giocatore, 0, casella);
		Pastore pastore = giocatore.getPastori().get(0);
		assertEquals(casella, pastore.getCasella());
		Casella casella2 = (Casella) core.getStatus().getMappa().trovaCasella(1);
		core.posizionaPastore(giocatore, 0, casella2);
		assertEquals(casella, pastore.getCasella());
		
	}
	
	@Test
	public void testColoriUnici() {
		List<Colore> col = new ArrayList<>();
		col.add(Colore.ROSSO);
		col.add(Colore.ROSSO);
		col.add(Colore.BLU);
		boolean actual = core.coloriUnici(col);
		assertEquals(false, actual);
		col.remove(1);
		actual = core.coloriUnici(col);
		assertEquals(true, actual);
	}
	
	@Test
	public void testCostoMaxSparatoria() {
		Method method = null;
		Regione regione = (Regione) core.getStatus().getMappa().trovaRegione(0);
		Casella casella = (Casella) core.getStatus().getMappa().trovaCasella(0);
		Giocatore giocatore = core.getStatus().getGiocatoreCorrente();
		Pastore pastore = giocatore.getPastori().get(0);
		core.posizionaPastore(giocatore, 0, casella);
		try {
			method = GameCore.class.getDeclaredMethod("costoMaxSparatoria", Pastore.class, Regione.class);
		} catch (NoSuchMethodException | SecurityException e) {
			fail("exception");
		}
		method.setAccessible(true);
		int actual;
		try {
			actual = (int) method.invoke(core, pastore, regione);
			assertEquals(0, actual);
		} catch (IllegalAccessException e) {
			fail("Exception");
		} catch (IllegalArgumentException e) {
			fail("Exception");
		} catch (InvocationTargetException e) {
			fail("Exception");
		}
		Giocatore tmp = new Giocatore(0, Colore.ROSSO);
		Pastore p = new Pastore(tmp, 0);
		Casella ctmp = (Casella) core.getStatus().getMappa().trovaCasella(1);
		ctmp.setPastore(p);
		try {
			actual = (int) method.invoke(core, pastore, regione);
			assertEquals(0, actual);
		} catch (IllegalAccessException e) {
			fail("Exception");
		} catch (IllegalArgumentException e) {
			fail("Exception");
		} catch (InvocationTargetException e) {
			fail("Exception");
		}
		tmp = new Giocatore(0, Colore.GIALLO);
		p = new Pastore(tmp, 0);
		ctmp = (Casella) core.getStatus().getMappa().trovaCasella(2);
		ctmp.setPastore(p);
		try {
			actual = (int) method.invoke(core, pastore, regione);
			assertEquals(0, actual);
		} catch (IllegalAccessException e) {
			fail("Exception");
		} catch (IllegalArgumentException e) {
			fail("Exception");
		} catch (InvocationTargetException e) {
			fail("Exception");
		}
	}
	
	@Test
	public void testCreaGiocatore() {
		List<Giocatore> giocatori = core.getStatus().getGiocatori();
		for(int i = 0; i < giocatori.size(); i++) {
			giocatori.remove(i);
			i--;
		}
		core.getStatus().setNumeroGiocatori(2);
		Method method = null;
		try {
			method  = GameCore.class.getDeclaredMethod("creaGiocatore", Colore.class, Terreno.class);
		} catch (NoSuchMethodException | SecurityException e) {
			fail("exception");
		}
		method.setAccessible(true);
		try {
			method.invoke(core, Colore.BLU, Terreno.COLLINA);
			assertEquals(30, giocatori.get(0).getMonete());
			assertEquals(new Integer(1), giocatori.get(0).getTerreniComprati().get(Terreno.COLLINA));
			assertEquals(2, giocatori.get(0).getPastori().size());
		} catch (IllegalAccessException e) {
			fail("Exception");
		} catch (IllegalArgumentException e) {
			fail("Exception");
		} catch (InvocationTargetException e) {
			fail("Exception");
		}
		core.getStatus().setNumeroGiocatori(3);
		try {
			method.invoke(core, Colore.ROSSO, Terreno.MONTAGNA);
			assertEquals(20, giocatori.get(1).getMonete());
			assertEquals(new Integer(1), giocatori.get(1).getTerreniComprati().get(Terreno.MONTAGNA));
			assertEquals(1, giocatori.get(1).getPastori().size());
		} catch (IllegalAccessException e) {
			fail("Exception");
		} catch (IllegalArgumentException e) {
			fail("Exception");
		} catch (InvocationTargetException e) {
			fail("Exception");
		}
		
	}
		
	@Test
	public void testMovimentoAnimale() {
		Giocatore giocatore = core.getStatus().getGiocatoreCorrente();
		Pastore pastore = giocatore.getPastori().get(0);
		Casella casella = (Casella) core.getStatus().getMappa().trovaCasella(0);
		Regione regione1 = (Regione) core.getStatus().getMappa().trovaRegione(0);
		Regione regione2 = (Regione) core.getStatus().getMappa().trovaRegione(16);
		core.posizionaPastore(giocatore, 0, casella);
		Method method = null;
		try {
			method  = GameCore.class.getDeclaredMethod("movimentoAnimale", Pastore.class, Regione.class, Regione.class, Animale.class);
		} catch (NoSuchMethodException | SecurityException e) {
			fail("exception");
		}
		method.setAccessible(true);
		try {
			boolean actual = (boolean) method.invoke(core, pastore, regione1, regione2, Animale.ARIETE);
			assertEquals(true, actual);
			assertEquals(0, regione1.getArieti());
			assertEquals(2, regione2.getArieti());
		} catch (IllegalAccessException e) {
			fail("Exception");
		} catch (IllegalArgumentException e) {
			fail("Exception");
		} catch (InvocationTargetException e) {
			fail("Exception");
		}
		try {
			boolean actual = (boolean) method.invoke(core, pastore, regione1, regione2, Animale.PECORA);
			assertEquals(true, actual);
			assertEquals(0, regione1.getPecore());
			assertEquals(2, regione2.getPecore());
		} catch (IllegalAccessException e) {
			fail("Exception");
		} catch (IllegalArgumentException e) {
			fail("Exception");
		} catch (InvocationTargetException e) {
			fail("Exception");
		}
		core.getStatus().setPosizionePecoraNera(regione1);
		regione1.setPecoraNera(true);
		try {
			boolean actual = (boolean) method.invoke(core, pastore, regione1, regione2, Animale.PECORA_NERA);
			assertEquals(true, actual);
			assertEquals(false, regione1.hasPecoraNera());
			assertEquals(true, regione2.hasPecoraNera());
		} catch (IllegalAccessException e) {
			fail("Exception");
		} catch (IllegalArgumentException e) {
			fail("Exception");
		} catch (InvocationTargetException e) {
			fail("Exception");
		}
		try {
			boolean actual = (boolean) method.invoke(core, pastore, regione1, regione2, Animale.ARIETE);
			assertEquals(false, actual);
		} catch (IllegalAccessException e) {
			fail("Exception");
		} catch (IllegalArgumentException e) {
			fail("Exception");
		} catch (InvocationTargetException e) {
			fail("Exception");
		}
		try {
			boolean actual = (boolean) method.invoke(core, pastore, regione1, regione2, Animale.PECORA);
			assertEquals(false, actual);
		} catch (IllegalAccessException e) {
			fail("Exception");
		} catch (IllegalArgumentException e) {
			fail("Exception");
		} catch (InvocationTargetException e) {
			fail("Exception");
		}
		try {
			boolean actual = (boolean) method.invoke(core, pastore, regione1, regione2, Animale.PECORA_NERA);
			assertEquals(false, actual);
		} catch (IllegalAccessException e) {
			fail("Exception");
		} catch (IllegalArgumentException e) {
			fail("Exception");
		} catch (InvocationTargetException e) {
			fail("Exception");
		}
	}
	
	@Test
	public void testMuoviAnimale() {
		Giocatore giocatore = core.getStatus().getGiocatoreCorrente();
		Pastore pastore = giocatore.getPastori().get(0);
		Casella casella = (Casella) core.getStatus().getMappa().trovaCasella(0);
		Regione regione1 = (Regione) core.getStatus().getMappa().trovaRegione(0);
		Regione regione2 = (Regione) core.getStatus().getMappa().trovaRegione(16);
		core.posizionaPastore(giocatore, 0, casella);
		Method method = null;
		try {
			method  = GameCore.class.getDeclaredMethod("muoviAnimale", Pastore.class, Regione.class, Regione.class, Animale.class);
		} catch (NoSuchMethodException | SecurityException e) {
			fail("exception");
		}
		method.setAccessible(true);
		try {
			boolean actual = (boolean) method.invoke(core, pastore, regione1, regione2, Animale.ARIETE);
			assertEquals(true, actual);
			assertEquals(1, pastore.getNumeroMossa());
			assertEquals(true, pastore.isPecoraMossa());
		} catch (IllegalAccessException e) {
			fail("Exception");
		} catch (IllegalArgumentException e) {
			fail("Exception");
		} catch (InvocationTargetException e) {
			fail("Exception");
		}
	}
}
