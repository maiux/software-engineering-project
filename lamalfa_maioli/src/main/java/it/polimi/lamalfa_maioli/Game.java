package it.polimi.lamalfa_maioli;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import it.polimi.lamalfa_maioli.controller.GameCore;
import it.polimi.lamalfa_maioli.controller.exceptions.ColorException;
import it.polimi.lamalfa_maioli.controller.exceptions.MyRuntimeException;
import it.polimi.lamalfa_maioli.controller.rete.rmi.ReteRMI;
import it.polimi.lamalfa_maioli.controller.rete.socket.ReteSocket;
import it.polimi.lamalfa_maioli.model.Colore;
import it.polimi.lamalfa_maioli.model.Giocatore;
import it.polimi.lamalfa_maioli.model.Pastore;
import it.polimi.lamalfa_maioli.model.Regione;
import it.polimi.lamalfa_maioli.view.CompravenditaCompra;
import it.polimi.lamalfa_maioli.view.ControllerMappa;
import it.polimi.lamalfa_maioli.view.CompravenditaVendi;
import it.polimi.lamalfa_maioli.view.ImpostazioniPartita;
import it.polimi.lamalfa_maioli.view.ImpostazioniPartita.TipoConnessione;
import it.polimi.lamalfa_maioli.view.ViewRegione;
import it.polimi.lamalfa_maioli.view.ControllerMappa.Azione;
import it.polimi.lamalfa_maioli.view.ImpostazioniPartita.TipoPartita;

/**
 * Classe che avvia il gioco
 * @author lamalfa_maioli
 *
 */
public class Game {

	/**
	 * Costruttore privato
	 */
	private Game() {
		// Null
	}
	
	/**
	 * Main
	 */
	public static void main(String[] args) {
		ImpostazioniPartita settings = new ImpostazioniPartita();
		while(!settings.getUserOk()) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// Ignora 
			}
		}
		if(settings.getGameType().equals(TipoPartita.LOCALE)) {
			int numeroGiocatori = settings.getNumeroGiocatori();
			GameCore core = new GameCore(numeroGiocatori);
			List<Colore> colori = Colore.getRandoms();
			for(int i = 3;  i > numeroGiocatori-1; i--) {
				colori.remove(i);
			}
			try {
				core.creaGiocatori(colori);
			} catch (ColorException e) {
				throw new MyRuntimeException("Errore con i giocatori", e);
			}
			ControllerMappa frame = new ControllerMappa(core);
			frame.allButtonsDisabled();
			frame.setVisible(true);
			core.impostaGiocatoreIniziale();
			Giocatore corrente = null;
			for(int i = 0; i < core.getStatus().getNumeroGiocatori(); i++) {
				corrente = core.getStatus().getGiocatoreCorrente();
				frame.requestPastore(corrente, core, null);
				core.getStatus().setGiocatoreCorrente(core.getStatus().prossimoGiocatore());
			}
			int turno = core.getStatus().getTurno();
			while(!core.getStatus().gameEnd()) {
				Giocatore tmp = core.getStatus().getGiocatoreCorrente();
				if(!tmp.equals(corrente)) {
					corrente = tmp;
					turno++;
					core.getStatus().setTurno(turno);
					if(tmp.equals(core.getStatus().getGiocatoreIniziale()) && core.getStatus().getNumeroRecinti() <= 0) {
						frame.endGame(core, null);
					} else {
						if(frame.partitaIniziata()) {
							frame.muoviPecoraNera(core, null);
							frame.muoviLupo(core, null);
							List<Regione> update = core.verificaAgnelli();
							for(int i = 0; i < update.size(); i++) {
								ViewRegione reg = frame.getViewRegione(update.get(i));
								reg.getAgnello().update();
								reg.getAriete().update();
								reg.getPecoraBianca().update();
							}
						}
						JOptionPane.showMessageDialog(frame, "Turno del giocatore "+corrente.getColore());
						frame.updateTurno(corrente.getColore().toString());
						frame.updateNumeroTurno(turno+"");
					}
				}
				Pastore pastoreCorrente = corrente.getPastoreCorrente();
				frame.updateContatori(corrente, core);
				frame.updateButtons(corrente, core);
				if(pastoreCorrente == null) {
					frame.richiediPastoreCorrente(corrente, null);
				}
				switch(frame.getAzione()) {
					case MUOVI_PASTORE:
						frame.muoviPastore(corrente, core, null);
						break;
					case MUOVI_PECORA:
						frame.muoviPecora(corrente, core, null);
						break;
					case MUOVI_ARIETE:
						frame.muoviAriete(corrente, core, null);
						break;
					case MUOVI_PECORA_NERA:
						frame.muoviPecoraNera(corrente, core, null);
						break;
					case SPARATORIA:
						frame.sparatoria(corrente, core, null);
						break;
					case ACCOPPIA:
						frame.accoppia(corrente, core, null);
						break;
					case COMPRA_TERRENO:
						frame.compraTerreno(corrente, core, null);
						break;
					case COMPRAVENDITA:
						Object[] options = { "Compra", "Vendi", "Annulla" };
						int n = JOptionPane.showOptionDialog(frame, "Che azione intendi fare?", "Compravendita", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[2]);
						switch(n) {
							case 0:
								frame.setAzione(Azione.COMPRAVENDITA_COMPRA);
								break;
							case 1:
								frame.setAzione(Azione.COMPRAVENDITA_VENDI);
								break;
							default:
								frame.reset(core);
								break;
						}
						break;
					case COMPRAVENDITA_COMPRA:
						List<Giocatore> giocatori = core.getStatus().getGiocatori();
						List<Giocatore> compraDa = new ArrayList<>();
						for(int i = 0; i < giocatori.size(); i++) {
							if(!corrente.equals(giocatori.get(i))) {
								compraDa.add(giocatori.get(i));
							}
						}
						int indiceGiocatore = 0;
						int compraDaSize = compraDa.size();
						if(compraDaSize > 1) {
							Object[] gopt = new Object[compraDaSize];
							for(int i = 0; i < compraDaSize; i++) {
								gopt[i] = new String(compraDa.get(i).getColore().toString());
							}
							indiceGiocatore = JOptionPane.showOptionDialog(frame, "Che azione intendi fare?", "Compravendita", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, gopt, gopt[0]);
						}
						if(indiceGiocatore < 0 || indiceGiocatore >= compraDaSize) {
							frame.reset(core);
							break;
						}
						CompravenditaCompra fc = new CompravenditaCompra(core, compraDa.get(indiceGiocatore), null);
						while(!fc.getEnd()) {
							try {
								Thread.sleep(50);
							} catch (InterruptedException e) {
								// Ignora
							}
						}
						String returnFc = fc.getStatus();
						if(!"Acquisti: \n".equals(returnFc) && returnFc.length() > 0) {
							JOptionPane.showMessageDialog(frame, returnFc);
						}
						frame.reset(core);
						break;
					case COMPRAVENDITA_VENDI:
						CompravenditaVendi fv = new CompravenditaVendi(core, null);
						while(!fv.getEnd()) {
							try {
								Thread.sleep(50);
							} catch (InterruptedException e) {
								// Ignora
							}
						}
						String returnFv = fv.getStatus();
						if(!"Status vendita:\n".equals(returnFv) && returnFv.length() > 0) {
							JOptionPane.showMessageDialog(frame, returnFv);
						}
						frame.reset(core);
						break;
					case FINE_TURNO:
						if(pastoreCorrente.getNumeroMossa() < 3) {
							JOptionPane.showMessageDialog(frame, "Devi finire le tue mosse prima di terminare il turno!");
							frame.reset(core);
							break;
						} else {
							if(corrente.getPastori().size() == 2) {
								corrente.setPastoreCorrente(null);
							}
							pastoreCorrente.reset();
							core.getStatus().setGiocatoreCorrente(core.getStatus().prossimoGiocatore());
							frame.reset(core);
						}
						break;
					default:
						try {
							Thread.sleep(50);
						} catch (InterruptedException e) {
							// Ignora
						}
						break;
				}
			}			
		} else if(settings.getGameType().equals(TipoPartita.RMI)) {
			if(settings.getConnectionType().equals(TipoConnessione.SERVER)) {
				new  ReteRMI(settings.getPort());
			} else {
				new ReteRMI(settings.getHost(), settings.getPort());
			}		
		} else if(settings.getGameType().equals(TipoPartita.SOCKET)) {
			try {
				if(settings.getConnectionType().equals(TipoConnessione.SERVER)) {
					new ReteSocket(settings.getPort());
				} else {
					new ReteSocket(settings.getHost(), settings.getPort());
				}
			} catch (IOException e) {
				throw new MyRuntimeException(e);
			}			
		}		
	}	
}
