package it.polimi.lamalfa_maioli.controller.exceptions;

import java.io.Serializable;

/**
 * Eccezione sollevata per un problema coi colori
 * @author lamalfa_maioli
 *
 */
public class ColorException extends Exception implements Serializable {
	
	private static final long serialVersionUID = -6810732286426880333L;

	/**
	 * Costruttore
	 */
	public ColorException() {
		super();
	}
	
	/**
	 * Costruttore
	 * @param message messaggio per descrivere l'eccezione
	 */
	public ColorException(String message) {
		super(message);
	}
	
	/**
	 * Costruttore nel caso di eccezione ramificata
	 * @param message messaggio per descrivere l'eccezione
	 * @param e eccezione vecchia
	 */
	public ColorException(String message, Throwable cause) {
		super(message, cause);
	}
}
