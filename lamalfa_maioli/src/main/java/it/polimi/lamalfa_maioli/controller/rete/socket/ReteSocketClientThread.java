package it.polimi.lamalfa_maioli.controller.rete.socket;

import it.polimi.lamalfa_maioli.controller.GameCore;
import it.polimi.lamalfa_maioli.controller.Serializator;
import it.polimi.lamalfa_maioli.controller.exceptions.MyRuntimeException;
import it.polimi.lamalfa_maioli.model.Giocatore;

import java.io.IOException;
import java.net.Socket;

/**
 * Thread avviato dal server per gestire più client
 * @author lamalfa_maioli
 *
 */
public class ReteSocketClientThread extends Thread {
	private Socket socket;
	private ReteSocket server;
	private int num;
	
	/**
	 * Costruttore
	 * @param socket socket su cui comunicare con il client
	 * @param server server che ha avviato il thread
	 * @param num numero del giocatore
	 */
	public ReteSocketClientThread(Socket socket, ReteSocket server, int num) {
		super();
		this.server = server;
		this.socket = socket;
		this.num = num;
	}
	
	/**
	 * @see Thread#run()
	 */
	@Override	
	public void run(){
		try {
			socket.setSoTimeout(0);
			server.sendMsg(socket, ("Client numero "+num).getBytes());
		} catch (IOException e) {
			throw new MyRuntimeException(e);
		}
		while(!server.started()) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// Ignora					
			}
		}
		try {
			server.sendMsg(socket, Serializator.toString(server.getCore()));
		} catch (IOException e) {
			throw new MyRuntimeException(e);
		}
		Giocatore me = server.getCore().getStatus().getGiocatori().get(num);
		while(!server.getCore().getStatus().gameEnd()) {
			while(!server.getCore().getStatus().getGiocatoreCorrente().equals(me)) {
				try {
					if(server.getCore().getStatus().gameEnd()) {
						break;
					}
					Thread.sleep(50);
				} catch (InterruptedException e) {
					// Ignora
				}
			}
			if(server.getCore().getStatus().gameEnd()) {
				break;
			}
			try {
				server.sendMsg(socket, Serializator.toString(server.getCore()));
				GameCore test = (GameCore) Serializator.fromString(server.readMsg(socket));
				server.setCore(test);
			} catch (ClassNotFoundException e) {
				throw new MyRuntimeException(e);
			} catch (IOException e) {
				throw new MyRuntimeException(e);
			}
			
		}
		if(!server.getCore().getStatus().getGiocatoreIniziale().equals(me)){
			try {
				server.sendMsg(socket, Serializator.toString(server.getCore()));
			} catch (IOException e) {
				throw new MyRuntimeException(e);
			}
		}
	}	
}