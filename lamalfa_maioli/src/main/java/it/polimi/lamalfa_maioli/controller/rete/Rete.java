package it.polimi.lamalfa_maioli.controller.rete;

import java.io.IOException;

/**
 * Interfaccia standard da usare per la rete
 * @author lamalfa_maioli
 *
 */
public interface Rete {
	/**
	 * Inizializza e avvia il server che rimane in ascolto per uno o più client.
	 * @throws IOException 
	 */
	public void startServer() throws IOException;
	
	/**
	 * Inizializza e avvia il client che si mette in comunicazione con il server.
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public void startClient() throws ClassNotFoundException, IOException;
}
