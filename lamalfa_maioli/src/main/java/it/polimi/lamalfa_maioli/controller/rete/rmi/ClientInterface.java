package it.polimi.lamalfa_maioli.controller.rete.rmi;

import java.rmi.Remote;

/**
 * Interfaccia del client. Serve per poter prendere il numero del giocatore associato al client che si connette
 * @author lamalfa_maioli
 *
 */
public interface ClientInterface extends Remote {
	
}
