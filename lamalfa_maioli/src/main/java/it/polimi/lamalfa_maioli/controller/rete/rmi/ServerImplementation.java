package it.polimi.lamalfa_maioli.controller.rete.rmi;

import it.polimi.lamalfa_maioli.controller.GameCore;
import it.polimi.lamalfa_maioli.controller.GameCore.Animale;
import it.polimi.lamalfa_maioli.controller.exceptions.ColorException;
import it.polimi.lamalfa_maioli.controller.exceptions.DadoException;
import it.polimi.lamalfa_maioli.controller.exceptions.MyRuntimeException;
import it.polimi.lamalfa_maioli.model.Casella;
import it.polimi.lamalfa_maioli.model.Colore;
import it.polimi.lamalfa_maioli.model.Giocatore;
import it.polimi.lamalfa_maioli.model.Pastore;
import it.polimi.lamalfa_maioli.model.Regione;
import it.polimi.lamalfa_maioli.model.Terreno;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementazione dell'interfaccia remota per utilizzare il server tramite rmi
 * @author lamalfa_maioli
 *
 */
public class ServerImplementation extends UnicastRemoteObject implements ServerInterface {
	
	private static final long serialVersionUID = 6667956371479445571L;
	private List<ClientInterface> clients = new ArrayList<ClientInterface>();
	private GameCore core;
	private boolean start = false;
	
	/**
	 * Costruttore
	 * @throws RemoteException
	 */
	protected ServerImplementation() throws RemoteException {
		super(0);
	}
	
	/**
	 * @see ServerInterface#inizializza()
	 */
	@Override
	public void inizializza() throws RemoteException {
		List<Colore> colori = Colore.getRandoms();
		List<Colore> coloriEffettivi = new ArrayList<>();
		int numeroGiocatori = clients.size() + 1;
		GameCore gcore = new GameCore(numeroGiocatori);
		for(int i = 0; i < numeroGiocatori; i++) {
			coloriEffettivi.add(colori.get(i));
		}
		try {
			gcore.creaGiocatori(coloriEffettivi);
		} catch (ColorException e) {
			throw new MyRuntimeException(e);
		}
		gcore.impostaGiocatoreIniziale();
		this.core = gcore;
		start = true;
	}
	
	/**
	 * @see ServerInterface#addClient(ClientInterface)
	 */
	@Override
	public boolean addClient(ClientInterface client) throws RemoteException {
		if(core == null && clients.size() < 3) {
			clients.add(client);
			return true;
		} 
		return false;
	}

	/**
	 * @see ServerInterface#myNum(ClientInterface)
	 */
	@Override
	public int myNum(ClientInterface client) throws RemoteException {
		return clients.indexOf(client)+1;
	}

	/**
	 * @see ServerInterface#numeroClients()
	 */
	@Override
	public int numeroClients() {
		return clients.size();
	}
	

	/**
	 * @see ServerInterface#core()
	 */
	@Override
	public GameCore core() throws RemoteException {
		return this.core;
	}

	/**
	 * @see ServerInterface#partitaIniziata()
	 */
	@Override
	public boolean partitaIniziata() throws RemoteException {
		return start;
	}

	/**
	 * @see ServerInterface#setGiocatoreCorrente(Giocatore)
	 */
	@Override
	public void setGiocatoreCorrente(Giocatore giocatore) throws RemoteException {
		core.getStatus().setGiocatoreCorrente(giocatore);
	}

	/**
	 * @see ServerInterface#posizionaPastore(Giocatore, int, Casella)
	 */
	@Override
	public void posizionaPastore(Giocatore giocatore, int i, Casella casella) throws RemoteException {
		core.posizionaPastore(giocatore, i, casella);		
	}

	/**
	 * @see ServerInterface#setTurno(int)
	 */
	@Override
	public void setTurno(int turno) throws RemoteException {
		core.getStatus().setTurno(turno);
	}

	/**
	 * @see ServerInterface#dadoPecoraNera()
	 */
	@Override
	public void dadoPecoraNera() throws RemoteException {
		core.dadoPecoraNera();
	}

	/**
	 * @see ServerInterface#dadoLupo()
	 */
	@Override
	public void dadoLupo() throws RemoteException {
		core.dadoLupo();
	}

	/**
	 * @see ServerInterface#verificaAgnelli()
	 */
	@Override
	public List<Regione> verificaAgnelli() throws RemoteException {
		return core.verificaAgnelli();
	}

	/**
	 * @see ServerInterface#setPastoreCorrente(Giocatore, Pastore)
	 */
	@Override
	public boolean setPastoreCorrente(Giocatore giocatore, Pastore pastore) throws RemoteException {
		List<Giocatore> giocatori = core.getStatus().getGiocatori();
		int g = -1;
		for(int i = 0; i < giocatori.size(); i++) {
			if(giocatori.get(i).equals(giocatore)) {
				g = i;
				break;
			}
		}
		if(g == -1) {
			return false;
		}
		return giocatori.get(g).setPastoreCorrente(pastore);
	}

	/**
	 * @see ServerInterface#muoviPastore(Pastore, Casella)
	 */
	@Override
	public boolean muoviPastore(Pastore pastore, Casella casella) throws RemoteException {
		return core.muoviPastore(pastore, casella);
	}

	/**
	 * @see ServerInterface#muoviPecora(Pastore, Regione, Regione)
	 */
	@Override
	public boolean muoviPecora(Pastore pastore, Regione regione1, Regione regione2) throws RemoteException {
		return core.muoviPecora(pastore, regione1, regione2);
	}

	/**
	 * @see ServerInterface#muoviAriete(Pastore, Regione, Regione)
	 */
	@Override
	public boolean muoviAriete(Pastore pastore, Regione regione1, Regione regione2) throws RemoteException {
		return core.muoviAriete(pastore, regione1, regione2);
	}

	/**
	 * @see ServerInterface#muoviPecoraNera(Pastore, Regione, Regione)
	 */
	@Override
	public boolean muoviPecoraNera(Pastore pastore, Regione regione1, Regione regione2) throws RemoteException {
		return core.muoviPecoraNera(pastore, regione1, regione2);
	}

	/**
	 * @see ServerInterface#sparatoria(Pastore, Regione, Animale)
	 */
	@Override
	public int sparatoria(Pastore pastore, Regione regione, Animale animale) throws RemoteException, DadoException {
		return core.sparatoria(pastore, regione, animale);
	}

	/**
	 * @see ServerInterface#accoppia(Pastore, Regione)
	 */
	@Override
	public boolean accoppia(Pastore pastore, Regione regione) throws RemoteException, DadoException {
		return core.accoppia(pastore, regione);
	}

	/**
	 * @see ServerInterface#compraTerreno(Pastore, Terreno)
	 */
	@Override
	public boolean compraTerreno(Pastore pastore, Terreno terreno) throws RemoteException {
		return core.compraTerreno(pastore, terreno);
	}

	/**
	 * @see ServerInterface#compra(Giocatore, Giocatore, Terreno, int)
	 */
	@Override
	public boolean compra(Giocatore giocatore1, Giocatore giocatore2, Terreno terreno, int quantita) throws RemoteException {
		return core.compra(giocatore1, giocatore2, terreno, quantita);
	}

	/**
	 * @see ServerInterface#vendi(Pastore, Terreno, int, int)
	 */
	@Override
	public boolean vendi(Pastore pastore, Terreno terreno, int quantita, int prezzo) throws RemoteException {
		return core.vendi(pastore, terreno, quantita, prezzo);
	}

	/**
	 * @see ServerInterface#reset(Giocatore)
	 */
	@Override
	public void reset(Giocatore giocatore) throws RemoteException {
		List<Giocatore> giocatori = core.getStatus().getGiocatori();
		int g = -1;
		for(int i = 0; i < giocatori.size(); i++) {
			if(giocatori.get(i).equals(giocatore)) {
				g = i;
				break;
			}
		}
		if(g == -1) {
			return;
		}
		giocatori.get(g).getPastoreCorrente().reset();
	}

	/**
	 * @see ServerInterface#setGameEnd(boolean)
	 */
	@Override
	public void setGameEnd(boolean status) throws RemoteException {
		core.getStatus().setGameEnd(status);
	}
	
}
