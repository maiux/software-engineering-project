package it.polimi.lamalfa_maioli.controller.exceptions;

import java.io.Serializable;

/**
 * Exception per errori di sintassi nel file xml della mappa
 * @author lamalfa_maioli
 *
 */
public class MapSyntaxException extends Exception  implements Serializable {

	private static final long serialVersionUID = -7456689979460435785L;

	/**
	 * Costruttore
	 */
	public MapSyntaxException() {
		super();
	}
	
	/**
	 * Costruttore
	 * @param message messaggio per descrivere l'eccezione
	 */
	public MapSyntaxException(String message) {
		super(message);
	}
	
	/**
	 * Costruttore nel caso di eccezione ramificata
	 * @param message messaggio per descrivere l'eccezione
	 * @param e eccezione vecchia
	 */
	public MapSyntaxException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
