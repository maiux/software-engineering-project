package it.polimi.lamalfa_maioli.controller.rete.rmi;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import it.polimi.lamalfa_maioli.controller.GameCore;
import it.polimi.lamalfa_maioli.controller.exceptions.MyRuntimeException;
import it.polimi.lamalfa_maioli.controller.rete.Rete;
import it.polimi.lamalfa_maioli.model.Agnello;
import it.polimi.lamalfa_maioli.model.Casella;
import it.polimi.lamalfa_maioli.model.GameStatus;
import it.polimi.lamalfa_maioli.model.Giocatore;
import it.polimi.lamalfa_maioli.model.GraphXML;
import it.polimi.lamalfa_maioli.model.OggettoVendita;
import it.polimi.lamalfa_maioli.model.Pastore;
import it.polimi.lamalfa_maioli.model.Regione;
import it.polimi.lamalfa_maioli.model.Terreno;
import it.polimi.lamalfa_maioli.view.CompravenditaCompra;
import it.polimi.lamalfa_maioli.view.CompravenditaVendi;
import it.polimi.lamalfa_maioli.view.ControllerMappa;
import it.polimi.lamalfa_maioli.view.ControllerMappa.Azione;
import it.polimi.lamalfa_maioli.view.AnimazionePastore;
import it.polimi.lamalfa_maioli.view.ReteClient;
import it.polimi.lamalfa_maioli.view.ReteServer;
import it.polimi.lamalfa_maioli.view.ViewCasella;
import it.polimi.lamalfa_maioli.view.ViewRegione;

import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Classe per la gestione della rete di rmi
 * @author lamalfa_maioli
 *
 */
public class ReteRMI implements Rete {
	private int port;
	private String host;
	private String name = "GameServer";
	
	/**
	 * Costruttore che avvia il server in ascolto
	 * @param port porta su cui ascoltare
	 */
	public ReteRMI(int port) {
		this.host = "localhost";
		this.port = port;
		System.setProperty("java.security.policy", "src/main/java/security.policy");
		startServer();
	}
	
	/**
	 * Costruttore che avvia il client, che si mette in ascolto col server
	 * @param host indirizzo del server
	 * @param port posta del server
	 */
	public ReteRMI(String host, int port) {
		this.host = host;
		this.port = port;
		System.setProperty("java.security.policy", "src/main/java/security.policy");
		try {
			startClient();
		} catch (ClassNotFoundException e) {
			throw new MyRuntimeException(e);
		} catch (IOException e) {
			throw new MyRuntimeException(e);
		}
	}
	
	/**
	 * @see Rete#startServer()
	 */
	@Override
	public final void startServer() {
		try {
			LocateRegistry.createRegistry(1337);
		} catch (RemoteException e) {
			throw new MyRuntimeException(e);
		}		
		
		try {
			ServerImplementation serverImplementation = new ServerImplementation();	
			Naming.rebind("//"+host+":"+port+"/"+name, serverImplementation);
			ReteServer viewServer = new ReteServer();
			while(!viewServer.getStart()) {
				try {
					viewServer.updateClientConnessi(serverImplementation.numeroClients());
					Thread.sleep(50);
				} catch (InterruptedException e) { 
					// Ignora
				}
			}
			serverImplementation.inizializza();
			int playerIndex = 0;
			Giocatore mioGiocatore = serverImplementation.core().getStatus().getGiocatori().get(playerIndex);
			ControllerMappa frame = new ControllerMappa(serverImplementation.core());
			frame.allButtonsDisabled();
			frame.setVisible(true);
			while(!serverImplementation.core().getStatus().getGiocatoreCorrente().equals(mioGiocatore)) {
				try {
					Thread.sleep(50);
					frame.updateTurno(serverImplementation.core().getStatus().getGiocatoreCorrente().getColore().toString());
					frame.updateNumeroTurno(serverImplementation.core().getStatus().getTurno()+"");
				} catch (InterruptedException e) {
					// Ignora
				}
			}
			viewPastoriInit(serverImplementation.core(), frame);
			updateView(serverImplementation.core(), frame);
			frame.requestPastore(mioGiocatore, serverImplementation.core(), null);
			serverImplementation.core().getStatus().setGiocatoreCorrente(serverImplementation.core().getStatus().prossimoGiocatore());
			while(!serverImplementation.core().pastoriInizializzati()) {
				try {
					Thread.sleep(50);
					frame.updateTurno(serverImplementation.core().getStatus().getGiocatoreCorrente().getColore().toString());
					frame.updateNumeroTurno(serverImplementation.core().getStatus().getTurno()+"");
				} catch (InterruptedException e) {
					// Ignora
				}
			}
			viewPastoriInit(serverImplementation.core(), frame);
			boolean initTurno = false;
			while(!serverImplementation.core().getStatus().gameEnd()) {
				int mossa = 0;
				while(!serverImplementation.core().getStatus().getGiocatoreCorrente().equals(mioGiocatore)) {
					initTurno = true;
					try {
						Thread.sleep(50);
						Pastore correntePastore = serverImplementation.core().getStatus().getGiocatoreCorrente().getPastoreCorrente();
						int mossaCorrente = mossa;
						if(correntePastore != null) {
							mossaCorrente = correntePastore.getNumeroMossa();
						}
						if(mossaCorrente != mossa) {
							updateView(serverImplementation.core(), frame);
							mossa = mossaCorrente;
						}
						
						frame.updateTurno(serverImplementation.core().getStatus().getGiocatoreCorrente().getColore().toString());
						frame.updateNumeroTurno(serverImplementation.core().getStatus().getTurno()+"");
						frame.allButtonsDisabled();
						frame.reset(null);						
					} catch (InterruptedException e) {
						// Ignora
					}
				}
				if(initTurno) {
					initTurno = false;
					updateView(serverImplementation.core(), frame);
					serverImplementation.core().getStatus().setTurno(serverImplementation.core().getStatus().getTurno()+1);
					if(mioGiocatore.equals(serverImplementation.core().getStatus().getGiocatoreIniziale()) && serverImplementation.core().getStatus().getNumeroRecinti() <= 0) {
						frame.endGame(serverImplementation.core(), null);
					} else {
						if(frame.partitaIniziata()) {
							frame.muoviPecoraNera(serverImplementation.core(), null);
							frame.muoviLupo(serverImplementation.core(), null);
							List<Regione> update = serverImplementation.core().verificaAgnelli();
							for(int i = 0; i < update.size(); i++) {
								ViewRegione reg = frame.getViewRegione(update.get(i));
								reg.getAgnello().update();
								reg.getAriete().update();
								reg.getPecoraBianca().update();
							}
						}
						JOptionPane.showMessageDialog(frame, "E' iniziato il tuo turno!");
						frame.updateTurno(mioGiocatore.getColore().toString());
						frame.updateNumeroTurno(serverImplementation.core().getStatus().getTurno()+"");
					}
				}
				Pastore pastoreCorrente = mioGiocatore.getPastoreCorrente();
				frame.updateContatori(mioGiocatore, serverImplementation.core());
				frame.updateButtons(mioGiocatore, serverImplementation.core());
				if(pastoreCorrente == null) {
					frame.richiediPastoreCorrente(mioGiocatore, null);
				}
				switch(frame.getAzione()) {
					case MUOVI_PASTORE:
						frame.muoviPastore(mioGiocatore, serverImplementation.core(), null);
						break;
					case MUOVI_PECORA:
						frame.muoviPecora(mioGiocatore, serverImplementation.core(), null);
						break;
					case MUOVI_ARIETE:
						frame.muoviAriete(mioGiocatore, serverImplementation.core(), null);
						break;
					case MUOVI_PECORA_NERA:
						frame.muoviPecoraNera(mioGiocatore, serverImplementation.core(), null);
						break;
					case SPARATORIA:
						frame.sparatoria(mioGiocatore, serverImplementation.core(), null);
						break;
					case ACCOPPIA:
						frame.accoppia(mioGiocatore, serverImplementation.core(), null);
						break;
					case COMPRA_TERRENO:
						frame.compraTerreno(mioGiocatore, serverImplementation.core(), null);
						break;
					case COMPRAVENDITA:
						Object[] options = { "Compra", "Vendi", "Annulla" };
						int n = JOptionPane.showOptionDialog(frame, "Che azione intendi fare?", "Compravendita", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[2]);
						switch(n) {
							case 0:
								frame.setAzione(Azione.COMPRAVENDITA_COMPRA);
								break;
							case 1:
								frame.setAzione(Azione.COMPRAVENDITA_VENDI);
								break;
							default:
								frame.reset(null);
								break;
						}
						break;
					case COMPRAVENDITA_COMPRA:
						List<Giocatore> giocatori = serverImplementation.core().getStatus().getGiocatori();
						List<Giocatore> compraDa = new ArrayList<>();
						for(int i = 0; i < giocatori.size(); i++) {
							if(!mioGiocatore.equals(giocatori.get(i))) {
								compraDa.add(giocatori.get(i));
							}
						}
						int indiceGiocatore = 0;
						int compraDaSize = compraDa.size();
						if(compraDaSize > 1) {
							Object[] gopt = new Object[compraDaSize];
							for(int i = 0; i < compraDaSize; i++) {
								gopt[i] = new String(compraDa.get(i).getColore().toString());
							}
							indiceGiocatore = JOptionPane.showOptionDialog(frame, "Che azione intendi fare?", "Compravendita", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, gopt, gopt[0]);
						}
						if(indiceGiocatore < 0 || indiceGiocatore >= compraDaSize) {
							frame.reset(null);
							break;
						}
						CompravenditaCompra fc = new CompravenditaCompra(serverImplementation.core(), compraDa.get(indiceGiocatore), null);
						while(!fc.getEnd()) {
							try {
								Thread.sleep(50);
							} catch (InterruptedException e) {
								// Ignora
							}
						}
						String returnFc = fc.getStatus();
						if(!"Acquisti: \n".equals(returnFc) && returnFc.length() > 0) {
							JOptionPane.showMessageDialog(frame, returnFc);
						}
						frame.reset(null);
						break;
					case COMPRAVENDITA_VENDI:
						CompravenditaVendi fv = new CompravenditaVendi(serverImplementation.core(), null);
						while(!fv.getEnd()) {
							try {
								Thread.sleep(50);
							} catch (InterruptedException e) {
								// Ignora
							}
						}
						String returnFv = fv.getStatus();
						if(!"Status vendita:\n".equals(returnFv) && returnFv.length() > 0) {
							JOptionPane.showMessageDialog(frame, returnFv);
						}
						frame.reset(null);
						break;
					case FINE_TURNO:
						if(pastoreCorrente.getNumeroMossa() < 3) {
							JOptionPane.showMessageDialog(frame, "Devi finire le tue mosse prima di terminare il turno!");
							frame.reset(null);
							break;
						} else {
							if(mioGiocatore.getPastori().size() == 2) {
								mioGiocatore.setPastoreCorrente(null);
							}
							pastoreCorrente.reset();
							serverImplementation.core().getStatus().setGiocatoreCorrente(serverImplementation.core().getStatus().prossimoGiocatore());
							frame.reset(null);
						}
						break;
					default:
						try {
							Thread.sleep(50);
						} catch (InterruptedException e) {
							// Ignora
						}
						break;
				}
			}
		} catch (MalformedURLException e) {
			throw new MyRuntimeException(e);
		} catch (RemoteException e) {
			throw new MyRuntimeException(e);
		}
	}

	/**
	 * @see Rete#startClient()
	 */
	@Override
	public final void startClient() throws ClassNotFoundException, IOException {
		ServerInterface server;
		try {
			server = (ServerInterface)Naming.lookup("//"+host+":"+port+"/"+name);
			ClientImplementation client = new ClientImplementation();
			ClientInterface remoteRef = (ClientInterface) UnicastRemoteObject.exportObject(client, 0);
			if(!server.addClient(remoteRef)) {
				new ReteClient("Partita gia iniziata!");
				return;
			}
			ReteClient view = new ReteClient("In attesa dell'avvio della partita...");
			while(!server.partitaIniziata()) {
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) { 
					// Ignora
					
				}
			}
			view.dispose();
			
			int playerIndex = server.myNum(remoteRef);
			Giocatore mioGiocatore = server.core().getStatus().getGiocatori().get(playerIndex);
			ControllerMappa frame = new ControllerMappa(server.core());
			frame.allButtonsDisabled();
			frame.setVisible(true);
			while(!server.core().getStatus().getGiocatoreCorrente().equals(mioGiocatore)) {
				try {
					Thread.sleep(100);
					frame.updateTurno(server.core().getStatus().getGiocatoreCorrente().getColore().toString());
					frame.updateNumeroTurno(server.core().getStatus().getTurno()+"");
				} catch (InterruptedException e) {
					// Ignora
				}
			}
			viewPastoriInit(server.core(), frame);
			updateView(server.core(), frame);
			frame.requestPastore(mioGiocatore, server.core(), server);
			server.setGiocatoreCorrente(server.core().getStatus().prossimoGiocatore());
			while(!server.core().pastoriInizializzati()) {
				try {
					Thread.sleep(100);
					frame.updateTurno(server.core().getStatus().getGiocatoreCorrente().getColore().toString());
					frame.updateNumeroTurno(server.core().getStatus().getTurno()+"");
				} catch (InterruptedException e) {
					// Ignora
				}
			}
			viewPastoriInit(server.core(), frame);
			boolean mustResync = false;
			while(!server.core().getStatus().gameEnd()) {
				int mossa = 0;
				while(!server.core().getStatus().getGiocatoreCorrente().equals(mioGiocatore)) {
					mustResync = true;
					try {
						Thread.sleep(100);
						Pastore correntePastore = server.core().getStatus().getGiocatoreCorrente().getPastoreCorrente();
						int mossaCorrente = mossa;
						if(correntePastore != null) {
							mossaCorrente = correntePastore.getNumeroMossa();
						}
						if(mossaCorrente != mossa) {
							updateView(server.core(), frame);
							mossa = mossaCorrente;
						}
						frame.updateTurno(server.core().getStatus().getGiocatoreCorrente().getColore().toString());
						frame.updateNumeroTurno(server.core().getStatus().getTurno()+"");
						frame.allButtonsDisabled();
						frame.reset(null);
					} catch (InterruptedException e) {
						// Ignora
					}
				}
				if(mustResync) {
					mustResync = false;
					updateView(server.core(), frame);
					server.setTurno(server.core().getStatus().getTurno()+1);
					if(mioGiocatore.equals(server.core().getStatus().getGiocatoreIniziale()) && server.core().getStatus().getNumeroRecinti() <= 0) {
						frame.endGame(server.core(), server);
					} else {
						if(frame.partitaIniziata()) {
							frame.muoviPecoraNera(server.core(), server);
							frame.muoviLupo(server.core(), server);
							List<Regione> update = server.verificaAgnelli();
							for(int i = 0; i < update.size(); i++) {
								ViewRegione reg = frame.getViewRegione(update.get(i));
								reg.getAgnello().update();
								reg.getAriete().update();
								reg.getPecoraBianca().update();
							}
						}
						JOptionPane.showMessageDialog(frame, "E' iniziato il tuo turno!");
						frame.updateTurno(mioGiocatore.getColore().toString());
						frame.updateNumeroTurno(server.core().getStatus().getTurno()+"");
					}
				}
				Pastore pastoreCorrente = mioGiocatore.getPastoreCorrente();
				frame.updateContatori(mioGiocatore, server.core());
				frame.updateButtons(mioGiocatore, server.core());
				if(pastoreCorrente == null) {
					frame.richiediPastoreCorrente(mioGiocatore, server);
				}
				switch(frame.getAzione()) {
					case MUOVI_PASTORE:
						frame.muoviPastore(mioGiocatore, server.core(), server);
						break;
					case MUOVI_PECORA:
						frame.muoviPecora(mioGiocatore, server.core(), server);
						break;
					case MUOVI_ARIETE:
						frame.muoviAriete(mioGiocatore, server.core(), server);
						break;
					case MUOVI_PECORA_NERA:
						frame.muoviPecoraNera(mioGiocatore, server.core(), server);
						break;
					case SPARATORIA:
						frame.sparatoria(mioGiocatore, server.core(), server);
						break;
					case ACCOPPIA:
						frame.accoppia(mioGiocatore, server.core(), server);
						break;
					case COMPRA_TERRENO:
						frame.compraTerreno(mioGiocatore, server.core(), server);
						break;
					case COMPRAVENDITA:
						Object[] options = { "Compra", "Vendi", "Annulla" };
						int n = JOptionPane.showOptionDialog(frame, "Che azione intendi fare?", "Compravendita", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[2]);
						switch(n) {
							case 0:
								frame.setAzione(Azione.COMPRAVENDITA_COMPRA);
								break;
							case 1:
								frame.setAzione(Azione.COMPRAVENDITA_VENDI);
								break;
							default:
								frame.reset(null);
								break;
						}
						break;
					case COMPRAVENDITA_COMPRA:
						List<Giocatore> giocatori = server.core().getStatus().getGiocatori();
						List<Giocatore> compraDa = new ArrayList<>();
						for(int i = 0; i < giocatori.size(); i++) {
							if(!mioGiocatore.equals(giocatori.get(i))) {
								compraDa.add(giocatori.get(i));
							}
						}
						int indiceGiocatore = 0;
						int compraDaSize = compraDa.size();
						if(compraDaSize > 1) {
							Object[] gopt = new Object[compraDaSize];
							for(int i = 0; i < compraDaSize; i++) {
								gopt[i] = new String(compraDa.get(i).getColore().toString());
							}
							indiceGiocatore = JOptionPane.showOptionDialog(frame, "Che azione intendi fare?", "Compravendita", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, gopt, gopt[0]);
						}
						if(indiceGiocatore < 0 || indiceGiocatore >= compraDaSize) {
							frame.reset(null);
							break;
						}
						CompravenditaCompra fc = new CompravenditaCompra(server.core(), compraDa.get(indiceGiocatore), server);
						while(!fc.getEnd()) {
							try {
								Thread.sleep(50);
							} catch (InterruptedException e) {
								// Ignora
							}
						}
						String returnFc = fc.getStatus();
						if(!"Acquisti: \n".equals(returnFc) && returnFc.length() > 0) {
							JOptionPane.showMessageDialog(frame, returnFc);
						}
						frame.reset(null);
						break;
					case COMPRAVENDITA_VENDI:
						CompravenditaVendi fv = new CompravenditaVendi(server.core(), server);
						while(!fv.getEnd()) {
							try {
								Thread.sleep(50);
							} catch (InterruptedException e) {
								// Ignora
							}
						}
						String returnFv = fv.getStatus();
						if(!"Status vendita:\n".equals(returnFv) && returnFv.length() > 0) {
							JOptionPane.showMessageDialog(frame, returnFv);
						}
						frame.reset(null);
						break;
					case FINE_TURNO:
						if(pastoreCorrente.getNumeroMossa() < 3) {
							JOptionPane.showMessageDialog(frame, "Devi finire le tue mosse prima di terminare il turno!");
							frame.reset(null);
							break;
						} else {
							if(mioGiocatore.getPastori().size() == 2) {
								server.setPastoreCorrente(mioGiocatore, null);
							}
							server.reset(pastoreCorrente.getGiocatore());
							Giocatore next = server.core().getStatus().prossimoGiocatore();
							server.setGiocatoreCorrente(next);
							frame.reset(null);
						}
						break;
					default:
						try {
							Thread.sleep(50);
						} catch (InterruptedException e) {
							// Ignora
						}
						break;
				}
			}
			
		} catch (MalformedURLException e) {
			throw new MyRuntimeException(e);
		} catch (RemoteException e) {
			throw new MyRuntimeException(e);
		} catch (NotBoundException e) {
			throw new MyRuntimeException(e);
		}
	}
	
	
	/**
	 * Inizializza la view dei pastori
	 * @param core gamecore inizializzato
	 * @param frame JFrame della GUI
	 */
	public void viewPastoriInit(GameCore core, ControllerMappa frame) {
		List<Giocatore> giocatori = core.getStatus().getGiocatori();
		for(int i = 0; i < giocatori.size(); i++) {
			List<Pastore> pastori = giocatori.get(i).getPastori();
			for(int j = 0; j < pastori.size(); j++) {
				Pastore pastore = pastori.get(j);
				if(pastore.getCasella() != null && frame.getViewPastore(pastore) == null) {
					frame.creaViewPastore(giocatori.get(i), core);
				}
			}
			
			
		}
	}
	
	/**
	 * Aggiorna vari parametri nella gui
	 * @param core
	 * @param frame
	 */
	public void updateView(GameCore core, ControllerMappa frame) {
		List<Giocatore> giocatori = core.getStatus().getGiocatori();
		for(int i = 0; i < giocatori.size(); i++) {
			List<Pastore> pastori = giocatori.get(i).getPastori();
			for(int j = 0; j < pastori.size(); j++) {
				Pastore tmp = pastori.get(j);
				if(tmp.getCasella() != null && frame.getViewPastore(tmp) != null) {
					ViewCasella actual = frame.getViewCasella(tmp.getCasella());
					ViewCasella real = frame.getViewPastore(tmp).getViewCasella();
					if(!actual.equals(real)) {
						AnimazionePastore animazione = new AnimazionePastore();
						animazione.inizializza(frame, frame.getContentPane(), real, frame.getViewPastore(tmp));
						new Thread(animazione).start();
					}
				}
			}
		
		}
		List<Casella> caselle = core.getStatus().getMappa().allCasella();
		for(int i = 0; i < caselle.size(); i++) {
			frame.getViewCasella(caselle.get(i)).rUpdate();
		}
		List<Regione> regioni = core.getStatus().getMappa().allRegione();
		for(int i = 0; i < regioni.size(); i++) {
			frame.getViewRegione(regioni.get(i)).update();
		}
		
	}
	
	/**
	 * Locale viene sincronizzato con remote (copia tutto da remote)
	 * @param remote gamecore remoto
	 * @param locale gamecore locale
	 * @param firstTime true se è la prima sincronizzazione
	 */
	public void sync(GameStatus remote, GameStatus locale, boolean firstTime, ControllerMappa frame) {
		if(firstTime) {
			locale.setNumeroGiocatori(remote.getNumeroGiocatori());
			int firstPlayer = remote.getGiocatori().indexOf(remote.getGiocatoreIniziale());
			locale.setGiocatoreIniziale(locale.getGiocatori().get(firstPlayer));
		}
		List<Giocatore> remoteGiocatori = remote.getGiocatori();
		List<Giocatore> localeGiocatori = locale.getGiocatori();
		for(int i = 0; i < remoteGiocatori.size(); i++) {
			Giocatore rg = remoteGiocatori.get(i);
			Giocatore lg = localeGiocatori.get(i);
			if(firstTime) {
				lg.setColore(rg.getColore());
			}
			lg.setMonete(rg.getMonete());
			Map<Terreno, Integer> rtc = rg.getTerreniComprati();
			Map<Terreno, Integer> ltc = lg.getTerreniComprati();
			for(int j = 0; j < Terreno.values().length-1; j++) {
				Terreno ttmp = Terreno.values()[j];
				ltc.remove(ttmp);
				ltc.put(ttmp, rtc.get(ttmp));
			}
			List<OggettoVendita> rtv = rg.getTerreniVendita();
			List<OggettoVendita> ltv = lg.getTerreniVendita();
			ltv.clear();
			for(int j = 0; j < rtv.size(); j++) {
				OggettoVendita rov = rtv.get(j);
				OggettoVendita tov = new OggettoVendita(lg, rov.getTipoTerreno(), rov.getQuantita(), rov.getPrezzo());
				ltv.add(tov);
			}
			if(!firstTime) {
				List<Pastore> rpastore = rg.getPastori();
				List<Pastore> lpastore = lg.getPastori();
				for(int j = 0; j < rpastore.size(); j++) {
					Pastore rp = rpastore.get(i);
					Pastore lp = lpastore.get(i);
					if(rp.getCasella().getId() != lp.getCasella().getId()) {
						lp.setCasella((Casella) locale.getMappa().trovaCasella(rp.getCasella().getId()));
						ViewCasella tmpViewCasella = frame.getViewCasella(lp.getCasella());
						frame.getViewPastore(lp).spostaPastoreSuCasella(tmpViewCasella);
					}
				}
			}
		}
		locale.setPosizioneLupo((Regione) locale.getMappa().trovaRegione(remote.getPosizioneLupo().getId()));
		locale.setPosizionePecoraNera((Regione) locale.getMappa().trovaRegione(remote.getPosizionePecoraNera().getId()));
		locale.setTurno(remote.getTurno());
		locale.setGameEnd(remote.gameEnd());
		locale.setNumeroRecinti(remote.getNumeroRecinti());
		List<Agnello> remoteAgnelli = remote.getAgnelli();
		List<Agnello> localeAgnelli = locale.getAgnelli();
		localeAgnelli.clear();
		for(int i = 0; i < remoteAgnelli.size(); i++) {
			Agnello rema = remoteAgnelli.get(i);
			Giocatore tg = locale.getGiocatori().get(remote.getGiocatori().indexOf(rema.getGiocatore()));
			Regione retm = (Regione) locale.getMappa().trovaRegione(rema.getRegione().getId());
			Agnello atmp = new Agnello(rema.getAge(), tg, retm);
			localeAgnelli.add(atmp);
		}
		GraphXML remoteMap = remote.getMappa();
		GraphXML localeMap = locale.getMappa();
		List<Casella> caselleRemote = remoteMap.allCasella();
		List<Casella> caselleLocale = localeMap.allCasella();
		for(int i = 0; i < caselleRemote.size(); i++) {
			Casella rtmp = caselleRemote.get(i);
			Casella ltmp = caselleLocale.get(i);
			ltmp.setRecinto(rtmp.hasRecinto());
			if(!firstTime) {
				boolean isFinale = frame.getViewCasella(rtmp).isFinale();
				frame.getViewCasella(ltmp).update(isFinale);
			}
			if(rtmp.hasPastore()) {
				int plIndex = remote.getGiocatori().indexOf(rtmp.getPastore().getGiocatore());
				int psIndex = rtmp.getPastore().getGiocatore().getPastori().indexOf(rtmp.getPastore());	
				ltmp.setPastore(locale.getGiocatori().get(plIndex).getPastori().get(psIndex));
			}
		}
		if(!firstTime) {
			List<Regione> regioniRemote = remoteMap.allRegione();
			List<Regione> regioniLocale = localeMap.allRegione();
			for(int i = 0; i < regioniRemote.size(); i++) {
				Regione rtmp = regioniRemote.get(i);
				Regione ltmp = regioniLocale.get(i);
				ltmp.setPecore(rtmp.getPecore());
				ltmp.setArieti(rtmp.getArieti());
				ltmp.setPecoraNera(rtmp.hasPecoraNera());
				ltmp.setLupo(rtmp.hasLupo());
				List<Agnello> rAgn = rtmp.getAgnelli();
				List<Agnello> lAgn = ltmp.getAgnelli();
				lAgn.clear();
				for(int j = 0; j < rAgn.size(); j++) {
					Agnello rema = rAgn.get(j);
					Giocatore tg = locale.getGiocatori().get(remote.getGiocatori().indexOf(rema.getGiocatore()));
					Regione retm = (Regione) locale.getMappa().trovaRegione(rema.getRegione().getId());
					Agnello atmp = new Agnello(rema.getAge(), tg, retm);
					lAgn.add(atmp);
				}
				frame.getViewRegione(ltmp).update();
			}
		}				
	}
}
