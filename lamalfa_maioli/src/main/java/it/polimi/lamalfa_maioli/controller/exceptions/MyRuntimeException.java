package it.polimi.lamalfa_maioli.controller.exceptions;

import java.io.Serializable;

/**
 * Eccezione runtime sollevata nel caso in cui non si può procedere con la partita
 * @author lamalfa_maioli
 *
 */
public class MyRuntimeException extends RuntimeException  implements Serializable {

	private static final long serialVersionUID = 6050974044020427484L;

	/**
	 * Costruttore
	 */
	public MyRuntimeException() {
		super();
	}

	/**
	 * Costruttore
	 * @param message messaggio per descrivere l'eccezione
	 */
	public MyRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public MyRuntimeException(String message) {
		super(message);
	}
	
	/**
	 * Costruttore nel caso di eccezione ramificata
	 * @param cause eccezione precedente
	 */
	public MyRuntimeException(Throwable cause) {
		super(cause);
	}

}
