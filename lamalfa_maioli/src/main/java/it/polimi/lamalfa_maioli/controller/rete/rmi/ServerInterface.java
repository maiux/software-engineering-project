package it.polimi.lamalfa_maioli.controller.rete.rmi;

import it.polimi.lamalfa_maioli.controller.*;
import it.polimi.lamalfa_maioli.controller.GameCore.Animale;
import it.polimi.lamalfa_maioli.controller.exceptions.DadoException;
import it.polimi.lamalfa_maioli.model.Casella;
import it.polimi.lamalfa_maioli.model.Giocatore;
import it.polimi.lamalfa_maioli.model.Pastore;
import it.polimi.lamalfa_maioli.model.Regione;
import it.polimi.lamalfa_maioli.model.Terreno;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * Interfaccia per utilizzare il server tramite rmi
 * @author lamalfa_maioli
 *
 */
public interface ServerInterface extends Remote {
	/**
	 * Aggiunge un client a quelli connessi
	 * @param client ClientInterface del client connesso
	 * @return se il client è stato aggiunto (false se la partita è già iniziata)
	 * @throws RemoteException
	 */
	public boolean addClient(ClientInterface client) throws RemoteException;
	
	/**
	 * @return il gamecore del server
	 * @throws RemoteException
	 */
	public GameCore core() throws RemoteException;
	
	/**
	 * @return numero di client connessi
	 * @throws RemoteException
	 */
	public int numeroClients() throws RemoteException;
	
	/**
	 * @return se la partita è iniziata
	 * @throws RemoteException
	 */
	public boolean partitaIniziata()  throws RemoteException;
	
	/**
	 * Restituisce il numero giocatore associato al client
	 * @param client client di cui si vuole conosce il numero giocatore
	 * @return numero giocatore associato al client
	 * @throws RemoteException
	 */
	public int myNum(ClientInterface client) throws RemoteException;
	
	/**
	 * Inizializza la partita
	 * @throws RemoteException
	 */
	public void inizializza() throws RemoteException;
	
	/**
	 * Imposta il giocatore corrente
	 * @param giocatore giocatore da impostare come corrente
	 * @throws RemoteException
	 */
	public void setGiocatoreCorrente(Giocatore giocatore) throws RemoteException;
	
	/**
	 * Posiziona un pastore sulla mappa 
	 * @param giocatore giocatore che possiede il pastore
	 * @param i indice del pastore
	 * @param casellaPremuta casella su cui posizionare il pastore
	 * @throws RemoteException
	 */
	public void posizionaPastore(Giocatore giocatore, int i, Casella casellaPremuta) throws RemoteException;
	
	/**
	 * Imposta il turno
	 * @param turno numero del turno
	 * @throws RemoteException
	 */
	public void setTurno(int turno) throws RemoteException ;
	
	/**
	 * Tira il dado per muovere la pecora nera
	 * @throws RemoteException
	 */
	void dadoPecoraNera() throws RemoteException;
	
	/**
	 * Tira il dado per muovere il lupo
	 * @throws RemoteException
	 */
	void dadoLupo() throws RemoteException;
	
	/**
	 * Esegue la verifica degli agnelli
	 * @return le regioni in  cui gli agnelli sono diventati grandi
	 * @throws RemoteException
	 */
	public List<Regione> verificaAgnelli() throws RemoteException;
	
	/**
	 * Imposta un pastore corrente su un giocatore
	 * @param giocatore giocatore
	 * @param pastore pastore da impostare
	 * @return risultato dell'operazione
	 * @throws RemoteException
	 */
	public boolean setPastoreCorrente(Giocatore giocatore, Pastore pastore) throws RemoteException;
	
	/**
	 * Muove un pastore su una casella 
	 * @param pastore pastore da muovere
	 * @param casella casella su cui muovere il pastore
	 * @return risultato dell'operazione
	 * @throws RemoteException
	 */
	public boolean muoviPastore(Pastore pastore, Casella casella) throws RemoteException;
	
	/**
	 * Il pastore muove una pecora da regione1 a regione2
	 * @param pastore pastore che muove
	 * @param regione1 regione di partenza
	 * @param regione2 regione di arrivo
	 * @return risultato dell'operazione
	 * @throws RemoteException
	 */
	boolean muoviPecora(Pastore pastore, Regione regione1, Regione regione2) throws RemoteException;
	
	/**
	 * Il pastore muove un ariete da regione1 a regione2
	 * @param pastore
	 * @param regione1 regione di partenza
	 * @param regione2 regione di arrivo
	 * @return risultato dell'operazione
	 * @throws RemoteException
	 */
	public boolean muoviAriete(Pastore pastore, Regione regione1, Regione regione2) throws RemoteException;
	
	/**
	 * Il pastore muove la pecora nera da regione1 a regione2
	 * @param pastore
	 * @param regione1 regione di partenza
	 * @param regione2 regione di arrivo
	 * @return risultato dell'operazione
	 * @throws RemoteException
	 */
	public boolean muoviPecoraNera(Pastore pastore, Regione regione1, Regione regione2) throws RemoteException;
	
	/**
	 * Il pastore esegue la sparatoria nella regione sull'animale
	 * @param pastore pastore che spara
	 * @param regione regione in cui spara
	 * @param animale animale da uccidere
	 * @return risultato dell'operazione
	 * @throws RemoteException
	 * @throws DadoException
	 */
	public int sparatoria(Pastore pastore, Regione regione, Animale animale) throws RemoteException, DadoException;
	
	/**
	 * Un pastore fa accoppiare una pecora e un ariete in una regione
	 * @param pastore pastore che fa accoppiare
	 * @param regione regione in cui avviene il coito
	 * @return risultato dell'operazione
	 * @throws RemoteException
	 * @throws DadoException
	 */
	public boolean accoppia(Pastore pastore, Regione regione) throws RemoteException, DadoException;
	
	/**
	 * Il pastore compra il terreno
	 * @param pastore pastore che compra il terreno
	 * @param terreno terreno da comprare
	 * @return risultato dell'operazione
	 * @throws RemoteException
	 */
	public boolean compraTerreno(Pastore pastore, Terreno terreno) throws RemoteException;
	
	/**
	 * Il giocatore 1 compra un terreno da giocatore2
	 * @param giocatore1 giocatore che compra
	 * @param giocatore2 giocatore che vende
	 * @param terreno terreno da comprare
	 * @param quantita quantità da comprare
	 * @return risultato dell'operazione
	 * @throws RemoteException
	 */
	public boolean compra(Giocatore giocatore1, Giocatore giocatore2, Terreno terreno, int quantita) throws RemoteException;
	
	/**
	 * Il giocatore associato al pastore mette in vendita una quantità di terreno
	 * @param pastore pastore che vende
	 * @param terreno terreno da vendere
	 * @param quantita quantità da vendere
	 * @param prezzo prezzo di vendita
	 * @return risultato dell'operazione
	 * @throws RemoteException
	 */
	public boolean vendi(Pastore pastore, Terreno terreno, int quantita, int prezzo) throws RemoteException;
	
	/**
	 * Reimposta i parametri di un giocatore
	 * @param giocatore
	 * @throws RemoteException
	 */
	public void reset(Giocatore giocatore) throws RemoteException;
	
	/**
	 * Imposta la fine della partia
	 * @param status se la partita è finita
	 * @throws RemoteException
	 */
	public void setGameEnd(boolean status) throws RemoteException;
}
