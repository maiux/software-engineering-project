package it.polimi.lamalfa_maioli.controller;

import it.polimi.lamalfa_maioli.controller.exceptions.ColorException;
import it.polimi.lamalfa_maioli.controller.exceptions.DadoException;
import it.polimi.lamalfa_maioli.controller.exceptions.MapSyntaxException;
import it.polimi.lamalfa_maioli.controller.exceptions.MyRuntimeException;
import it.polimi.lamalfa_maioli.model.Agnello;
import it.polimi.lamalfa_maioli.model.Casella;
import it.polimi.lamalfa_maioli.model.Colore;
import it.polimi.lamalfa_maioli.model.GameStatus;
import it.polimi.lamalfa_maioli.model.Giocatore;
import it.polimi.lamalfa_maioli.model.GraphXML;
import it.polimi.lamalfa_maioli.model.OggettoVendita;
import it.polimi.lamalfa_maioli.model.Pastore;
import it.polimi.lamalfa_maioli.model.Regione;
import it.polimi.lamalfa_maioli.model.Terreno;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * GameCore
 * @author lamalfa_maioli
 */
public class GameCore implements Serializable {
	private static final long serialVersionUID = -3505015034280578991L;
	private volatile GameStatus status;
	
	
	public enum Animale {
		PECORA, PECORA_NERA, ARIETE;
	}
	
	/**
	 * Inizializza il gamestatus, la mappa, la posizione della pecora nera e del lupo
	 */
	public GameCore(int numeroGiocatori) {
		try {
			GraphXML grafico = new GraphXML("/it/polimi/lamalfa_maioli/mappa.xml");
			status = new GameStatus(grafico);
			List<Regione> tmp = status.getMappa().trovaRegioni(Terreno.SHEEPSBURG);
			Regione sheepsburg = tmp.get(0);
			sheepsburg.setPecoraNera(true);
			status.setPosizionePecoraNera(sheepsburg);
			int randomLupo;
			Regione regioneLupo;
			do{
				randomLupo = (int)(Math.random()*status.getMappa().numeroRegioni());
				regioneLupo = (Regione)status.getMappa().trovaRegione(randomLupo);
			} while(regioneLupo.equals(sheepsburg));
			regioneLupo.setLupo(true);
			status.setPosizioneLupo(regioneLupo);
			status.setNumeroGiocatori(numeroGiocatori);
		} catch (MapSyntaxException e) {
			throw new MyRuntimeException(e);
		}
	}
	
	/**
	 * Inizializzazione del gamecore usando uno status già inizializzato
	 * @param status GameStatus già inizializzato
	 */
	public GameCore(GameStatus status) {
		this.status = status;
	}
	
	/**
	 * @return gamestatus
	 */
	public GameStatus getStatus() {
		return status;
	}
	
	/**
	 * Imposta il giocatore iniziale, scelto a caso
	 */
	public void impostaGiocatoreIniziale() {
		int indice = (int)(Math.random() * (status.getNumeroGiocatori()-1));
		status.setGiocatoreIniziale(status.getGiocatori().get(indice));
	}
	
	/**
	 * Muove una pecora, un ariete o la pecora nera da una regione ad un'altra
	 * @param pastore pastore che intende eseguire il movimento
	 * @param regione1 regione di partenza
	 * @param regione2 regione di arrivo
	 * @param elemento pecora o ariete
	 * @return successo del movimento
	 */
	private boolean movimentoAnimale(Pastore pastore, Regione regione1, Regione regione2, Animale animale) {
		if(status.getMappa().areNear(regione1, regione2, pastore.getCasella())) {
			int valore = 0;
			switch(animale) {
				case PECORA:
					valore = regione1.getPecore();
					if(valore > 0) {
						regione1.setPecore(valore-1);
						valore = regione2.getPecore();
						regione2.setPecore(valore+1);
						return true;
					}
					return false;
				case ARIETE:
					valore = regione1.getArieti();
					if(valore > 0) {
						regione1.setArieti(valore-1);
						valore = regione2.getArieti();
						regione2.setArieti(valore+1);
						return true;
					}
					return false;
				case PECORA_NERA:
					if(regione1.hasPecoraNera()) {
						regione1.setPecoraNera(false);
						regione2.setPecoraNera(true);
						status.setPosizionePecoraNera(regione2);
						return true;
					}
					return false;
				default:
					return false;
			}
		}
		return false;
	}
	
	/**
	 * Muove un animale
	 * @param pastore pastore che muove l'animale
	 * @param regione1 regione di partenza
	 * @param regione2 regione di arrivo
	 * @param elemento tipo di animale
	 * @return successo del movimento
	 */
	private boolean muoviAnimale(Pastore past, Regione reg1, Regione reg2, Animale animale) {
		Regione regione1 = (Regione) status.getMappa().trovaRegione(reg1.getId());
		Regione regione2 = (Regione) status.getMappa().trovaRegione(reg2.getId());
		Pastore pastore = pastoreEffettivo(past);
		if(pastore == null) {
			return false;
		}
		
		if(pastore.isPecoraMossa() || !puoEseguire(pastore)) {
			return false;
		} else if(movimentoAnimale(pastore, regione1, regione2, animale)) {
			int mossa = pastore.getNumeroMossa();
			pastore.setPecoraMossa(true);
			pastore.setNumeroMossa(mossa+1);
			return true;
		}
		return false;
	}
	/**
	 * Il pastore corrente muove una pecora da regione1 a regione2
	 * @param pastore pastore che muove l'animale
	 * @param regione1 regione di partenza
	 * @param regione2 regione di arrivo
	 * @return successo del movimento
	 */
	public boolean muoviPecora(Pastore pastore, Regione regione1, Regione regione2) {
		return muoviAnimale(pastore, regione1, regione2, Animale.PECORA);
	}
	
	/**
	 * Il pastore corrente muove un ariete da regione1 a regione2
	 * @param pastore pastore che muove l'animale
	 * @param regione1 regione di partenza
	 * @param regione2 regione di arrivo
	 * @return successo del movimento
	 */
	public boolean muoviAriete(Pastore pastore, Regione regione1, Regione regione2) {
		return muoviAnimale(pastore, regione1, regione2, Animale.ARIETE);
	}
	
	/**
	 * Il pastore corrente muove la pecora nera da regione1 a regione2
	 * @param pastore pastore che muove l'animale
	 * @param regione1 regione di partenza
	 * @param regione2 regione di arrivo
	 * @return successo del movimento
	 */
	public boolean muoviPecoraNera(Pastore pastore, Regione regione1, Regione regione2) {
		return muoviAnimale(pastore, regione1, regione2, Animale.PECORA_NERA);
	}
	
	/**
	 * Muove la pecora nera con il dado
	 */
	public void dadoPecoraNera() {
		int dado = 1 + (int)(Math.random()*6);
		Regione after = status.getPosizionePecoraNera();
		Casella move = status.getMappa().casellaNearBy(after, dado);
		if(move == null) {
			return;
		}
		if(!move.hasRecinto() && !move.hasPastore()) {
			Regione before = status.getMappa().regioneNearBy(move, after);
			if(before != null) {
				after.setPecoraNera(false);
				before.setPecoraNera(true);
				status.setPosizionePecoraNera(before);
			}
		}
	}
	
	/**
	 * Muove il lupo con il dado e mangia una pecora o un ariete nella regione di destinazione.
	 * Policy: il lupo si sposta sempre. Se non ci sono caselle senza recinto, ne salta una
	 */
	public void dadoLupo() {
		Regione after = status.getPosizioneLupo();
		List<Casella> caselle = status.getMappa().casellaNearBy(after);
		int initSize = caselle.size();
		int i = 0;
		do {
			Casella current = caselle.get(i);
			if(current.hasRecinto()) {
				caselle.remove(i);
			} else {
				i++;
			}
		} while(i < caselle.size());
		int size = caselle.size();
		int dado = (int)(Math.random()*((size > 0) ? size : initSize));
		if(size == 0) {
			caselle = status.getMappa().casellaNearBy(after);
		}
		Casella current = caselle.get(dado);
		Regione before = status.getMappa().regioneNearBy(current, after);
		if(before != null) {
			after.setLupo(false);
			before.setLupo(true);
			status.setPosizioneLupo(before);
		}
		int numero = 0;
		if(Math.random() > 0.5) {
			numero = before.getArieti();
			before.setArieti(numero-1);
		} else {
			numero = before.getPecore();
			before.setPecore(numero-1);
		}
		
	}
	
	/**
	 * Ritorna una lista delle caselle libere su cui ci si può muovere gratuitamente
	 * @param pastore pastore di cui si vuole prendere le mosse gratuite
	 * @return le caselle su cui il pastore si può spostare gratuitamente
	 */
	public List<Casella> getFreeMoves(Pastore pastore) {
		List<Casella> free = status.getMappa().casellaNearBy((Casella) status.getMappa().trovaCasella(pastore.getCasella().getId()));
		for(int i = 0; i < free.size(); i++) {
			Casella tmp = free.get(i);
			if(tmp.hasPastore() || tmp.hasRecinto()) {
				free.remove(tmp);
				i--;
			}
		}
		return free;
	}
	
	/**
	 * Restituisce il costo della mossa verso la casella selezionata del pastore selezionato
	 * @param pastore pastore selezionato
	 * @param casella casella su cui ci si vuole muovere
	 * @return costo della mossa verso la casella selezionata del pastore selezionato
	 */
	public int getMoveMoney(Pastore pastore, Casella casella) {
		List<Casella> freeMoves = getFreeMoves(pastore);
		freeMoves.add((Casella) status.getMappa().trovaCasella(pastore.getCasella().getId()));
		if(freeMoves.contains(casella)) {
			return 0;
		}
		return 1;
	}
	
	/**
	 * Verifica se la casella è libera (nessun recinto e pastore sopra)
	 * @param casella casella da verificare
	 * @return se la casella è libera
	 */
	public boolean casellaLibera(Casella cas) {
		Casella casella = (Casella) status.getMappa().trovaCasella(cas.getId());
		if(casella.hasPastore() || casella.hasRecinto()) {
			return false;
		}
		return true;
	}
	
	public boolean pastoriInizializzati() {
		int pastori = 0;
		List<Giocatore> giocatori = status.getGiocatori();
		int size = giocatori.size();
		int expected = size > 2 ? size : 4;
		for(int i = 0; i < size; i++) {
			pastori += giocatori.get(i).getPastori().size();
		}
		return pastori == expected;
	}
	
	/**
	 * Muove il pastore verso la casella selezionata
	 * @param pastore pastore da muovdere
	 * @param casella casella su cui muovere il pastore
	 * @return successo del movimento
	 */
	public boolean muoviPastore(Pastore past, Casella cas) {
		Giocatore giocatore = giocatoreEffettivo(past.getGiocatore());
		Pastore pastore = pastoreEffettivo(past);
		Casella casella = (Casella) status.getMappa().trovaCasella(cas.getId());
		Casella casellaPastore = (Casella) status.getMappa().trovaCasella(pastore.getCasella().getId());
		if(casella.equals(casellaPastore)) {
			return false;
		}
		int mossa = pastore.getNumeroMossa();
		int monete = giocatore.getMonete();
		if(mossa < 3 && casellaLibera(casella)) {
			int newBalance = monete-getMoveMoney(pastore, casella);
			if(newBalance >= 0) {
				pastore.setNumeroMossa(mossa+1);
				casellaPastore.setRecinto(true);
				int recinti = status.getNumeroRecinti()-1;
				status.setNumeroRecinti(recinti);
				casellaPastore.setPastore(null);
				giocatore.setMonete(newBalance);
				pastore.setCasella(casella);
				pastore.setPastoreMosso(true);
				casella.setPastore(pastore);
				return true;
			}
			return false;
		}
		return false;
	}
	
	/**
	 * Restituisce i terreni comprabili da un dato pastore
	 * @param pastore pastore selezionato
	 * @return Terreni comprabili dal pastore selezionato
	 */
	public List<Terreno> terreniComprabili(Pastore past) {
		Pastore pastore = pastoreEffettivo(past);
		List<Regione> tmp = status.getMappa().regioneNearBy((Casella) status.getMappa().trovaCasella(pastore.getCasella().getId()));
		List<Terreno> terreni = new ArrayList<>();
		for(int i = 0; i < tmp.size(); i++) {
			Terreno type = tmp.get(i).getType();
			if(!type.equals(Terreno.SHEEPSBURG) && !terreni.contains(type)) {
				terreni.add(type);
			}
		}
		return terreni;
	}
	
	/**
	 * Restituisce se un dato pastore può comprare un dato terreno
	 * Policy: max 6 terreni per tipo
	 * @param pastore pastore selezionato
	 * @param type terreno da comprare
	 * @return se è possibile comprare questo terreno
	 */
	public boolean isTerrenoComprabile(Pastore pastore, Terreno type) {
		if(type.equals(Terreno.SHEEPSBURG)) {
			return false;
		}
		if(type.getCosto() >= 6) {
			return false;
		}
		List<Terreno> tmp = terreniComprabili(pastore);
		for(int i = 0; i < tmp.size(); i++) {
			if(type.equals(tmp.get(i))) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Il giocatore associato al pastore compra il terreno del tipo di type
	 * @param pastore pastore selezionato
	 * @param type terreno da comprare
	 * @return risultato dell'operazione
	 */
	public boolean compraTerreno(Pastore past, Terreno type) {
		Pastore pastore = pastoreEffettivo(past);
		if(isTerrenoComprabile(pastore, type)) {
			Giocatore giocatore = giocatoreEffettivo(pastore.getGiocatore());
			if(pastore.isTerrenoComprato() || !puoEseguire(pastore)) {
				return false;
			}
			int costo = type.getCosto();
			int balance = giocatore.getMonete()-costo;
			if(balance >= 0) {
				int mossa = pastore.getNumeroMossa();
				giocatore.setMonete(balance);
				giocatore.addTerreno(type, 1);
				type.setCosto(costo+1);
				pastore.setTerrenoComprato(true);
				pastore.setNumeroMossa(mossa+1);
				return true;
			}			
		}
		return false;
	}
	
	/**
	 * Verifica se il pastore può accoppiare un ariete e una pecora nella regione selezionata
	 * @param pastore pastore selezionato
	 * @param regione regione dove si intende eseguire l'accoppiamento
	 * @return se il pastore può accoppiare un ariete e una pecora nella regione selezionata
	 */
	public boolean puoAccoppiare(Pastore past, Regione reg) {
		Pastore pastore = pastoreEffettivo(past);
		Regione regione = (Regione) status.getMappa().trovaRegione(reg.getId());
		int pecore = regione.getPecore();
		int arieti = regione.getArieti();
		if(pecore < 1 || arieti < 1 ) {
			return false;
		}
		if(pastore.isPecoraAccoppiata() || !puoEseguire(pastore)) {
			return false;
		}
		List<Regione> adiacenti = status.getMappa().regioneNearBy(pastore.getCasella());
		for(int i = 0; i < adiacenti.size(); i++) {
			if(regione.equals(adiacenti.get(i))) {
				return true; 
			}
		}
		return false;
	}
	
	/**
	 * Esegue l'accoppiamento di un ariete e una pecora
	 * L'accoppiamento va a buon fine se il numero uscito dal dado è uguale al numero della casella su cui è il pastore
	 * @param pastore pastore selezionato
	 * @param regione regione in cui desidero fare l'accoppiamento
	 * @return se l'accoppiamento è stato eseguito
	 * @throws DadoException 
	 */
	public boolean accoppia(Pastore past, Regione reg) throws DadoException {
		Pastore pastore = pastoreEffettivo(past);
		Regione regione = (Regione) status.getMappa().trovaRegione(reg.getId());
		if(!puoAccoppiare(pastore, regione)) {
			return false;
		}
		int dado = (int)(Math.random()*5+1);
		int mossa = pastore.getNumeroMossa();
		pastore.setPecoraAccoppiata(true);
		pastore.setNumeroMossa(mossa+1);
		if(dado == pastore.getCasella().getValue()) {
			Agnello agnello = new Agnello(status.getTurno(), pastore.getGiocatore(), regione);
			status.getAgnelli().add(agnello);
			regione.getAgnelli().add(agnello);
			return true;
		}
		throw new DadoException(""+dado);			
	}
	
	/**
	 * Verifica l'età degli agnelli e se necessario li trasforma in arieti / pecore
	 */
	public List<Regione> verificaAgnelli() {
		List<Agnello> agnelli = status.getAgnelli();
		List<Regione> update = new ArrayList<>();
		int turno = status.getTurno()-2;
		Giocatore current = status.getGiocatoreCorrente();
		for(int i = 0; i < agnelli.size(); i++) {
			Agnello tmp = agnelli.get(i);
			if((tmp.getAge() == turno && current.equals(tmp.getGiocatore())) || (tmp.getAge() < turno)) {
				Regione regione = (Regione) status.getMappa().trovaRegione(tmp.getRegione().getId());
				if(Math.random() > 0.5) {
					int num = regione.getArieti();
					regione.setArieti(num+1);
				} else {
					int num = regione.getPecore();
					regione.setPecore(num+1);
				}
				agnelli.remove(i);
				regione.getAgnelli().remove(tmp);
				update.add(regione);
				i--;
			}
		}
		return update;
	}
	
	/**
	 * Verifica se posso eseguire una mossa e se posso non muovere il pastore
	 * @param pastore pastore da verificare
	 * @return se può fare una mossa diversa dal muovipastore
	 */
	public boolean puoEseguire(Pastore past) {
		Pastore pastore = pastoreEffettivo(past);
		int mossa = pastore.getNumeroMossa();
		if(mossa == 2 && !pastore.isPastoreMosso() || mossa == 3) {
			return false;
		}
		return true;
	}
	
	/**
	 * Cerca i pastori che possono assistere alla sparatoria
	 * @param pastore pastore da cui intendo sparare
	 * @param regione regione su cui intende sparare
	 * @return lista dei pastori che possono assistere alla sparatoria
	 */
	public List<Pastore> testimoniSparatoria(Pastore pastore, Regione regione) {
		Giocatore me = pastore.getGiocatore();
		List<Pastore> testimoni = new ArrayList<>();
		List<Casella> caselleRegione = status.getMappa().casellaNearBy(regione);
		for(int i = 0; i < caselleRegione.size(); i++) {
			Casella current = caselleRegione.get(i);
			if(current.hasPastore() && !pastore.equals(current.getPastore()) && !pastore.getGiocatore().equals(me)) {
				testimoni.add(current.getPastore());
			}
		}
		return testimoni;
	}
	
	/**
	 * Calcola il costo per corrompere i pastori nel peggiore dei casi
	 * @param pastore pastore che intende sparare
	 * @param regione regione su cui si intende sparare
	 * @return costo massimo sparatoria
	 */
	private int costoMaxSparatoria(Pastore pastore, Regione regione) {
		List<Pastore> testimoni = testimoniSparatoria(pastore, regione);
		return 2*testimoni.size();
	}
	
	/**
	 * Verifica se il pastore può sparare su una data regione
	 * @param pastore pastore che spara
	 * @param regione regione su cui si spara
	 * @param animale tipo di animale su cui si intende sparare
	 * @return se il pastore può sparare
	 */
	public boolean puoSparare(Pastore past, Regione reg, Animale animale) {
		Pastore pastore = pastoreEffettivo(past);
		Regione regione = (Regione) status.getMappa().trovaRegione(reg.getId());
		if(pastore.isHaSparato() || !puoEseguire(pastore)) {
			return false;
		}
		if(!status.getMappa().areNear(pastore.getCasella(), regione)) {
			return false;
		}
		int num;
		switch(animale) {
			case PECORA:
				num = regione.getPecore()-1;
				break;
			case ARIETE:
				num = regione.getArieti()-1;
				break;
			default:
				return false;
		}
		if(num < 0) {
			return false;
		}
		int balance = pastore.getGiocatore().getMonete();
		balance -= costoMaxSparatoria(pastore, regione);
		if(balance >= 0) {
			return true;
		}
		return false;
	}
	
	/**
	 * Corrompe un giocatore dandogli 2 monete
	 * @param pastore pastore che ha sparato
	 * @param pastoreCorrotto pastore che deve essere corrotto
	 */
	public void corrompi(Pastore pastore, Pastore pastoreCorrotto) {
		Giocatore giocatore = giocatoreEffettivo(pastore.getGiocatore());
		Giocatore giocatoreCorrotto = giocatoreEffettivo(pastoreCorrotto.getGiocatore());
		int monete = giocatore.getMonete()-2;
		giocatore.setMonete(monete);
		monete = giocatoreCorrotto.getMonete()+2;
		giocatoreCorrotto.setMonete(monete);
	}
	
	/**
	 * Esegue la sparatoria sulla regione
	 * @param pastore pastore che esegue la sparatoria
	 * @param regione regione su cui sparare
	 * @param animale animale da colpire
	 * @return successo della sparatoria
	 * @throws DadoException 
	 */
	public int sparatoria(Pastore past, Regione reg, Animale animale) throws DadoException {
		Pastore pastore = pastoreEffettivo(past);
		Regione regione = (Regione) status.getMappa().trovaRegione(reg.getId());
		if(!puoSparare(pastore, regione, animale)) {
			return -1;
		}
		pastore.setHaSparato(true);
		int mossa = pastore.getNumeroMossa()+1;
		pastore.setNumeroMossa(mossa);
		int dado = (int)(Math.random()*5+1);
		Casella current = pastore.getCasella();
		if(current.getValue() != dado) {
			throw new DadoException(""+dado);
		}
		int num;
		switch(animale) {
			case PECORA:
				num = regione.getPecore()-1;
				regione.setPecore(num);
				break;
			case ARIETE:
				num = regione.getArieti()-1;
				regione.setArieti(num);
				break;
			default:
				return -1;
		}
		int corrotti = 0;
		List<Pastore> testimoni = testimoniSparatoria(pastore, regione);
		for(int i = 0; i < testimoni.size(); i++) {
			dado = (int)(Math.random()*5+1);
			if(dado > 4) {
				corrompi(pastore, testimoni.get(i));
				corrotti++;
			}
		}
		return corrotti;
	}
	
	/**
	 * Verifica se il pastore può vendere la quantità di terreni selezionati e se il prezzo è giusto
	 * @param pastore pastore selezionato
	 * @param terreno tipo di terreno da vendere
	 * @param quantita quantita di terreno da vendere
	 * @return se il pastore può vendere la quantità di terreni selezionati
	 */
	public boolean puoVendere(Pastore pastore, Terreno terreno, int quantita, int prezzo) {
		if(prezzo < 1 || prezzo > 4) {
			return false;
		}
		Map<Terreno,Integer> terreni = giocatoreEffettivo(pastore.getGiocatore()).getTerreniComprati();
		if(terreni.get(terreno) >= quantita && quantita > 0) {
			return true;
		}
		return false;		
	}
	
	/**
	 * Avvia la compravendita di una data quantità di un tipo di terreno
	 * @param pastore pastore che vende
	 * @param terreno tipo di terreno da vendere
	 * @param prezzo prezzo per ogni unità di terreno
	 * @param quantita quantità di terreni da vendere
	 * @return successo dell'operazione
	 */
	public boolean vendi(Pastore pastore, Terreno terreno, int quantita, int prezzo) {
		Giocatore giocatore = giocatoreEffettivo(pastore.getGiocatore());
		OggettoVendita vendita = searchVendita(giocatore, terreno);
		boolean exists = vendita != null;
		int quantitaTotale;
		if(exists) {
			quantitaTotale = quantita+vendita.getQuantita();
		} else {
			quantitaTotale = quantita;
		}
		if(!puoVendere(pastore, terreno, quantitaTotale, prezzo)) {
			return false;
		}
		List<OggettoVendita> vendite = giocatore.getTerreniVendita();
		List<OggettoVendita> venditeS = status.getCompravendita();
		if(exists) {
			vendite.remove(vendita);
			venditeS.remove(vendita);
			vendita.setPrezzo(prezzo);
			vendita.setQuantita(quantitaTotale);
		} else {
			vendita = new OggettoVendita(giocatore, terreno, quantitaTotale, prezzo);
		}
		vendite.add(vendita);
		venditeS.add(vendita);
		return true;
	}
	
	/**
	 * Cerca se il giocatore sta già vendendo una tessera regione del tipo selezionato
	 * @param giocatore giocatore da controllare
	 * @param terreno tipo terreno
	 * @return oggettovendita se presente
	 */
	public OggettoVendita searchVendita(Giocatore gio, Terreno terreno) {
		Giocatore giocatore = giocatoreEffettivo(gio);
		List<OggettoVendita> vendita = status.getCompravendita();
		for(int i = 0; i < vendita.size(); i++) {
			OggettoVendita tmp = vendita.get(i);
			if(giocatore.equals(tmp.getGiocatore()) && terreno.equals(tmp.getTipoTerreno())) {
				return tmp;
			}
		}
		return null;
	}
	
	/**
	 * Giocatore1 compra una data quantità di terreno da giocatore 2
	 * @param giocatore1
	 * @param giocatore2
	 * @param terreno
	 * @param quantita
	 * @return
	 */
	public boolean compra(Giocatore gioca1, Giocatore gioca2, Terreno terreno, int quantita) {
		Giocatore giocatore1 = giocatoreEffettivo(gioca1);
		Giocatore giocatore2 = giocatoreEffettivo(gioca2);
		OggettoVendita vendita = searchVendita(giocatore2, terreno);
		if(vendita == null) {
			return false;
		}
		int spesa = quantita*vendita.getPrezzo();
		int balance = giocatore1.getMonete()-spesa;
		if(balance < 0) {
			return false;
		}
		int nuovaQuantita = vendita.getQuantita()-quantita;
		if(nuovaQuantita < 0) {
			return false;
		}
		giocatore1.setMonete(balance);
		balance = giocatore2.getMonete()+spesa;
		giocatore2.setMonete(balance);
		vendita.setQuantita(nuovaQuantita);
		Map<Terreno, Integer> posseduti = giocatore1.getTerreniComprati();
		int posseduto = posseduti.get(terreno);
		posseduti.put(terreno, posseduto+quantita);
		posseduti = giocatore2.getTerreniComprati();
		posseduto = posseduti.get(terreno);
		posseduti.put(terreno, posseduto-quantita);
		if(nuovaQuantita == 0) {
			status.getCompravendita().remove(vendita);
			giocatore2.getTerreniVendita().remove(vendita);
		}
		return true;
	}

	/**
	 * Crea un giocatore e i pastori ad esso assegnati
	 * @param colore colore del giocatore
	 * @param terrenoIniziale terreno iniziale del giocatore
	 */
	private void creaGiocatore(Colore colore, Terreno terrenoIniziale) {
		int monete = 20;
		int numeroPastori = 1;
		if(status.getNumeroGiocatori() == 2) {
			monete = 30;
			numeroPastori = 2;
		}
		Giocatore giocatore = new Giocatore(monete, colore);
		giocatore.setTerrenoIniziale(terrenoIniziale);
		List<Pastore> pastori = giocatore.getPastori();
		for(int i = 0; i < numeroPastori; i++) {
			Pastore tmp = new Pastore(giocatore, i);
			pastori.add(tmp);
		}
		if(pastori.size() == 1) {
			giocatore.setPastoreCorrente(pastori.get(0));
		}
		status.getGiocatori().add(giocatore);
	}	
	
	/**
	 * Crea i giocatori
	 * @param colori arraylist dei colori dei giocatori (valori unici)
	 * @throws ColorException 
	 */
	public void creaGiocatori(List<Colore> colori) throws ColorException {
		if(!coloriUnici(colori)) {
			throw new ColorException("Colori giocatori non unici!");
		}
		int numeroGiocatori = status.getNumeroGiocatori();
		if(numeroGiocatori != colori.size()) {
			throw new ColorException("Numero di giocatori errato!");
		}
		List<Terreno> random = Terreno.getRandoms();
		for(int i = 0; i < colori.size(); i++) {
			creaGiocatore(colori.get(i), random.get(i));
		}
	}
	
	/**
	 * Posiziona un pastore
	 * @param giocatore giocatore proprietario del pastore
	 * @param i indice del pastore
	 * @param casella casella su cui posizionarlo
	 */
	public boolean posizionaPastore(Giocatore gio, int i, Casella cas) {
		Giocatore giocatore = giocatoreEffettivo(gio);
		Casella casella = (Casella) status.getMappa().trovaCasella(cas.getId());
		Pastore pastore = giocatore.getPastori().get(i);
		return setCasellaInizialePastore(pastore, casella);
	}
	/**
	 * Imposta il pastore su una data casella
	 * @param pastore pastore da posizionare
	 * @param casella casella su cui mettere il pastore
	 * @return se l'operazione è andata a buon fine
	 */
	private boolean setCasellaInizialePastore(Pastore pas, Casella cas) {
		Pastore pastore = pastoreEffettivo(pas);
		Casella casella = (Casella) status.getMappa().trovaCasella(cas.getId());
		if(!casella.hasPastore() && pastore.getCasella() == null) {
			pastore.setCasella(casella);
			casella.setPastore(pastore);
			return true;
		}
		return false;
	}
	
	/**
	 * Verifica se i colori per impostare i giocatori sono unici
	 * @param colori arraylist dei colori dei giocatori
	 * @return se i colori sono unici
	 */
	public boolean coloriUnici(List<Colore> colori) {
		Set<Colore> unique = new HashSet<Colore>(colori);
		if(colori.size() != unique.size()) {
			return false;
		}
		return true;
	}
	
	
	/**
	 * Calcola la classifica finale e decreta il vincitore
	 * @return Array contenente la classifica da stampare e il colore del vincitore
	 */
	public String[] decretaVincitore() {
		List<Giocatore> giocatori = status.getGiocatori();
		Map<Terreno, Integer> statistiche = new HashMap<>();
		for(int i = 0; i < Terreno.values().length; i++) {
			statistiche.put(Terreno.values()[i], 0);
		}
		List<Regione> all = status.getMappa().allRegione();
		for(int i = 0; i < all.size(); i++) {
			Regione tmp = all.get(i);
			Terreno type = all.get(i).getType();
			int value = statistiche.get(type);
			value += tmp.getArieti();
			value += tmp.getPecore();
			value += tmp.hasPecoraNera() ? 2 : 0;
			statistiche.remove(type);
			statistiche.put(type, value);
		}
		int count = status.getNumeroGiocatori();
		int[][] stats = new int[count][2];
		for(int i = 0; i < giocatori.size(); i++) {
			int value = giocatori.get(i).getMonete();
			Map<Terreno, Integer> terreni = giocatori.get(i).getTerreniComprati();
			for(int j = 0; j < Terreno.values().length; j++) {
				value += terreni.get(Terreno.values()[j])*statistiche.get(Terreno.values()[j]);
			}
			stats[i][0] = i;
			stats[i][1] = value;
		}
		for(int i = 0; i < count-1; i++) {
			for(int j = i; j < count; j++) {
				if(stats[i][1] < stats[j][1]) {
					int[] tmp = {stats[i][0], stats[i][1]};
					stats[i][0] = stats[j][0];
					stats[i][1] = stats[j][1];
					stats[j][0] = tmp[0];
					stats[j][1] = tmp[1];
					j = i;
				}
			}
		}
		String win = giocatori.get(stats[0][0]).getColore().toString();
		String ret = "Ha vinto il giocatore "+win+"\n\nClassifica:\n";
		for(int i = 0; i < count; i++) {
			ret += "Giocatore "+giocatori.get(stats[i][0]).getColore()+" con "+stats[i][1]+" punti\n";
		}
		return new String[] {ret , win};
	}
	
	/**
	 * Fix per rete, trova l'oggetto giocatore vero.
	 * @param giocatore giocatore da trovare
	 * @return giocatore effettivo
	 */
	public Giocatore giocatoreEffettivo(Giocatore giocatore) {
		if(giocatore == null) {
			return null;
		}
		List<Giocatore> lg = status.getGiocatori();
		for(int i = 0; i < lg.size(); i++) {
			if(lg.get(i).equals(giocatore)) {
				return lg.get(i);
			}
		}
		return null;
	}
	
	/**
	 * Fix per rete, trova l'oggetto pastore vero
	 * @param pastore pastore da trovare
	 * @return pastore effettivo
	 */
	public Pastore pastoreEffettivo(Pastore pastore) {
		if(pastore == null) {
			return null;
		}
		Giocatore giocatore = giocatoreEffettivo(pastore.getGiocatore());
		if(giocatore == null) {
			return null;
		}
		List<Pastore> pst = giocatore.getPastori();
		for(int i = 0; i < pst.size(); i++) {
			if(pst.get(i).equals(pastore)) {
				return giocatore.getPastori().get(i);
			}
		}
		return null;
	}
}