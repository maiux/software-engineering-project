package it.polimi.lamalfa_maioli.controller;

import java.io.*;

/**
 * Classe per la serializzazione
 * @author lamalfa_maioli
 *
 */
public class Serializator {
	
	/**
	 * Costruttore privato
	 */
	private Serializator() {
		// Null
	}
	
	/**
	 * Crea un oggetto a partire da un array di byte
	 * @param data array di byte
	 * @return oggetto serializzato
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public final static Object fromString( byte[] data ) throws IOException , ClassNotFoundException {
		ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
		Object o  = ois.readObject();
		ois.close();
		return o;
	}
	
	/**
	 * Trasforma l'oggetto in un array di byte
	 * @param o Oggetto da serializzare
	 * @return array di byte
	 * @throws IOException
	 */
	public final static byte[] toString( Serializable o ) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    ObjectOutputStream oos = new ObjectOutputStream( baos );
	    oos.writeObject( o );
	    oos.close();
	    return baos.toByteArray();
	}
}