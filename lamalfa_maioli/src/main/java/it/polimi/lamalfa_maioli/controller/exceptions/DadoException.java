package it.polimi.lamalfa_maioli.controller.exceptions;

import java.io.Serializable;

/**
 * Eccezione sollevata per comunicare il risultato di un dado
 * @author lamalfa_maioli
 *
 */
public class DadoException extends Exception  implements Serializable {
	
	private static final long serialVersionUID = -7384411129916057192L;

	/**
	 * Costruttore
	 */
	public DadoException() {
		super();
	}
	
	/**
	 * Costruttore
	 * @param message messaggio per descrivere l'eccezione
	 */
	public DadoException(String message) {
		super(message);
	}
	
	/**
	 * Costruttore nel caso di eccezione ramificata
	 * @param message messaggio per descrivere l'eccezione
	 * @param e eccezione vecchia
	 */
	public DadoException(String message, Throwable cause) {
		super(message, cause);
	}
}
