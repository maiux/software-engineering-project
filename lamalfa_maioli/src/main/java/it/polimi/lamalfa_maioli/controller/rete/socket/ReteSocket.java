package it.polimi.lamalfa_maioli.controller.rete.socket;

import it.polimi.lamalfa_maioli.controller.GameCore;
import it.polimi.lamalfa_maioli.controller.Serializator;
import it.polimi.lamalfa_maioli.controller.exceptions.ColorException;
import it.polimi.lamalfa_maioli.controller.exceptions.MyRuntimeException;
import it.polimi.lamalfa_maioli.controller.rete.Rete;
import it.polimi.lamalfa_maioli.model.Casella;
import it.polimi.lamalfa_maioli.model.Colore;
import it.polimi.lamalfa_maioli.model.Giocatore;
import it.polimi.lamalfa_maioli.model.Pastore;
import it.polimi.lamalfa_maioli.model.Regione;
import it.polimi.lamalfa_maioli.view.CompravenditaCompra;
import it.polimi.lamalfa_maioli.view.CompravenditaVendi;
import it.polimi.lamalfa_maioli.view.ControllerMappa;
import it.polimi.lamalfa_maioli.view.ReteClient;
import it.polimi.lamalfa_maioli.view.ReteServer;
import it.polimi.lamalfa_maioli.view.ControllerMappa.Azione;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

/**
 * Classe per l'avvio di una partita in rete attraverso socket
 * @author lamalfa_maioli
 *
 */
public class ReteSocket implements Rete, Runnable {
	private ServerSocket serverSocket;
	private Socket socket;
	private int numeroGiocatori = 1;
	private volatile GameCore core;
	private List<Socket> clients = new ArrayList<>();
	private boolean started = false;
	private int id = 0;
	private ControllerMappa frame;
	private boolean pastoriOk = false;
	
	/**
	 * Costruttore utilizzato per avviare il server.
	 * @param port porta in cui il server si deve mettere in ascolto
	 * @throws IOException
	 */
	public ReteSocket(int port) throws IOException {
		this.serverSocket = new ServerSocket(port);
		startServer();
	}
	
	/**
	 * Costruttore utilizzato per avviare il client.
	 * @param host indirizzo del server
	 * @param port porta del server
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public ReteSocket(String host, int port) throws UnknownHostException, IOException {
		this.socket = new Socket(host, port);
		startClient();
	}	
		
	/**
	 * @return se la partita è iniziata
	 */
	public final boolean started() {
		return started;
	}
	
	/**
	 * @return gamecore con synchronized
	 */
	public final synchronized GameCore getCore() {
		return core;
	}
	
	/**
	 * Imposta il gamecore senza synchronized
	 * @param core gamecore da impostare
	 */
	public final void setNotCore(GameCore core) {
		this.core = core;
	}
		
	/**
	 * @see Rete#startServer()
	 */
	public final void startServer() throws IOException {
		ReteServer viewServer = new ReteServer(serverSocket.getInetAddress(), serverSocket.getLocalPort());
		while(!viewServer.getStart()) {
			try {
				serverSocket.setSoTimeout(2500);
				Socket newSocket = serverSocket.accept();
				if(newSocket.isConnected()) {
					clients.add(newSocket);
					new ReteSocketClientThread(newSocket, this, numeroGiocatori).start();
					viewServer.updateClientConnessi(numeroGiocatori);
					numeroGiocatori++;
					if(numeroGiocatori > 3) {
						viewServer.inizia();
					}
				}
			} catch (SocketTimeoutException e) {
				// Ignore
			} catch (IOException e) {
				throw new MyRuntimeException(e);
			}
		}
		serverSocket.setSoTimeout(0);
		inizializza();
		started = true;
		apriGUI();
		frame.setServer(this);
		while(!core.getStatus().getGiocatoreCorrente().equals(me())) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// Ignora
			}
		}
		chiediPastori();
		while(!core.getStatus().gameEnd()) {
			while(!core.getStatus().getGiocatoreCorrente().equals(me())) {
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					// Ignora
				}
			}
			if(core.getStatus().getGiocatoreCorrente().equals(core.getStatus().getGiocatoreIniziale()) && core.getStatus().getNumeroRecinti() < 0) {
				core.getStatus().setGameEnd(true);
				break;
			}
			frame.aggiornaView(core, null);
			core.getStatus().setTurno(core.getStatus().getTurno()+1);
			frame.updateTurno(core.getStatus().getGiocatoreCorrente().getColore().toString());
			eseguiDadi();
			updateView();
			while(core.getStatus().getGiocatoreCorrente().equals(me())) {
				performActions();
			}
			pastoriOk = true;
		}
		String[] winner = core.decretaVincitore();
		if(winner[1].equals(me().getColore().toString())) {
			JOptionPane.showMessageDialog(frame, "Bravo, Hai vinto!\n"+winner[0]);
		} else {
			JOptionPane.showMessageDialog(frame, "Hai perso!\n"+winner[0]);
		}
		frame.dispose();
	}
	
	/**
	 * Inizializza il gamecore e imposta i parametri iniziali
	 * @throws IOException
	 */
	public final void inizializza() throws IOException {
		List<Colore> colori = Colore.getRandoms();
		List<Colore> coloriEffettivi = new ArrayList<>();
		core = new GameCore(numeroGiocatori);
		for(int i = 0; i < numeroGiocatori; i++) {
			coloriEffettivi.add(colori.get(i));
		}
		try {
			core.creaGiocatori(coloriEffettivi);
		} catch (ColorException e) {
			throw new MyRuntimeException(e);
		}
		core.impostaGiocatoreIniziale();
		core.getStatus().setGiocatoreCorrente(core.getStatus().getGiocatoreIniziale());
	}
		
	/**
	 * @see Rete#startClient()
	 */
	public final void startClient() {
		try {
			String msg = new String(readMsg(socket));
			String[] split = msg.split("Client numero ");
			id = (int)Integer.parseInt(split[1].trim());
			ReteClient viewClient = new ReteClient("Client numero "+id+". In attesa del gioco...");
			waitForCore(socket);
			viewClient.dispose();
			apriGUI();
			waitForCore(socket);
			updateView();
			chiediPastori();
			sendMsg(socket, Serializator.toString(core));
			while(!core.getStatus().gameEnd()) {
				waitForCore(socket);
				if(core.getStatus().gameEnd()) {
					break;
				}
				if(me().equals(core.getStatus().getGiocatoreIniziale()) && core.getStatus().getNumeroRecinti() < 0) {
					core.getStatus().setGameEnd(true);
					break;
				}
				frame.aggiornaView(core, null);
				updateView();
				core.getStatus().setTurno(core.getStatus().getTurno()+1);
				frame.updateTurno(core.getStatus().getGiocatoreCorrente().getColore().toString());
				eseguiDadi();
				while(core.getStatus().getGiocatoreCorrente().equals(me())) {
					performActions();
				}
				sendCore(socket);
				pastoriOk = true;
			}
			if(core.getStatus().getGiocatoreIniziale().equals(me())) {
				sendMsg(socket, Serializator.toString(core));
			}
			String[] winner = core.decretaVincitore();
			if(winner[1].equals(me().getColore().toString())) {
				JOptionPane.showMessageDialog(frame, "Bravo, Hai vinto!\n"+winner[0]);
			} else {
				JOptionPane.showMessageDialog(frame, "Hai perso!\n"+winner[0]);
			}
			frame.dispose();
		} catch (IOException e) {
			throw new MyRuntimeException("Errore nel client", e);
		}
	}
	
	/**
	 * Apre la GUI di gioco
	 */
	public void apriGUI() {
		frame = new ControllerMappa(core);
		frame.allButtonsDisabled();
		frame.setVisible(true);
	}
	
	/**
	 * Chiede tramite la gui di posizionare il pastore / i pastori
	 */
	public final void chiediPastori() {
		Giocatore corrente = core.getStatus().getGiocatoreCorrente();
		frame.requestPastore(corrente, core, null);
		core.getStatus().setGiocatoreCorrente(core.getStatus().prossimoGiocatore());
		frame.updateTurno(core.getStatus().getGiocatoreCorrente().getColore().toString());
	}
	
	/**
	 * Rimane in attesa della recezione del gamecore serializzato e lo sostituisce al gamecore corrente
	 * @param socket
	 */
	public final void waitForCore(Socket socket) {
		try {
			core = (GameCore) Serializator.fromString(readMsg(socket));
		} catch (ClassNotFoundException e) {
			throw new MyRuntimeException(e);
		} catch (IOException e) {
			throw new MyRuntimeException(e);
		}
	}
	
	/**
	 * Imposta il core con synchronized
	 * @param core gamecore da impostare
	 */
	public final synchronized void setCore(GameCore core) {
		this.core = core;
	}
	
	/**
	 * Invia il gamecore corrente serializzato al socket
	 * @param socket socket su cui inviare il gamecore
	 */
	public final void sendCore(Socket socket) {
		try {
			sendMsg(socket, Serializator.toString(core));
		} catch (IOException e) {
			throw new MyRuntimeException(e);
		}
	}
	
	/**
	 * Invia un array di byte al socket
	 * @param socket socket a cui inviare i dati
	 * @param bs array di byte da inviare
	 * @throws IOException
	 */
	public final void sendMsg(Socket socket, byte[] bs) throws IOException {
		DataOutputStream dOut = new DataOutputStream(socket.getOutputStream());
		dOut.writeInt(bs.length);
		dOut.write(bs); 
	}
	
	/**
	 * Rimane in attesa di ricevere un array di byte
	 * @param socket socket da cui bisogna ricevere i dati
	 * @return l'array di byte ricevuto
	 * @throws IOException
	 */
	public final byte[] readMsg(Socket socket) throws IOException {
		DataInputStream dIn = new DataInputStream(socket.getInputStream());
		int length = dIn.readInt();                    
		if(length>0) {
		    byte[] message = new byte[length];
		    dIn.readFully(message, 0, message.length);
		    return message;
		}
		return new byte[] {};
	}
	
	/**
	 * @return il giocatore corrente
	 */
	public final Giocatore me() {
		return core.giocatoreEffettivo(core.getStatus().getGiocatori().get(id));
	}
	
	/**
	 * Aggiorna vari parametri nella GUI
	 */
	public final void updateView() {
		viewPastoriInit();
		List<Giocatore> giocatori = core.getStatus().getGiocatori();
		for(int i = 0; i < giocatori.size(); i++) {
			List<Pastore> pastori = giocatori.get(i).getPastori();
			for(int j = 0; j < pastori.size(); j++) {
				Pastore tmp = pastori.get(j);
				if(tmp.getCasella() != null && frame.getViewPastore(tmp) != null) {
					frame.getViewPastore(tmp).setPastore(tmp);
					frame.getViewPastore(tmp).spostaPastoreSuCasella(frame.getViewCasella(tmp.getCasella()));
				}
			}
		
		}
		List<Casella> caselle = core.getStatus().getMappa().allCasella();
		for(int i = 0; i < caselle.size(); i++) {
			frame.getViewCasella(caselle.get(i)).setCasella(caselle.get(i));
			frame.getViewCasella(caselle.get(i)).rUpdate();
		}
		List<Regione> regioni = core.getStatus().getMappa().allRegione();
		for(int i = 0; i < regioni.size(); i++) {
			frame.getViewRegione(regioni.get(i)).setRegione(regioni.get(i));
			frame.getViewRegione(regioni.get(i)).update();
		}
	}
	
	/**
	 * Esegue i controlli da fare all'inizio del turno (lancio dadi + verifica agnello)
	 */
	public final void eseguiDadi() {
		core.dadoPecoraNera();
		core.dadoLupo();
		core.verificaAgnelli();
	}
	
	/**
	 * Esegue il turno del giocatore
	 */
	public final void performActions() {
		Giocatore corrente = me();
		Pastore pastoreCorrente = core.pastoreEffettivo(corrente.getPastoreCorrente());
		frame.updateContatori(corrente, core);
		frame.updateButtons(corrente, core);
		frame.updateNumeroTurno(core.getStatus().getTurno()+"");
		if(pastoreCorrente == null) {
			frame.richiediPastoreCorrente(corrente, null);
		}
		switch(frame.getAzione()) {
			case MUOVI_PASTORE:
				frame.muoviPastore(corrente, core, null);
				break;
			case MUOVI_PECORA:
				frame.muoviPecora(corrente, core, null);
				break;
			case MUOVI_ARIETE:
				frame.muoviAriete(corrente, core, null);
				break;
			case MUOVI_PECORA_NERA:
				frame.muoviPecoraNera(corrente, core, null);
				break;
			case SPARATORIA:
				frame.sparatoria(corrente, core, null);
				break;
			case ACCOPPIA:
				frame.accoppia(corrente, core, null);
				break;
			case COMPRA_TERRENO:
				frame.compraTerreno(corrente, core, null);
				break;
			case COMPRAVENDITA:
				Object[] options = { "Compra", "Vendi", "Annulla" };
				int n = JOptionPane.showOptionDialog(frame, "Che azione intendi fare?", "Compravendita", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[2]);
				switch(n) {
					case 0:
						frame.setAzione(Azione.COMPRAVENDITA_COMPRA);
						break;
					case 1:
						frame.setAzione(Azione.COMPRAVENDITA_VENDI);
						break;
					default:
						frame.reset(core);
						break;
				}
				break;
			case COMPRAVENDITA_COMPRA:
				List<Giocatore> giocatori = core.getStatus().getGiocatori();
				List<Giocatore> compraDa = new ArrayList<>();
				for(int i = 0; i < giocatori.size(); i++) {
					if(!corrente.equals(giocatori.get(i))) {
						compraDa.add(giocatori.get(i));
					}
				}
				int indiceGiocatore = 0;
				int compraDaSize = compraDa.size();
				if(compraDaSize > 1) {
					Object[] gopt = new Object[compraDaSize];
					for(int i = 0; i < compraDaSize; i++) {
						gopt[i] = new String(compraDa.get(i).getColore().toString());
					}
					indiceGiocatore = JOptionPane.showOptionDialog(frame, "Che azione intendi fare?", "Compravendita", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, gopt, gopt[0]);
				}
				if(indiceGiocatore < 0 || indiceGiocatore >= compraDaSize) {
					frame.reset(core);
					break;
				}
				CompravenditaCompra fc = new CompravenditaCompra(core, compraDa.get(indiceGiocatore), null);
				while(!fc.getEnd()) {
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						// Ignora
					}
				}
				String returnFc = fc.getStatus();
				if(!"Acquisti: \n".equals(returnFc) && returnFc.length() > 0) {
					JOptionPane.showMessageDialog(frame, returnFc);
				}
				frame.reset(core);
				break;
			case COMPRAVENDITA_VENDI:
				CompravenditaVendi fv = new CompravenditaVendi(core, null);
				while(!fv.getEnd()) {
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						// Ignora
					}
				}
				String returnFv = fv.getStatus();
				if(!"Status vendita:\n".equals(returnFv) && returnFv.length() > 0) {
					JOptionPane.showMessageDialog(frame, returnFv);
				}
				frame.reset(core);
				break;
			case FINE_TURNO:
				if(pastoreCorrente.getNumeroMossa() < 3) {
					JOptionPane.showMessageDialog(frame, "Devi finire le tue mosse prima di terminare il turno!");
					frame.reset(core);
					break;
				} else {
					if(corrente.getPastori().size() == 2) {
						corrente.setPastoreCorrente(null);
					}
					pastoreCorrente.reset(pastoreCorrente.getCasella());
					me().getPastori().get(pastoreCorrente.getId()).setCasella(pastoreCorrente.getCasella());
					core.getStatus().setGiocatoreCorrente(core.getStatus().prossimoGiocatore());
					frame.reset(core);
					frame.allButtonsDisabled();
					frame.socketUpdateTurno("In attesa degli altri giocatori...");
				}
				break;
			default:
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					// Ignora
				}
				break;
		}
	}
	
	/**
	 * Inizializza le view dei pastori
	 */
	public final void viewPastoriInit() {
		if(pastoriOk) {
			return;
		}
		List<Giocatore> giocatori = core.getStatus().getGiocatori();
		for(int i = 0; i < giocatori.size(); i++) {
			List<Pastore> pastori = giocatori.get(i).getPastori();
			for(int j = 0; j < pastori.size(); j++) {
				Pastore pastore = pastori.get(j);
				if(pastore.getCasella() != null && frame.getViewPastore(pastore) == null && !giocatori.get(i).equals(me())) {
					frame.creaViewPastore(giocatori.get(i), core);
				}
			}
		}
	}

	/**
	 * @see Runnable#run()
	 */
	@Override
	public void run() {
	}
}
