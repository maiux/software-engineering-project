package it.polimi.lamalfa_maioli.view;
import it.polimi.lamalfa_maioli.controller.GameCore;
import it.polimi.lamalfa_maioli.controller.exceptions.MyRuntimeException;
import it.polimi.lamalfa_maioli.controller.rete.rmi.ServerInterface;
import it.polimi.lamalfa_maioli.model.Giocatore;
import it.polimi.lamalfa_maioli.model.OggettoVendita;
import it.polimi.lamalfa_maioli.model.Terreno;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;





import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JSpinner;

/***
 * classe che gestisce compravendita
 * @author lamalfa_maioli
 *
 */
public class CompravenditaCompra extends JFrame implements ActionListener, Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5661263235477250652L;
	private JPanel contentPane;
	private JButton btnBuy = new JButton("Conferma");
	private GameCore core;
	private Giocatore giocatore;
	private JSpinner spinnerPalude, spinnerForesta, spinnerSpiaggia, spinnerCollina, spinnerMontagna, spinnerPascolo;
	private Map<Terreno, JSpinner> mapSpinner = new HashMap<>();
	private String alert = "";
	private boolean end = false;
	private ServerInterface server = null;
	
	/**
	 * Costruttore: inizializza un frame per la compravendita, impostando i dati del giocatore da cui si vuole comprare
	 * @param core GameCore già inizializzato
	 * @param giocatore giocatore da cui si vuole comprare qualcosa
	 * @param server 
	 */
	public CompravenditaCompra(GameCore core, Giocatore giocatore, ServerInterface server) {
		this.core = core;
		this.server = server;
		this.giocatore = giocatore;
		List<OggettoVendita> vendita = giocatore.getTerreniVendita();
		final Map<Terreno, Integer> mapPrezzo = new HashMap<>(); 
		final Map<Terreno, Integer> mapQuantitaMax = new HashMap<>();
		for(int i = 0; i < Terreno.values().length; i++) {
			mapPrezzo.put(Terreno.values()[i], 0);
			mapQuantitaMax.put(Terreno.values()[i], 0);
		}
		for(int i = 0; i < vendita.size(); i++) {
			OggettoVendita tmp = vendita.get(i);
			mapPrezzo.put(tmp.getTipoTerreno(), tmp.getPrezzo());
			mapQuantitaMax.put(tmp.getTipoTerreno(), tmp.getQuantita());
		}
		
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setBounds(100, 100, 480, 443);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel labelTessera1 = new JLabel("");
		labelTessera1.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/tesseraTerreno1.png")));
		labelTessera1.setBounds(61, 41, 80, 80);
		contentPane.add(labelTessera1);
		
		JLabel labelTessera2 = new JLabel(" ");
		labelTessera2.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/tesseraTerreno2.png")));
		labelTessera2.setBounds(188, 41, 80, 80);
		contentPane.add(labelTessera2);
		
		JLabel labelTessera3 = new JLabel(" ");
		labelTessera3.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/tesseraTerreno3.png")));
		labelTessera3.setBounds(315, 41, 80, 80);
		contentPane.add(labelTessera3);
		
		JLabel labelTessera4 = new JLabel(" ");
		labelTessera4.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/tesseraTerreno4.png")));
		labelTessera4.setBounds(61, 196, 80, 80);
		contentPane.add(labelTessera4);
		
		JLabel labelTessera5 = new JLabel(" ");
		labelTessera5.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/tesseraTerreno5.png")));
		labelTessera5.setBounds(188, 196, 80, 80);
		contentPane.add(labelTessera5);
		
		JLabel labelTessera6 = new JLabel(" ");
		labelTessera6.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/tesseraTerreno6.png")));
		labelTessera6.setBounds(315, 196, 80, 80);
		contentPane.add(labelTessera6);
		
		JLabel lblGiocatoreI = new JLabel("Compra dal Giocatore "+giocatore.getColore().toString());
		lblGiocatoreI.setBounds(10, 11, 191, 14);
		contentPane.add(lblGiocatoreI);
		
		btnBuy.setBounds(352, 347, 89, 23);
		btnBuy.addActionListener(this);
		contentPane.add(btnBuy);
		
		spinnerPascolo = new JSpinner();
		spinnerPascolo.setBounds(112, 132, 29, 20);
		spinnerPascolo.addChangeListener(new ChangeListener() {      
			  @Override
			  public void stateChanged(ChangeEvent e) {
				  	int value = (Integer) spinnerPascolo.getValue();
				  	int max = mapQuantitaMax.get(Terreno.PASCOLO);
				  	if(value < 0) {
				  		spinnerPascolo.setValue(Integer.valueOf(0));
				  	} else if(value > max) {
				  		spinnerPascolo.setValue(Integer.valueOf(max));
				  	}		    
			  }
		});
		contentPane.add(spinnerPascolo);
		
		spinnerForesta = new JSpinner();
		spinnerForesta.setBounds(239, 132, 29, 20);
		spinnerForesta.addChangeListener(new ChangeListener() {      
			  @Override
			  public void stateChanged(ChangeEvent e) {
				  	int value = (Integer) spinnerForesta.getValue();
				  	int max = mapQuantitaMax.get(Terreno.FORESTA);
				  	if(value < 0) {
				  		spinnerForesta.setValue(Integer.valueOf(0));
				  	} else if(value > max) {
				  		spinnerForesta.setValue(Integer.valueOf(max));
				  	}		    
			  }
		});
		contentPane.add(spinnerForesta);
		
		spinnerPalude = new JSpinner();
		spinnerPalude.setBounds(366, 132, 29, 20);
		spinnerPalude.addChangeListener(new ChangeListener() {      
			  @Override
			  public void stateChanged(ChangeEvent e) {
				  	int value = (Integer) spinnerPalude.getValue();
				  	int max = mapQuantitaMax.get(Terreno.PALUDE);
				  	if(value < 0) {
				  		spinnerPalude.setValue(Integer.valueOf(0));
				  	} else if(value > max) {
				  		spinnerPalude.setValue(Integer.valueOf(max));
				  	}		    
			  }
		});
		contentPane.add(spinnerPalude);
		
		spinnerCollina = new JSpinner();
		spinnerCollina.setBounds(112, 287, 29, 20);
		spinnerCollina.addChangeListener(new ChangeListener() {      
			  @Override
			  public void stateChanged(ChangeEvent e) {
				  	int value = (Integer) spinnerCollina.getValue();
				  	int max = mapQuantitaMax.get(Terreno.COLLINA);
				  	if(value < 0) {
				  		spinnerCollina.setValue(Integer.valueOf(0));
				  	} else if(value > max) {
				  		spinnerCollina.setValue(Integer.valueOf(max));
				  	}		    
			  }
		});
		contentPane.add(spinnerCollina);
		
		spinnerSpiaggia = new JSpinner();
		spinnerSpiaggia.setBounds(239, 287, 29, 20);
		spinnerSpiaggia.addChangeListener(new ChangeListener() {      
			  @Override
			  public void stateChanged(ChangeEvent e) {
				  	int value = (Integer) spinnerSpiaggia.getValue();
				  	int max = mapQuantitaMax.get(Terreno.SPIAGGIA);
				  	if(value < 0) {
				  		spinnerSpiaggia.setValue(Integer.valueOf(0));
				  	} else if(value > max) {
				  		spinnerSpiaggia.setValue(Integer.valueOf(max));
				  	}		    
			  }
		});
		contentPane.add(spinnerSpiaggia);
		
		spinnerMontagna = new JSpinner();
		spinnerMontagna.setBounds(366, 287, 29, 20);
		spinnerMontagna.addChangeListener(new ChangeListener() {      
			  @Override
			  public void stateChanged(ChangeEvent e) {
				  	int value = (Integer) spinnerMontagna.getValue();
				  	int max = mapQuantitaMax.get(Terreno.MONTAGNA);
				  	if(value < 0) {
				  		spinnerMontagna.setValue(Integer.valueOf(0));
				  	} else if(value > max) {
				  		spinnerMontagna.setValue(Integer.valueOf(max));
				  	}		    
			  }
		});
		contentPane.add(spinnerMontagna);
		
		mapSpinner.put(Terreno.COLLINA, spinnerCollina);
		mapSpinner.put(Terreno.FORESTA, spinnerForesta);
		mapSpinner.put(Terreno.SPIAGGIA, spinnerSpiaggia);
		mapSpinner.put(Terreno.PALUDE, spinnerPalude);
		mapSpinner.put(Terreno.MONTAGNA, spinnerMontagna);
		mapSpinner.put(Terreno.PASCOLO, spinnerPascolo);
		
		
		JLabel lblCompra = new JLabel("Compra:");
		lblCompra.setBounds(61, 135, 46, 14);
		contentPane.add(lblCompra);
		
		JLabel label = new JLabel("Compra:");
		label.setBounds(188, 135, 46, 14);
		contentPane.add(label);
		
		JLabel label1 = new JLabel("Compra:");
		label1.setBounds(315, 135, 46, 14);
		contentPane.add(label1);
		
		JLabel label2 = new JLabel("Compra:");
		label2.setBounds(61, 290, 46, 14);
		contentPane.add(label2);
		
		JLabel label3 = new JLabel("Compra:");
		label3.setBounds(188, 290, 46, 14);
		contentPane.add(label3);
		
		JLabel label4 = new JLabel("Compra:");
		label4.setBounds(315, 290, 46, 14);
		contentPane.add(label4);
		
		JLabel lblCostoUnitX = new JLabel("Costo unità: "+mapPrezzo.get(Terreno.PASCOLO));
		lblCostoUnitX.setBounds(61, 160, 80, 14);
		contentPane.add(lblCostoUnitX);
		
		JLabel label5 = new JLabel("Costo unità: "+mapPrezzo.get(Terreno.FORESTA));
		label5.setBounds(188, 160, 80, 14);
		contentPane.add(label5);
		
		JLabel label6 = new JLabel("Costo unità: "+mapPrezzo.get(Terreno.PALUDE));
		label6.setBounds(315, 160, 80, 14);
		contentPane.add(label6);
		
		JLabel label7 = new JLabel("Costo unità: "+mapPrezzo.get(Terreno.COLLINA));
		label7.setBounds(61, 312, 80, 14);
		contentPane.add(label7);
		
		JLabel label8 = new JLabel("Costo unità: "+mapPrezzo.get(Terreno.SPIAGGIA));
		label8.setBounds(188, 312, 80, 14);
		contentPane.add(label8);
		
		JLabel label9 = new JLabel("Costo unità: "+mapPrezzo.get(Terreno.MONTAGNA));
		label9.setBounds(315, 310, 80, 14);
		contentPane.add(label9);
		this.setVisible(true);
		this.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		        end = true;
		    }
		});
		
	}
	
	
	
	
	/***
     * funzione importata dall' implements Action Listener, prepara e attiva eventi all'occorrenza di eventi sui bottoni della classe
     *@param ae  il gestore di evento, si occupa di controllare se il determinato bottone viene premuto (previa connessione bottone-gestore_evento)
     */
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton button = (JButton) e.getSource();
        if(button.equals(btnBuy)) {
        	alert = "Acquisti: \n";
        	Giocatore giocatoreCorrente = core.getStatus().getGiocatoreCorrente();
        	for(int i = 0; i < Terreno.values().length-1; i++) {
        		Terreno tmp = Terreno.values()[i];
        		int quantita = (int) mapSpinner.get(tmp).getValue();
        		if(quantita > 0) {
        			boolean mov;
        			if(server != null) {
        				try {
							mov = server.compra(giocatoreCorrente, giocatore, tmp, quantita);
						} catch (RemoteException e1) {
							throw new MyRuntimeException(e1);
						}
        			} else {
        				mov = core.compra(giocatoreCorrente, giocatore, tmp, quantita);
        			}
        			if(mov) {
        				alert += "Comprato "+quantita+" tesser"+(quantita == 1 ? "a" : "e")+" di "+tmp.toString()+"\n";
        			} else {
        				alert += "Impossibile comprare "+quantita+" tesser"+(quantita == 1 ? "a" : "e")+" di "+tmp.toString()+"\n";
        			}
        		}
        	}
        	end = true;
        	this.dispose();
        }
	}
	
	/**
	 * @return se le operazioni di vendita sono state terminate
	 */
	public boolean getEnd() {
		return this.end;
	}
	
	
	/**
	 * @return una stringa contenente il risultato delle operazioni di vendita
	 */
	public String getStatus() {
		return this.alert;
	}
}
