package it.polimi.lamalfa_maioli.view;

import it.polimi.lamalfa_maioli.model.Regione;

import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;

/***
 * classe che gestisce la creazione della viewregione
 * @author lamalfa_maioli
 *
 */
public class ViewRegione implements Serializable {
	private static final long serialVersionUID = -6934214487664603140L;
	private List<JLabel> labelRegione = new ArrayList<>();
	private Regione regione;
	private String nomeRegione;
	private ViewPecoraBianca pecoraBianca;
	private ViewPecoraNera pecoraNera;
	private ViewAriete ariete;
	private ViewAgnello agnello;
	private static ControllerMappa controller;
	private ViewLupo lupo;
	
	/***
	 * aggiorna la regione e i contatori e le label su di essa presenti
	 */
	public void update() {
		pecoraBianca.update();
		pecoraNera.update();
		ariete.update();
		agnello.update();
		lupo.update();
	}
	
	/***
	 * istanzia una ViewRegione che serve a vedere la regione sulla mappa di gioco
	 * @param contentPane pannello della mappa di gioco
	 * @param regioni le label da aggiungere alla viewRegione
	 */
	public ViewRegione(JPanel contentPane, JLabel...regioni) {
		for(JLabel labelRegione : regioni) {
			this.labelRegione.add(labelRegione);
		}
	}
	
	/***
	 * ritorna la view del lupo, se presente
	 * @return la view del lupo
	 */
	public ViewLupo getLupo() {
		return lupo;
	}
	
	/***
	 * imposta casella della ViewRegione
	 * @param regione la regione da impostare
	 */
	public void setListenerRegione(ViewRegione viewregione) {
		for(JLabel reg : labelRegione) {
			reg.addMouseListener ( new MyMouseEvent (viewregione.getRegione(), controller) {
	           private static final long serialVersionUID = 6563181759902930870L;

				public void mousePressed ( MouseEvent e) {
	            	controller.setRegionePremuta((Regione) this.getVertex());
	            }
	        } );
		}

	}

	/***
	 * ritorna il puntatore alla Regione
	 * @return
	 */
	public Regione getRegione() {
		return regione;
	}
	
	/***
	 * imposta il puntatore ViewRegione->Regione
	 * @param regione
	 */
	public void setRegione(Regione regione) {
		this.regione = regione;
		setListenerRegione(this);
		
	}
	
	/***
	 * ritorna il nome della regione (numero da 0 a 18)
	 * @return
	 */
	public String getNomeRegione() {
		return nomeRegione;
	}

	/***
	 * imposta il numero della regione 
	 * @param nomeRegione
	 */
	public void setNomeRegione(String nomeRegione) {
		this.nomeRegione = nomeRegione;
	}
	
	/***
	 * getter pecoraBianca della ViewRegione
	 * @return la pecora bianca sulla regione
	 */
	public ViewPecoraBianca getPecoraBianca() { 
		return pecoraBianca;
	}

	/***
	 * getter pecoraNera della ViewRegione
	 * @return la pecora nera sulla regione
	 */
	public ViewPecoraNera getPecoraNera() {
		return pecoraNera;
	}
	
	/***
	 * getter ariete della ViewRegione
	 * @return l'ariete sulla regione
	 */
	public ViewAriete getAriete() {
		return ariete;
	}
	
	/***
	 * getter agbello della ViewRegione
	 * @return l'agnello sulla regione
	 */
	public ViewAgnello getAgnello() {
		return agnello;
	}
	
	/***
	 * setter percora bianca sulla regione
	 * @param pecoraBianca la pecora bianca della regione
	 */
	public void setPecoraBianca(ViewPecoraBianca pecoraBianca) {
		this.pecoraBianca = pecoraBianca;
	}
	
	/***
	 * setter percora nera sulla regione
	 * @param pecoraNera la pecora nera della regione
	 */
	public void setPecoraNera(ViewPecoraNera pecoraNera) {
		this.pecoraNera = pecoraNera;
	}
	
	/***
	 * setter ariete sulla regione
	 * @param ariete l'ariete sulla regione
	 */
	public void setAriete(ViewAriete ariete) {
		this.ariete = ariete;
	}
	
	/***
	 * setter agnello sulla regione
	 * @param agnello l'agnello sulla regione
	 */
	public void setAgnello(ViewAgnello agnello) {
		this.agnello = agnello;
	}
	
	/***
	 * setter lupo sulla regione
	 * @param lupo il lupo sulla regione
	 */
	public void setLupo(ViewLupo lupo) {
		this.lupo = lupo;
	}
	
	/***
	 * setter controller 
	 * @param controller il controller della mappa di gioco
	 */
	public void setController(ControllerMappa controller) {
		ViewRegione.controller = controller;
	}
	
}
