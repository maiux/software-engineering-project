package it.polimi.lamalfa_maioli.view;

import java.awt.Color;
import java.awt.Font;
import java.io.Serializable;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/***
 * classe che gestisce la visualizzazione a schermo dell'ariete
 * @author lamalfa_maioli
 *
 */
public class ViewAriete implements Serializable {
		
	/**
	 * 
	 */
	private static final long serialVersionUID = -8736605975692253117L;
	private ViewRegione viewRegione;
	private JLabel labelAriete;
	private JLabel labelNumeroArieti;	
	private int labelX;
	private int labelY;
	private int altezza;
	private int larghezza;
	
	/***
	 * costruttore Ariete, inizializza l'immagine sulla mappa
	 * @param contentPane il pannello della mappa
	 * @param viewReg la regione su cui e' la Ariete
	 * @param x coordinata ascisse
	 * @param y coordinata ordinate
	 * @param alt grandezza label Ariete
	 * @param lar larghezza label Ariete
	 */
	public ViewAriete(JPanel contentPane, ViewRegione viewReg, int x, int y, int alt,int lar) {
		this.labelAriete = new JLabel(" ");
		this.labelNumeroArieti = new JLabel();
		this.viewRegione = viewReg;
		this.labelX = x;
		this.labelY = y;
		this.altezza = alt;
		this.larghezza = lar;
		this.labelAriete.setBounds(labelX, labelY, altezza, larghezza);
		this.labelNumeroArieti.setForeground(Color.RED);
		this.labelNumeroArieti.setFont(new Font("Tahoma", Font.BOLD, 12));
		this.labelNumeroArieti.setBounds(labelX, labelY, 10, 10);
		this.labelAriete.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/ariete.png")));
		this.update();
		contentPane.add(labelNumeroArieti);
		contentPane.add(labelAriete);
	
	}
	
	/***
	 * imposta il parametro che consente poi di stampare a schermo il numero di arieti 
	 * @param num il numero di arieti
	 */
	public final void update() {
		String tmp = String.format("%d", viewRegione.getRegione().getArieti());
		this.labelNumeroArieti.setText(tmp);
	}
		
	/***
	 * 
	 * @return coordinata x della label
	 */
	public int getX() {
		return labelX;
	}
		
	/***
	 * 
	 * @return coordinata y della label
	 */
	public int getY() {
		return labelY;
	}

}