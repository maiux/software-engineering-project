package it.polimi.lamalfa_maioli.view;

import java.awt.Color;
import java.awt.Font;
import java.io.Serializable;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;


/***
 * classe che gestisce la visualizzazione della pecora nera su schermo
 * @author lamalfa_maioli
 *
 */
public class ViewPecoraNera  implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7914471009700157769L;
	private JLabel labelPecoraNera;
	private JLabel labelNumPecoreNere;	
	private ViewRegione viewRegione;
	private int labelX;
	private int labelY;
	private int altezza;
	private int larghezza;
	
	/***
	 * costruttore pecora nera
	 * @param contentPane il pannello della mappa
	 * @param viewReg la regione su cui si trova la pecora nera
	 * @param x coordinata ascisse pecora nera
	 * @param y cordinata ordinate pecora nera
	 * @param alt altezza label pecora nera
	 * @param lar largezza label pecora nera
	 */
	public ViewPecoraNera(JPanel contentPane, ViewRegione viewReg, int x, int y, int alt,int lar) {
		this.labelPecoraNera = new JLabel(" ");
		this.labelNumPecoreNere = new JLabel();
		this.viewRegione = viewReg;
		this.labelX = x;
		this.labelY = y;
		this.altezza = alt;
		this.larghezza = lar;
		this.labelPecoraNera.setBounds(labelX, labelY, altezza, larghezza);
		this.labelNumPecoreNere.setForeground(Color.BLACK);
		this.labelNumPecoreNere.setFont(new Font("Tahoma", Font.BOLD, 12));
		this.labelNumPecoreNere.setBounds(labelX, labelY, 10, 10);
		this.labelPecoraNera.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/pecoranera.png")));
		update();
		contentPane.add(labelNumPecoreNere);
		contentPane.add(labelPecoraNera);
	}
	
	/***
	 * ritorna la viewregione con su la pecoranera
	 * @return la viewregione con su la pecora nera
	 */
	public ViewRegione getRegione() {
		return viewRegione;
	}
	/***
	 * imposta numero pecore nere (0 o 1)
	 * @param num
	 */
	public final void update() {
		labelPecoraNera.setVisible(viewRegione.getRegione().hasPecoraNera());
		labelNumPecoreNere.setVisible(false);
	}
		
	/***
	 * 
	 * @return coordinata x della label
	 */
	public int getX() {
		return labelX;
	}
		
	/***
	 * 
	 * @return coordinata y della label
	 */
	public int getY() {
		return labelY;
	}
	
}
