package it.polimi.lamalfa_maioli.view;

import java.io.Serializable;
import javax.swing.JPanel;

/***
 * classe invocata per vedere l'animazione del pastore fra due date caselle
 * @author lamalfa_maioli
 *
 */
public class AnimazionePastore implements Runnable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5783544705371879481L;
	private JPanel contentPane;
	private ViewCasella labelDest;
	private ViewPastore viewpastore; 
	/***
	 * inizializza panel per movimento del pastore da casella a casella
	 * @param contentpane pannello che visualizza spostamento
	 * @param labelDest casella di destinazione
	 * @param viewpastore puntatore alla view del pastore
	 */
	public void inizializza(ControllerMappa controller, JPanel contentpane, ViewCasella labelDest, ViewPastore viewpastore) throws NullPointerException {
		this.viewpastore = viewpastore;
		this.contentPane = contentpane;
		controller.getViewCasella(viewpastore.getPastore().getCasella());
		this.labelDest = labelDest;
	}
	

/***
 * fa partire aniamzione pastore su therad separato
 */
	@Override
	public void run() throws NullPointerException {
		
		int destx = labelDest.getX();
		int desty = labelDest.getY()-5;
		labelDest.getY();
		
		while(viewpastore.getX() != destx || viewpastore.getY() != desty) {
			if(viewpastore.getX() < destx) {
				viewpastore.setX(+1);
			} else if(viewpastore.getX() > destx){
				viewpastore.setX(-1);
			} 
			if(viewpastore.getY() < desty) {
				viewpastore.setY(+1);
			} else if(viewpastore.getY() > desty) {
				viewpastore.setY(-1);
			}
			contentPane.validate();
	        contentPane.repaint(); 
		}
	}
	
}
