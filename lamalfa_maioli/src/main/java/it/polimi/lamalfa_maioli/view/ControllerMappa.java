package it.polimi.lamalfa_maioli.view;

import it.polimi.lamalfa_maioli.controller.GameCore;
import it.polimi.lamalfa_maioli.controller.GameCore.Animale;
import it.polimi.lamalfa_maioli.controller.exceptions.DadoException;
import it.polimi.lamalfa_maioli.controller.exceptions.MyRuntimeException;
import it.polimi.lamalfa_maioli.controller.rete.rmi.ServerInterface;
import it.polimi.lamalfa_maioli.controller.rete.socket.ReteSocket;
import it.polimi.lamalfa_maioli.model.Casella;
import it.polimi.lamalfa_maioli.model.Giocatore;
import it.polimi.lamalfa_maioli.model.Pastore;
import it.polimi.lamalfa_maioli.model.Regione;
import it.polimi.lamalfa_maioli.model.Terreno;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.Serializable;

/***
 * classe che istanzia la mappa di gioco e i suoi elementi
 * ed effettua azioni dei vari giocatori
 * @author lamalfa_maioli
 *
 */
public class ControllerMappa extends JFrame implements Serializable {
	
	private static final long serialVersionUID = -5657085241950714975L;
	private JPanel contentPane;
	private ControllerAnimali controllerAnimali = new ControllerAnimali();
	private ViewCasella [] casella = new ViewCasella[42];
	private ViewRegione [] regione = new ViewRegione[19];
	private ViewPecoraBianca [] pecoraBianca = new ViewPecoraBianca[19];
	private ViewPecoraNera [] pecoraNera = new ViewPecoraNera[19];
	private ViewAriete [] ariete = new ViewAriete[19];
	private ViewAgnello [] agnello = new ViewAgnello[19];
	private ViewLupo [] lupo = new ViewLupo[19];
	private AnimazioneAnimale animazioneAnimale;
	private Casella casellaPremuta = null;
	private Regione regionePremuta = null;
	private Pastore pastorePremuto = null;
	private List<JLabel> primary = new ArrayList<>();
	private List<JLabel> contatoreTerreni = new ArrayList<>();
	private Map<Integer, ViewPastore> viewPastori = new HashMap<>();
	private List<Pastore> pastoreAssociato = new ArrayList<>();
	
	public enum Azione {
		NO, MUOVI_PASTORE, MUOVI_PECORA, MUOVI_ARIETE, MUOVI_PECORA_NERA, ACCOPPIA, COMPRA_TERRENO, FINE_TURNO, SPARATORIA, COMPRAVENDITA_COMPRA, COMPRAVENDITA_VENDI, COMPRAVENDITA;
	}
	private Azione azione = Azione.NO;
	private JLabel lblNumeroMonete;
	private JLabel lblNumeroRecinti;
	private boolean partitaIniziata = false;
	private JLabel lblMosse;
	private JLabel labelTurno;
	private JLabel labelAzione;
	private JLabel lblTurnoNumero;
	private JButton btnMuoviAnimale;
	private JButton btnMuoviPastore;
	private JButton btnAccoppia;
	private JButton btnSparatoria;
	private JButton btnCompraTerreno;
	private JButton btnCompravendita;
	private JButton btnTerminaTurno;
	private ReteSocket reteSocket;
	
	/***
	 * aggiorna il numero del turno
	 * @param turno turno corrente
	 */
	public final void updateNumeroTurno(String turno) {
		lblTurnoNumero.setText("Turno numero: "+turno);
	}
	
	/***
	 * update del turno
	 * @param turno il turno corrente
	 */
	public final void updateTurno(String turno) {
		labelTurno.setText("Turno del giocatore: "+turno);
	}
	
	/***
	 * update sul socket del turno
	 * @param turno turno corrente
	 */
	public final void socketUpdateTurno(String turno) {
		labelTurno.setText(turno);
	}
	
	/***
	 * aggiorna l'azione di gioco
	 * @param azione l'azione corrente
	 */
	public final void updateAzione(String azione) {
		labelAzione.setText(azione);
	}
	
	/***
	 * funziona che ritrona se la partita e' iniziata
	 * @return boolean sul turno iniziato
	 */
	public final boolean partitaIniziata() {
		return partitaIniziata;
	}
	
	/***
	 * ritorna l'azione eseguita
	 * @return azione eseguita dal giocatore
	 */
	public final Azione getAzione() {
		return azione;
	}
	
	/***
	 * imposta azione da eseguire
	 * @param azione l'azione da eseguire
	 */
	public final void setAzione(Azione azione) {
		this.azione = azione;
	}
	
	/***
	 * ritorna la viewcasella della casella della quale si passa l'id
	 * @param casella la casella
	 * @return la viewcasella associata
	 */
	public final ViewCasella getViewCasella(Casella casella) {
		int max = this.casella.length;
		int id = casella.getId();
		if(id > max-1) {
			return null;
		}
		return this.casella[id];
	}
	
	/***
	 * ritorna la viewregione della regione della quale si passa l'id
	 * @param regione la regione
	 * @return la viewregione associata
	 */
	public final ViewRegione getViewRegione(Regione regione) {
		int max = this.regione.length;
		int id = regione.getId();
		if(id > max-1) {
			return null;
		}
		return this.regione[id];
	}
	
	/***
	 * imposta ViewCaselle e associa ad ogni Casella la rispettiva ViewCasella
	 * @param status lo status di gioco
	 * @param graphXML il grafo XML della mappa
	 */
	public final void inizializzaCaselleMappa(GameCore core) {
		casella[0] = new ViewCasella(contentPane, 122, 180, 24, 19);
		contentPane.add(casella[0].getLabel());		
		casella[0].setController(this);
		
		casella[1] = new ViewCasella(contentPane, 150, 229, 24, 19);
		contentPane.add(casella[1].getLabel());			

		casella[2] = new ViewCasella(contentPane, 83, 259, 24, 19);
		contentPane.add(casella[2].getLabel());		

		casella[3] = new ViewCasella(contentPane, 150, 288, 24, 19);
		contentPane.add(casella[3].getLabel());

		casella[4] = new ViewCasella(contentPane, 184, 328, 24, 19);
		contentPane.add(casella[4].getLabel());

		casella[5] = new ViewCasella(contentPane, 149, 354, 24, 19);
		contentPane.add(casella[5].getLabel());

		casella[6] = new ViewCasella(contentPane, 112, 385, 24, 19);
		contentPane.add(casella[6].getLabel());

		casella[7] = new ViewCasella(contentPane, 152, 420, 24, 19);
		contentPane.add(casella[7].getLabel());

		casella[8] = new ViewCasella(contentPane, 187, 465, 24, 19);
		contentPane.add(casella[8].getLabel());

		casella[9] = new ViewCasella(contentPane, 229, 418, 24, 19);
		contentPane.add(casella[9].getLabel());

		casella[10] = new ViewCasella(contentPane, 224, 348, 24, 19);
		contentPane.add(casella[10].getLabel());

		casella[11] = new ViewCasella(contentPane, 121, 525, 24, 19);
		contentPane.add(casella[11].getLabel());

		casella[12] = new ViewCasella(contentPane, 186, 584, 24, 19);
		contentPane.add(casella[12].getLabel());

		casella[13] = new ViewCasella(contentPane, 224, 487, 24, 19);
		contentPane.add(casella[13].getLabel());

		casella[14] = new ViewCasella(contentPane, 261, 510, 24, 19);
		contentPane.add(casella[14].getLabel());

		casella[15] = new ViewCasella(contentPane, 300, 480, 24, 19);
		contentPane.add(casella[15].getLabel());

		casella[16] = new ViewCasella(contentPane, 298, 405, 24, 19);
		contentPane.add(casella[16].getLabel());

		casella[17] = new ViewCasella(contentPane, 264, 372, 24, 19);
		contentPane.add(casella[17].getLabel());

		casella[18] = new ViewCasella(contentPane, 297, 560, 24, 19);
		contentPane.add(casella[18].getLabel());

		casella[19] = new ViewCasella(contentPane, 376, 501, 24, 19);
		contentPane.add(casella[19].getLabel());

		casella[20] = new ViewCasella(contentPane, 327, 454, 24, 19);
		contentPane.add(casella[20].getLabel());

		casella[21] = new ViewCasella(contentPane, 369, 410, 24, 19);
		contentPane.add(casella[21].getLabel());

		casella[22] = new ViewCasella(contentPane, 386, 346, 24, 19);
		contentPane.add(casella[22].getLabel());

		casella[23] = new ViewCasella(contentPane, 332, 327, 24, 19);
		contentPane.add(casella[23].getLabel());

		casella[24] = new ViewCasella(contentPane, 302, 348, 24, 19);
		contentPane.add(casella[24].getLabel());

		casella[25] = new ViewCasella(contentPane, 430, 387, 24, 19);
		contentPane.add(casella[25].getLabel());

		casella[26] = new ViewCasella(contentPane, 443, 259, 24, 19);
		contentPane.add(casella[26].getLabel());

		casella[27] = new ViewCasella(contentPane, 387, 294, 24, 19);
		contentPane.add(casella[27].getLabel());

		casella[28] = new ViewCasella(contentPane, 394, 241, 24, 19);
		contentPane.add(casella[28].getLabel());

		casella[29] = new ViewCasella(contentPane, 354, 226, 24, 19);
		contentPane.add(casella[29].getLabel());

		casella[30] = new ViewCasella(contentPane, 308, 248, 24, 19);
		contentPane.add(casella[30].getLabel());

		casella[31] = new ViewCasella(contentPane, 296, 298, 24, 19);
		contentPane.add(casella[31].getLabel());

		casella[32] = new ViewCasella(contentPane, 412, 175, 24, 19);
		contentPane.add(casella[32].getLabel());

		casella[33] = new ViewCasella(contentPane, 346, 128, 24, 19);
		contentPane.add(casella[33].getLabel());

		casella[34] = new ViewCasella(contentPane, 326, 202, 24, 19);
		contentPane.add(casella[34].getLabel());

		casella[35] = new ViewCasella(contentPane, 292, 175, 24, 19);
		contentPane.add(casella[35].getLabel());

		casella[36] = new ViewCasella(contentPane, 247, 187, 24, 19);
		contentPane.add(casella[36].getLabel());

		casella[37] = new ViewCasella(contentPane, 236, 238, 24, 19);
		contentPane.add(casella[37].getLabel());

		casella[38] = new ViewCasella(contentPane, 258, 273, 24, 19);
		contentPane.add(casella[38].getLabel());

		casella[39] = new ViewCasella(contentPane, 253, 120, 24, 19);
		contentPane.add(casella[39].getLabel());

		casella[40] = new ViewCasella(contentPane, 199, 208, 24, 19);
		contentPane.add(casella[40].getLabel());

		casella[41] = new ViewCasella(contentPane, 217, 299, 24, 19);
		contentPane.add(casella[41].getLabel());
		
		List<Casella> caselle = core.getStatus().getMappa().allCasella();
		for(int i=0; i<42 ; i++) {
			casella[i].setCasella(caselle.get(i));
		}
	}
	
	/***
	 * imposta ViewRegione e associa ad ogni Regione la sua ViewRegione
	 */
	public final void inizializzaRegioniMappa(GameCore core) {
		
		JLabel lblRegione01 = new JLabel("");
		lblRegione01.setBounds(90, 202, 64, 50);
		contentPane.add(lblRegione01);

		
		JLabel lblRegione02 = new JLabel("");
		lblRegione02.setBounds(60, 123, 64, 89);
		contentPane.add(lblRegione02);
		
		JLabel lblRegione03 = new JLabel("");
		lblRegione03.setBounds(41, 149, 30, 63);
		contentPane.add(lblRegione03);
		
		JLabel lblRegione04 = new JLabel("");
		lblRegione04.setBounds(145, 208, 17, 14);
		contentPane.add(lblRegione04);
		
		JLabel lblRegione11 = new JLabel("");
		lblRegione11.setBounds(113, 263, 41, 111);
		contentPane.add(lblRegione11);
		
		JLabel lblRegione12 = new JLabel("");
		lblRegione12.setBounds(149, 313, 24, 30);
		contentPane.add(lblRegione12);
		
		JLabel lblRegione13 = new JLabel("");
		lblRegione13.setBounds(76, 284, 30, 14);
		contentPane.add(lblRegione13);

		
		JLabel lblRegione21 = new JLabel("");
		lblRegione21.setBounds(183, 354, 46, 101);
		contentPane.add(lblRegione21);
		
		JLabel lblRegione22 = new JLabel("");
		lblRegione22.setBounds(168, 377, 73, 35);
		contentPane.add(lblRegione22);

		JLabel lblRegione23 = new JLabel(" ");
		lblRegione23.setBounds(174, 441, 55, 22);
		contentPane.add(lblRegione23);

		JLabel lblRegione31 = new JLabel(" ");
		lblRegione31.setBounds(99, 412, 55, 89);
		contentPane.add(lblRegione31);
		
		JLabel lblRegione32 = new JLabel(" ");
		lblRegione32.setBounds(65, 420, 41, 40);
		contentPane.add(lblRegione32);
		
		JLabel lblRegione33 = new JLabel(" ");
		lblRegione33.setBounds(78, 510, 46, 22);
		contentPane.add(lblRegione33);
		
		JLabel lblRegione41 = new JLabel(" ");
		lblRegione41.setBounds(155, 510, 86, 42);
		contentPane.add(lblRegione41);
		
		JLabel lblRegione42 = new JLabel(" ");
		lblRegione42.setBounds(168, 487, 61, 30);
		contentPane.add(lblRegione42);
		
		JLabel lblRegione43 = new JLabel(" ");
		lblRegione43.setBounds(113, 548, 114, 14);
		contentPane.add(lblRegione43);
		
		JLabel lblRegione44 = new JLabel(" ");
		lblRegione44.setBounds(123, 548, 66, 61);
		contentPane.add(lblRegione44);
		
		JLabel lblRegione51 = new JLabel(" ");
		lblRegione51.setBounds(258, 396, 46, 80);
		contentPane.add(lblRegione51);
		
		JLabel lblRegione52 = new JLabel(" ");
		lblRegione52.setBounds(258, 466, 46, 35);
		contentPane.add(lblRegione52);
		
		JLabel lblRegione53 = new JLabel(" ");
		lblRegione53.setBounds(245, 441, 17, 35);
		contentPane.add(lblRegione53);
		
		JLabel lblRegione61 = new JLabel(" ");
		lblRegione61.setBounds(168, 628, 55, 40);
		contentPane.add(lblRegione61);
		
		JLabel lblRegione62 = new JLabel(" ");
		lblRegione62.setBounds(199, 606, 63, 50);
		contentPane.add(lblRegione62);
		
		JLabel lblRegione63 = new JLabel(" ");
		lblRegione63.setBounds(218, 573, 86, 36);
		contentPane.add(lblRegione63);
		
		JLabel lblRegione64 = new JLabel(" ");
		lblRegione64.setBounds(256, 532, 41, 40);
		contentPane.add(lblRegione64);
		
		JLabel lblRegione71 = new JLabel(" ");
		lblRegione71.setBounds(325, 573, 45, 36);
		contentPane.add(lblRegione71);
		
		JLabel lblRegione72 = new JLabel(" ");
		lblRegione72.setBounds(332, 526, 73, 46);
		contentPane.add(lblRegione72);
		
		JLabel lblRegione73 = new JLabel(" ");
		lblRegione73.setBounds(315, 510, 55, 42);
		contentPane.add(lblRegione73);
		
		JLabel lblRegione74 = new JLabel(" ");
		lblRegione74.setBounds(329, 476, 41, 41);
		contentPane.add(lblRegione74);
		
		JLabel lblRegione81 = new JLabel(" ");
		lblRegione81.setBounds(330, 347, 41, 101);
		contentPane.add(lblRegione81);
		
		JLabel lblRegione82 = new JLabel(" ");
		lblRegione82.setBounds(315, 370, 97, 14);
		contentPane.add(lblRegione82);
		
		JLabel lblRegione83 = new JLabel(" ");
		lblRegione83.setBounds(365, 343, 24, 63);
		contentPane.add(lblRegione83);
		
		
		JLabel lblRegione91 = new JLabel(" ");
		lblRegione91.setBounds(381, 445, 80, 40);
		contentPane.add(lblRegione91);
		
		JLabel lblRegione92 = new JLabel(" ");
		lblRegione92.setBounds(401, 396, 30, 59);
		contentPane.add(lblRegione92);
		
		JLabel lblRegione93 = new JLabel(" ");
		lblRegione93.setBounds(418, 412, 41, 43);
		contentPane.add(lblRegione93);
		
		JLabel lblRegione101 = new JLabel(" ");
		lblRegione101.setBounds(418, 280, 46, 94);
		contentPane.add(lblRegione101);
		
		JLabel lblRegione102 = new JLabel(" ");
		lblRegione102.setBounds(398, 320, 46, 14);
		contentPane.add(lblRegione102);
		
		JLabel lblRegione111 = new JLabel("");
		lblRegione111.setBounds(343, 250, 46, 69);
		contentPane.add(lblRegione111);
		
		JLabel lblRegione112 = new JLabel(" ");
		lblRegione112.setBounds(325, 273, 31, 46);
		contentPane.add(lblRegione112);
		
		JLabel lblRegione113 = new JLabel(" ");
		lblRegione113.setBounds(365, 261, 41, 22);
		contentPane.add(lblRegione113);
		
		JLabel lblRegione121 = new JLabel(" ");
		lblRegione121.setBounds(418, 202, 46, 35);
		contentPane.add(lblRegione121);
		
		JLabel lblRegione122 = new JLabel(" ");
		lblRegione122.setBounds(428, 238, 30, 14);
		contentPane.add(lblRegione122);
		
		JLabel lblRegione123 = new JLabel(" ");
		lblRegione123.setBounds(445, 180, 17, 32);
		contentPane.add(lblRegione123);
		
		JLabel lblRegione131 = new JLabel(" ");
		lblRegione131.setBounds(398, 107, 55, 61);
		contentPane.add(lblRegione131);
		
		JLabel lblRegione132 = new JLabel(" ");
		lblRegione132.setBounds(359, 149, 46, 69);
		contentPane.add(lblRegione132);
		
		JLabel lblRegione133 = new JLabel(" ");
		lblRegione133.setBounds(385, 115, 24, 35);
		contentPane.add(lblRegione133);
		
		JLabel lblRegione141 = new JLabel(" ");
		lblRegione141.setBounds(271, 211, 41, 50);
		contentPane.add(lblRegione141);
		
		JLabel lblRegione142 = new JLabel("  ");
		lblRegione142.setBounds(258, 208, 64, 14);
		contentPane.add(lblRegione142);
		
		JLabel lblRegione143 = new JLabel(" ");
		lblRegione143.setBounds(280, 193, 41, 14);
		contentPane.add(lblRegione143);
		
		JLabel lblRegione151 = new JLabel(" ");
		lblRegione151.setBounds(301, 52, 46, 116);
		contentPane.add(lblRegione151);
		
		JLabel lblRegione152 = new JLabel(" ");
		lblRegione152.setBounds(332, 64, 68, 40);
		contentPane.add(lblRegione152);
		
		JLabel lblRegione153 = new JLabel(" ");
		lblRegione153.setBounds(282, 107, 30, 61);
		contentPane.add(lblRegione153);
		
		JLabel lblRegione161 = new JLabel(" ");
		lblRegione161.setBounds(134, 138, 138, 35);
		contentPane.add(lblRegione161);
		
		JLabel lblRegione162 = new JLabel(" ");
		lblRegione162.setBounds(156, 168, 97, 34);
		contentPane.add(lblRegione162);
		
		JLabel lblRegione163 = new JLabel(" ");
		lblRegione163.setBounds(145, 107, 24, 30);
		contentPane.add(lblRegione163);
		
		JLabel lblRegione164 = new JLabel(" ");
		lblRegione164.setBounds(233, 107, 24, 30);
		contentPane.add(lblRegione164);
		
		JLabel lblRegione171 = new JLabel(" ");
		lblRegione171.setBounds(183, 269, 41, 50);
		contentPane.add(lblRegione171);
		
		JLabel lblRegione172 = new JLabel(" ");
		lblRegione172.setBounds(174, 255, 67, 22);
		contentPane.add(lblRegione172);
		
		JLabel lblRegione173 = new JLabel(" ");
		lblRegione173.setBounds(183, 230, 58, 22);
		contentPane.add(lblRegione173);
		
		JLabel lblSheep1 = new JLabel(" ");
		lblSheep1.setBounds(253, 293, 46, 69);
		contentPane.add(lblSheep1);
		
		JLabel lblSheep2 = new JLabel(" ");
		lblSheep2.setBounds(226, 321, 46, 22);
		contentPane.add(lblSheep2);
				
		regione[0] = new ViewRegione(contentPane, lblRegione01, lblRegione02, lblRegione03, lblRegione04);
		regione[0].setController(this);
		
		regione[1] = new ViewRegione(contentPane, lblRegione11, lblRegione12, lblRegione13);
		
		regione[2] = new ViewRegione(contentPane, lblRegione21, lblRegione22, lblRegione23);
		
		regione[3] = new ViewRegione(contentPane, lblRegione31, lblRegione32, lblRegione33);
		
		regione[4] = new ViewRegione(contentPane, lblRegione41, lblRegione42, lblRegione43, lblRegione44);
		
		regione[5] = new ViewRegione(contentPane, lblRegione51, lblRegione52, lblRegione53);
		
		regione[6] = new ViewRegione(contentPane, lblRegione61, lblRegione62, lblRegione63, lblRegione64);
		
		regione[7] = new ViewRegione(contentPane, lblRegione71, lblRegione72, lblRegione73, lblRegione74);
		
		regione[8] = new ViewRegione(contentPane, lblRegione81, lblRegione82, lblRegione83);
		
		regione[9] = new ViewRegione(contentPane, lblRegione91, lblRegione92, lblRegione93);
		
		regione[10] = new ViewRegione(contentPane, lblRegione101, lblRegione102);
		
		regione[11] = new ViewRegione(contentPane, lblRegione111, lblRegione112, lblRegione113);
		
		regione[12] = new ViewRegione(contentPane, lblRegione121, lblRegione122, lblRegione123);
		
		regione[13] = new ViewRegione(contentPane, lblRegione131, lblRegione132, lblRegione133);
		
		regione[14] = new ViewRegione(contentPane, lblRegione141, lblRegione142, lblRegione143);
		
		regione[15] = new ViewRegione(contentPane, lblRegione151, lblRegione152, lblRegione153);
		
		regione[16] = new ViewRegione(contentPane, lblRegione161, lblRegione162, lblRegione163, lblRegione164);
		
		regione[17] = new ViewRegione(contentPane, lblRegione171, lblRegione172, lblRegione173);
		
		regione[18] = new ViewRegione(contentPane, lblSheep1, lblSheep2);

		List<Regione> regioni = core.getStatus().getMappa().allRegione(); 
		for(int i = 0; i < regioni.size(); i++) {
			regione[i].setRegione(regioni.get(i));
		}
		
		
		this.controllerAnimali.impostaPecoreBianche(contentPane, pecoraBianca, regione);
		this.controllerAnimali.impostaPecoreNere(contentPane, pecoraNera, regione);
		this.controllerAnimali.impostaArieti(contentPane, ariete, regione);
		this.controllerAnimali.impostaAgnelli(contentPane, agnello, regione);
		this.controllerAnimali.impostaLupo(contentPane, lupo, regione);
		
		this.animazioneAnimale = new AnimazioneAnimale(contentPane);
		
	}

	public final void aggiornaView(GameCore core, ServerInterface server) {
		try {
			List<Regione> regioni = core.getStatus().getMappa().allRegione();
			for(int i = 0; i < regioni.size(); i++) {
				this.regione[i].setRegione(regioneEffettiva(regioni.get(i), core, server));
			}
			List<Casella> caselle = core.getStatus().getMappa().allCasella();
			for(int i = 0; i < caselle.size(); i++) {
				this.casella[i].setCasella(casellaEffettiva(caselle.get(i), core, server));
			}
		} catch(RemoteException e) {
			throw new MyRuntimeException(e);
		}
	}
	
	
	/***
	 * inizializza la mappa e imposta i listener sui bottoni di gioco
	 */
	public ControllerMappa(GameCore core) {
		setResizable(false);
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			// Ignora
		} catch (InstantiationException e) {
			// Ignora
		} catch (IllegalAccessException e) {
			// Ignora
		} catch (UnsupportedLookAndFeelException e) {
			// Ignora
		}
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 968, 809);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(40,162,247));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
				
		for(int i = 0; i < 100; i++) {
			primary.add(new JLabel(""));
			contentPane.add(primary.get(i));
		}
		
		inizializzaCaselleMappa(core);
		inizializzaRegioniMappa(core);		
		
		btnMuoviPastore = new JButton("Muovi Pastore");
		btnMuoviPastore.setBounds(669, 420, 139, 35);
		btnMuoviPastore.addMouseListener ( new MyMouseEvent (this) {
           private static final long serialVersionUID = 1L;

			public void mousePressed ( MouseEvent e ) {	
            	azione = Azione.MUOVI_PASTORE;
            }
        } );
		contentPane.add(btnMuoviPastore);

		impostaElementiFinestraPrincipale();
		
	}
	
	public final void impostaElementiFinestraPrincipale() {

		btnMuoviAnimale = new JButton("Muovi animale");
		btnMuoviAnimale.setBounds(520, 420, 139, 35);
		btnMuoviAnimale.addMouseListener ( new MouseAdapter () {
            public void mousePressed ( MouseEvent e ) {	
            	muoviAnimale();
            }
        } );
		contentPane.add(btnMuoviAnimale);
		
		btnCompraTerreno = new JButton("Compra Terreno");
		btnCompraTerreno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				azione = Azione.COMPRA_TERRENO;
			}
		});
		btnCompraTerreno.setBounds(520, 514, 139, 35);
		contentPane.add(btnCompraTerreno);
		
		btnCompravendita = new JButton("Compravendita");
		btnCompravendita.setBounds(669, 514, 139, 35);
		btnCompravendita.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				azione = Azione.COMPRAVENDITA;
			}
		});
		contentPane.add(btnCompravendita);
		
		JLabel label5 = new JLabel("0");
		label5.setHorizontalAlignment(SwingConstants.RIGHT);
		label5.setFont(new Font("Tahoma", Font.BOLD, 15));
		label5.setForeground(Color.RED);
		label5.setBounds(644, 77, 46, 14);
		contentPane.add(label5);
		
		JLabel label0 = new JLabel("0");
		label0.setHorizontalAlignment(SwingConstants.RIGHT);
		label0.setForeground(Color.RED);
		label0.setFont(new Font("Tahoma", Font.BOLD, 15));
		label0.setBounds(554, 77, 46, 14);
		contentPane.add(label0);
		
	
		btnTerminaTurno = new JButton("Termina Turno");
		btnTerminaTurno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				azione = Azione.FINE_TURNO;
			}
		});
		btnTerminaTurno.setBounds(596, 585, 139, 35);
		contentPane.add(btnTerminaTurno);
		
		JLabel label1 = new JLabel("0");
		label1.setHorizontalAlignment(SwingConstants.RIGHT);
		label1.setForeground(Color.RED);
		label1.setFont(new Font("Tahoma", Font.BOLD, 15));
		label1.setBounds(644, 168, 46, 14);
		contentPane.add(label1);
		
		JLabel label2 = new JLabel("0");
		label2.setHorizontalAlignment(SwingConstants.RIGHT);
		label2.setForeground(Color.RED);
		label2.setFont(new Font("Tahoma", Font.BOLD, 15));
		label2.setBounds(554, 168, 46, 14);
		contentPane.add(label2);
		
		JLabel label3 = new JLabel("0");
		label3.setHorizontalAlignment(SwingConstants.RIGHT);
		label3.setForeground(Color.RED);
		label3.setFont(new Font("Tahoma", Font.BOLD, 15));
		label3.setBounds(554, 259, 46, 14);
		contentPane.add(label3);
		
		JLabel label4 = new JLabel("0");
		label4.setHorizontalAlignment(SwingConstants.RIGHT);
		label4.setForeground(Color.RED);
		label4.setFont(new Font("Tahoma", Font.BOLD, 15));
		label4.setBounds(644, 259, 46, 14);
		contentPane.add(label4);
		
		contatoreTerreni.add(label0);
		contatoreTerreni.add(label5);
		contatoreTerreni.add(label2);
		contatoreTerreni.add(label3);
		contatoreTerreni.add(label4);
		contatoreTerreni.add(label1);
		
		JLabel numeroTerreni1 = new JLabel("Terreno1");
		numeroTerreni1.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/tesseraTerreno1.png")));
		numeroTerreni1.setBounds(520, 11, 80, 80);
		contentPane.add(numeroTerreni1);
		
		JLabel numeroTerreni2 = new JLabel("Terreno2");
		numeroTerreni2.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/tesseraTerreno2.png")));
		numeroTerreni2.setBounds(610, 11, 80, 80);
		contentPane.add(numeroTerreni2);
		
		JLabel numeroTerreni3 = new JLabel("Terreno3");
		numeroTerreni3.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/tesseraTerreno3.png")));
		numeroTerreni3.setBounds(520, 102, 80, 80);
		contentPane.add(numeroTerreni3);
		
		JLabel numeroTerreni4 = new JLabel("Terreno4");
		numeroTerreni4.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/tesseraTerreno4.png")));
		numeroTerreni4.setBounds(610, 102, 80, 80);
		contentPane.add(numeroTerreni4);
		
		JLabel numeroTerreni5 = new JLabel("Terreno5");
		numeroTerreni5.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/tesseraTerreno5.png")));
		numeroTerreni5.setBounds(520, 193, 80, 80);
		contentPane.add(numeroTerreni5);
		
		JLabel numeroTerreni6 = new JLabel("Terreno6");
		numeroTerreni6.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/tesseraTerreno6.png")));
		numeroTerreni6.setBounds(610, 193, 80, 80);
		contentPane.add(numeroTerreni6);
		
		lblNumeroMonete = new JLabel("0");
		lblNumeroMonete.setForeground(new Color(0, 0, 0));
		lblNumeroMonete.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNumeroMonete.setBounds(744, 101, 29, 14);
		contentPane.add(lblNumeroMonete);
		
		JLabel lblMonete = new JLabel("");
		lblMonete.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/moneta.png")));
		lblMonete.setBounds(734, 40, 55, 61);
		contentPane.add(lblMonete);
		
		lblNumeroRecinti = new JLabel("20");
		lblNumeroRecinti.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNumeroRecinti.setForeground(new Color(0, 0, 0));
		lblNumeroRecinti.setBounds(748, 208, 24, 14);
		contentPane.add(lblNumeroRecinti);
		
		JLabel lblLabelRecinti = new JLabel("");
		lblLabelRecinti.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/recinto.png")));
		lblLabelRecinti.setBounds(732, 146, 75, 61);
		contentPane.add(lblLabelRecinti);
		
		btnAccoppia = new JButton("Accoppia");
		btnAccoppia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				azione = Azione.ACCOPPIA;
			}
		});
		btnAccoppia.setBounds(520, 466, 139, 35);
		contentPane.add(btnAccoppia);
		
		btnSparatoria = new JButton("Sparatoria");
		btnSparatoria.setBounds(669, 466, 139, 35);
		btnSparatoria.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				azione = Azione.SPARATORIA;
			}
		});
		contentPane.add(btnSparatoria);
		
		JLabel lblNewLabel = new JLabel("Mappa");
		lblNewLabel.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/mappa.jpg")));
		lblNewLabel.setBounds(10, 11, 500, 700);
		contentPane.add(lblNewLabel);
		
		lblMosse = new JLabel("Mosse rimanenti: 3");
		lblMosse.setForeground(Color.BLACK);
		lblMosse.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblMosse.setBounds(520, 329, 170, 14);
		contentPane.add(lblMosse);
		
		labelTurno = new JLabel("Turno del giocatore:");
		labelTurno.setForeground(Color.BLACK);
		labelTurno.setFont(new Font("Tahoma", Font.BOLD, 14));
		labelTurno.setBounds(520, 305, 288, 14);
		contentPane.add(labelTurno);
		
		labelAzione = new JLabel("Azione:");
		labelAzione.setForeground(Color.BLACK);
		labelAzione.setFont(new Font("Tahoma", Font.BOLD, 14));
		labelAzione.setBounds(520, 348, 432, 14);
		contentPane.add(labelAzione);
		
		lblTurnoNumero = new JLabel("Turno numero: 1");
		lblTurnoNumero.setForeground(Color.BLACK);
		lblTurnoNumero.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblTurnoNumero.setBounds(520, 284, 288, 14);
		contentPane.add(lblTurnoNumero);
	}
	
	/***
	 * richiede di impostare i pastori di gioco sulla mappa
	 * @param gioca il giocatore che deve impostare i pastori
	 * @param core il gamecore di gioco
	 * @param server il server di gioco
	 */
	public final void requestPastore(Giocatore gioca, GameCore core, ServerInterface server) {
		try {
			Giocatore giocatore = giocatoreEffettivo(gioca, core, server);
			for(int i = 0; i < giocatore.getPastori().size(); i++) {
				updateTurno(giocatore.getColore().toString());
				updateAzione("Imposta posizione pastore #"+(i+1));
				JOptionPane.showMessageDialog(this, "Giocatore "+giocatore.getColore().toString()+": imposta posizione pastore #"+(i+1));
				casellaPremuta = null;
				while(casellaPremuta == null) {
					Thread.sleep(50);
				}
				if(server != null) {
					server.posizionaPastore(giocatore, i, casellaPremuta);
				} else {
					core.posizionaPastore(giocatore, i, casellaPremuta);
				}
				Pastore pastore = giocatore.getPastori().get(i);
				JLabel vis = primary.get(0);
				ViewPastore tmp = new ViewPastore(vis, contentPane, getViewCasella(casellaPremuta), 30, 30, pastore, this);
				primary.remove(0);
				pastoreAssociato.add(pastore);
				viewPastori.put(pastoreAssociato.indexOf(pastore), tmp);
				casellaPremuta = null;
			}
			partitaIniziata = true;
			updateAzione("");
		} catch (RemoteException e1) {
			throw new MyRuntimeException(e1);
		} catch (InterruptedException e) {
			// Ignora
		}
	}
	
	/***
	 * crea la viewpastore del pastoree associato al giocatore corrente
	 * @param gioca il giocatore corrente
	 * @param core il gamecore di gioco
	 */
	public final void creaViewPastore(Giocatore gioca, GameCore core) {
		try {
			Giocatore giocatore = giocatoreEffettivo(gioca, core, null);
			List<Pastore> pastori = giocatore.getPastori();
			for(int i = 0; i < pastori.size(); i++) {
				Pastore tmp = pastori.get(i);
				if(tmp.getCasella() != null) {
					JLabel vis = primary.get(0);
					ViewPastore viewAssociata = new ViewPastore(vis, contentPane, getViewCasella(tmp.getCasella()), 30, 30, tmp, this);
					primary.remove(0);
					pastoreAssociato.add(tmp);
					viewPastori.put(pastoreAssociato.indexOf(tmp), viewAssociata);
				}
			}
		} catch (RemoteException e) {
			throw new MyRuntimeException(e);
		}
	}
	
	/***
	 * imposta la casella premuta
	 * @param casella la casella premuta
	 */
	public final void setCasellaPremuta(Casella casella) {
		casellaPremuta = casella;
	}
	
	/***
	 * imposta il pastore premuto dal giocatore
	 * @param pastore il pastore premuto
	 */
	public final void setPastorePremuto(Pastore pastore) {
		pastorePremuto = pastore;
	}
	
	/***
	 * imposta la regione premuta
	 * @param regione la regione da premere
	 */
	public final void setRegionePremuta(Regione regione) {
		regionePremuta = regione;
	}
	
	/***
	 * aggiorna i contatori di gioco
	 * @param gioca il giocatore corrente
	 * @param core il gamecore di gioco
	 */
	public final void updateContatori(Giocatore gioca, GameCore core) {
		Giocatore giocatore = core.giocatoreEffettivo(gioca);
		Map<Terreno, Integer> ter = giocatore.getTerreniComprati();
		for(int i = 0; i < Terreno.values().length-1; i++) {
			String numero = ter.get(Terreno.values()[i]).toString();
			contatoreTerreni.get(i).setText(numero);
		}
		lblNumeroMonete.setText(giocatore.getMonete()+"");
		int rimanenti = core.getStatus().getNumeroRecinti();
		if(rimanenti < 0) {
			rimanenti = 0;
		}
		lblNumeroRecinti.setText(rimanenti+"");
		Pastore tmp = giocatore.getPastoreCorrente();
		int mossa = 3 - ((tmp == null) ? 0 : tmp.getNumeroMossa());
		lblMosse.setText("Mosse rimanenti: "+mossa);
	}
	
	/***
	 * imposta il pastore effettivo che sta giocando
	 * @param pastore pastore corrente
	 * @param core core di gioco
	 * @param server il server di gioco
	 * @return il pastore che effettivamente agisce
	 * @throws RemoteException
	 */
	public final Pastore pastoreEffettivo(Pastore pastore, GameCore core, ServerInterface server) throws RemoteException {
		if(server != null) {
			return server.core().pastoreEffettivo(pastore);
		} else {
			return core.pastoreEffettivo(pastore);
		}
	}
	
	/***
	 * imposta il giocatore effettivo che sta giocando
	 * @param pastore pastore corrente
	 * @param core core di gioco
	 * @param server il server di gioco
	 * @return il giocatore che effettivamente agisce
	 * @throws RemoteException
	 */
	public final Giocatore giocatoreEffettivo(Giocatore giocatore, GameCore core, ServerInterface server) throws RemoteException {
		if(server != null) {
			return server.core().giocatoreEffettivo(giocatore);
		} else {
			return core.giocatoreEffettivo(giocatore);
		}
	}
	
	/***
	 * imposta la casella su cui si sta giocando
	 * @param pastore pastore corrente
	 * @param core core di gioco
	 * @param server il server di gioco
	 * @return il la casella su cui si gioca
	 * @throws RemoteException
	 */
	public final Casella casellaEffettiva(Casella casella, GameCore core, ServerInterface server) throws RemoteException {
		if(server != null) {
			return (Casella) server.core().getStatus().getMappa().trovaCasella(casella.getId());
		} else {
			return (Casella) core.getStatus().getMappa().trovaCasella(casella.getId());
		}
	}
	
	/***
	 * imposta la regione effetiva di gioco
	 * @param pastore pastore corrente
	 * @param core core di gioco
	 * @param server il server di gioco
	 * @return la regione selezionata per l'azione
	 * @throws RemoteException
	 */
	public final Regione regioneEffettiva(Regione regione, GameCore core, ServerInterface server) throws RemoteException {
		if(server != null) {
			return (Regione) server.core().getStatus().getMappa().trovaRegione(regione.getId());
		} else {
			return (Regione) core.getStatus().getMappa().trovaRegione(regione.getId());
		}
	}
	
	/***
	 * imposta il movimento del pastore 
	 * @param gioc il giocatore associato al pastore
	 * @param core il gamecore di gioco
	 * @param server il server di gioco
	 */
	public final void muoviPastore(Giocatore gioc, GameCore core, ServerInterface server) {
		try {
			boolean mov;
			Giocatore giocatore = giocatoreEffettivo(gioc, core, server);
			String msg = "Seleziona la casella su cui spostare il pastore";
			JOptionPane.showMessageDialog(this, msg);
			updateAzione(msg);
			casellaPremuta = null;
			while(casellaPremuta == null) {
				Thread.sleep(50);
			}
			Casella old = casellaEffettiva(giocatore.getPastoreCorrente().getCasella(), core, server);
			int costo = giocatore.getMonete();
			if(server != null) {
					mov = server.muoviPastore(giocatore.getPastoreCorrente(), casellaPremuta);
			} else {
				mov = core.muoviPastore(giocatore.getPastoreCorrente(), casellaPremuta);				
			}
			costo -= giocatore.getMonete();
			if(!mov) {
				msg = "Impossibile muovere il pastore sulla casella selezionata!";
				JOptionPane.showMessageDialog(this, msg);
				updateAzione(msg);
				reset(core);
				return;
			}
			giocatore.setPastoreCorrente(pastoreEffettivo(giocatore.getPastoreCorrente(), core, server));
			giocatore.updatePastore(pastoreEffettivo(giocatore.getPastoreCorrente(), core, server));
			AnimazionePastore animazione = new AnimazionePastore();
	    	animazione.inizializza(this, contentPane, getViewCasella(casellaPremuta), getViewPastore(giocatore.getPastoreCorrente()));
	    	getViewCasella(old).update(core.getStatus().getNumeroRecinti() < 0);
	    	getViewPastore(giocatore.getPastoreCorrente()).setPastore(giocatore.getPastoreCorrente());
	    	new Thread(animazione).start();
	    	updateAzione("Pastore spostato. Costo: "+costo+" monete.");
	     	reset(core);
     	} catch (RemoteException e) {
			throw new MyRuntimeException(e);
		} catch (InterruptedException e) {
			// Ignora
		}
	}

	/***
	 * ritorna l'id del pastore di gioco
	 * @param pastore il pastore di cui si vuole l'id
	 * @return l'id del pastore
	 */
	public final int getIdViewPastore(Pastore pastore) {
		for(int i = 0; i < pastoreAssociato.size(); i++) {
			if(pastoreAssociato.get(i).equals(pastore)) {
				return i;
			}
		}
		return -1;
	}
	
	/***
	 * ritorna la viewpastore del pastore associato
	 * @param pastore il pastore di gioco di cui si vuole la viewpastore
	 * @return la viewpastore del pastore
	 */
	public final ViewPastore getViewPastore(Pastore pastore) {
		int id = getIdViewPastore(pastore);
		if(id == -1) {
			return null;
		}
		return viewPastori.get(id);
	}

	/***
	 * richiede il pastore da utilizzare per le azioni
	 * @param giocatore il giocatore che deve scegliere il pastore
	 * @param server il server di gioco
	 */
	public final void richiediPastoreCorrente(Giocatore giocatore, ServerInterface server) {
		boolean mov = false;
		String msg = "Seleziona un pastore";
		JOptionPane.showMessageDialog(this, "Giocatore "+giocatore.getColore()+": "+msg);
		updateAzione(msg);
		while(!mov) {
			pastorePremuto = null;
			while(pastorePremuto == null) {
				try {
					Thread.sleep(50);
				} catch(InterruptedException e) {
					// Ignora
				}
			}
			if(server != null) {
				try {
					mov = server.setPastoreCorrente(giocatore, pastorePremuto);
				} catch (RemoteException e) {
					throw new MyRuntimeException(e);
				}
			} else {
				mov = giocatore.setPastoreCorrente(pastorePremuto);
			}
			if(!mov) {
				msg = "Pastore non valido, ripeti la selezione!";
				JOptionPane.showMessageDialog(this, msg);
				updateAzione(msg);
			}
		}
		updateAzione("Seleziona una mossa da fare");
		reset(null);
	}
	
	/***
	 * muove l'animale da una regione ad un'altra
	 */
	public final void muoviAnimale() {
		Object[] options = { "Pecora", "Ariete", "Pecora Nera" };
		int n = JOptionPane.showOptionDialog(this, "Che animale vuoi spostare?", "Sposta animale", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[2]);
		switch(n) {
			case -1:
				azione = Azione.NO;
				break;
			case 0:
				azione = Azione.MUOVI_PECORA;
				break;
			case 1:
				azione = Azione.MUOVI_ARIETE;
				break;
			case 2:
				azione = Azione.MUOVI_PECORA_NERA;
				break;
			default:
				azione = Azione.NO;
				return;
		}
	}
	
	/***
	 * muove la pecora bianca
	 * @param giocatore il giocatore che muove l'animale
	 * @param core il gamecore di gioco
	 * @param server il server di gioco
	 */
	public final void muoviPecora(Giocatore giocatore, GameCore core, ServerInterface server) {
		boolean mov;
		String msg = "Seleziona la regione da cui muovere la pecora";
		JOptionPane.showMessageDialog(this, msg);
		updateAzione(msg);
		regionePremuta = null;
		while(regionePremuta == null) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// Ignora
			}
		}
		Regione regione1 = regionePremuta;
		if(regione1.getPecore() == 0 ) {
			msg = "La regione selezionata non ha pecore!";
			JOptionPane.showMessageDialog(this, msg);
			updateAzione(msg);
			reset(core);
			return;
		}
		msg = "Seleziona la regione su cui muovere la pecora";
		JOptionPane.showMessageDialog(this, msg);
		updateAzione(msg);
		regionePremuta = null;
		while(regionePremuta == null) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// Ignora
			}
		}
		if(server != null) {
			try {
				mov = server.muoviPecora(giocatore.getPastoreCorrente(), regione1, regionePremuta);
			} catch (RemoteException e) {
				throw new MyRuntimeException(e);
			}
		} else {
			mov = core.muoviPecora(giocatore.getPastoreCorrente(), regione1, regionePremuta);
		}
		animazioneAnimale.muoviPecoraBianca(contentPane, getViewRegione(regione1), getViewRegione(regionePremuta));
		new Thread(animazioneAnimale).start();
		if(!mov) {
			msg = "Impossibile muovere la pecora sulla regione selezionata!";
			JOptionPane.showMessageDialog(this, msg);
			updateAzione(msg);
			reset(core);
			return;
		}
		updateAzione("Pecora spostata!");
		getViewRegione(regionePremuta).getPecoraBianca().update();
		getViewRegione(regione1).getPecoraBianca().update();
		reset(core);
	}
	
	/***
	 * muove l'ariete
	 * @param giocatore il giocatore che muove l'animale
	 * @param core il gamecore di gioco
	 * @param server il server di gioco
	 */
	public final void muoviAriete(Giocatore giocatore, GameCore core, ServerInterface server) {
		boolean mov;
		String msg = "Seleziona la regione da cui muovere l'ariete";
		JOptionPane.showMessageDialog(this, msg);
		updateAzione(msg);
		regionePremuta = null;
		while(regionePremuta == null) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// Ignora
			}
		}
		Regione regione1 = regionePremuta;
		if(regione1.getArieti() == 0 ) {
			msg = "La regione selezionata non ha arieti!";
			JOptionPane.showMessageDialog(this, msg);
			updateAzione(msg);
			reset(core);
			return;
		}
		msg = "Seleziona la regione su cui muovere l'ariete";
		JOptionPane.showMessageDialog(this, msg);
		updateAzione(msg);
		regionePremuta = null;
		while(regionePremuta == null) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// Ignora
			}
		}
		if(server != null) {
			try {
				mov = server.muoviAriete(giocatore.getPastoreCorrente(), regione1, regionePremuta);
			} catch (RemoteException e) {
				throw new MyRuntimeException(e);
			}
		} else {
			mov = core.muoviAriete(giocatore.getPastoreCorrente(), regione1, regionePremuta);
		}
		animazioneAnimale.muoviAriete(contentPane, getViewRegione(regione1), getViewRegione(regionePremuta));
		new Thread(animazioneAnimale).start(); 
		if(!mov) {
			msg = "Impossibile muovere l'ariete sulla regione selezionata!";
			JOptionPane.showMessageDialog(this, msg);
			updateAzione(msg);
			reset(core);
			return;
		}
		updateAzione("Ariete spostato!");
		getViewRegione(regionePremuta).getAriete().update();
		getViewRegione(regione1).getAriete().update();
		reset(core);
	}
	
	/***
	 * muove la pecora nera
	 * @param giocatore il giocatore che muove l'animale
	 * @param core il gamecore di gioco
	 * @param server il server di gioco
	 */
	public final void muoviPecoraNera(Giocatore giocatore, GameCore core, ServerInterface server) {
		boolean mov = false;
		List<Regione> reg = core.getStatus().getMappa().regioneNearBy(giocatore.getPastoreCorrente().getCasella());
		for(int i = 0; i < reg.size(); i++) {
			if(reg.get(i).hasPecoraNera()) {
				mov = true;
			}
		}
		String msg;
		if(!mov) {
			msg = "Non c'è la pecora nera nelle tue vicinanze!";
			JOptionPane.showMessageDialog(this, msg);
			updateAzione(msg);
			reset(core);
			return;
		}
		msg = "Seleziona la regione da cui muovere la pecora nera";
		JOptionPane.showMessageDialog(this, msg);
		updateAzione(msg);
		regionePremuta = null;
		while(regionePremuta == null) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// Ignora
			}
		}
		Regione regione1 = regionePremuta;
		if(!regione1.hasPecoraNera()) {
			msg = "La regione selezionata non ha la pecora nera!";
			JOptionPane.showMessageDialog(this, msg);
			updateAzione(msg);
			reset(core);
			return;
		}
		msg = "Seleziona la regione su cui muovere la pecora nera";
		JOptionPane.showMessageDialog(this, msg);
		updateAzione(msg);
		regionePremuta = null;
		while(regionePremuta == null) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// Ignora
			}
		}
		if(server != null) {
			try {
				mov = server.muoviPecoraNera(giocatore.getPastoreCorrente(), regione1, regionePremuta);
			} catch (RemoteException e) {
				throw new MyRuntimeException(e);
			}
		} else {
			mov = core.muoviPecoraNera(giocatore.getPastoreCorrente(), regione1, regionePremuta);
		}
		animazioneAnimale.muoviPecoraNera(contentPane, getViewRegione(regione1), getViewRegione(regionePremuta));
		new Thread(animazioneAnimale).start();
		if(!mov) {
			msg = "Impossibile muovere la pecora nera sulla regione selezionata!";
			JOptionPane.showMessageDialog(this, msg);
			updateAzione(msg);
			reset(core);
			return;
		}
		updateAzione("Pecora nera spostata!");
		getViewRegione(regionePremuta).getPecoraNera().update();
		getViewRegione(regione1).getPecoraNera().update();
		reset(core);
	}
	
	/***
	 * muove la pecora nera , overload
	 * @param giocatore il giocatore che muove l'animale
	 * @param core il gamecore di gioco
	 * @param server il server di gioco
	 */
	public final void muoviPecoraNera(GameCore core, ServerInterface server) {
		Regione posizione = core.getStatus().getPosizionePecoraNera();
		if(server != null) {
			try {
				server.dadoPecoraNera();
			} catch (RemoteException e) {
				throw new MyRuntimeException(e);
			}
		} else {
			core.dadoPecoraNera();
		}
		getViewRegione(posizione).getPecoraNera().update();
		posizione = core.getStatus().getPosizionePecoraNera();
		getViewRegione(posizione).getPecoraNera().update();
	}
	
	public final void muoviLupo(GameCore core, ServerInterface server) {
		Regione posizione = core.getStatus().getPosizioneLupo();
		if(server != null) {
			try {
				server.dadoLupo();
			} catch (RemoteException e) {
				throw new MyRuntimeException(e);
			}
		} else {
			core.dadoLupo();
		}
		ViewRegione tmp = getViewRegione(posizione).getLupo().getRegione();
		animazioneAnimale.muoviLupo(contentPane, tmp, getViewRegione(core.getStatus().getPosizioneLupo()));
		new Thread(animazioneAnimale).start();
		getViewRegione(posizione).getLupo().update();
		posizione = core.getStatus().getPosizioneLupo();
		getViewRegione(posizione).getLupo().update();
		getViewRegione(posizione).getAriete().update();
		getViewRegione(posizione).getPecoraBianca().update();
	}
	
	/***
	 * accoppia gli animali di gioco
	 * @param giocatore il giocatore che fa effettuare l'accoppiamento
	 * @param core il gamecore di gioco
	 * @param servervil server di gioco
	 */
	public final void accoppia(Giocatore giocatore, GameCore core, ServerInterface server) {
		String msg = "Seleziona la regione su cui effettuare l'accoppiamento";
		JOptionPane.showMessageDialog(this, msg);
		updateAzione(msg);
		regionePremuta = null;
		while(regionePremuta == null) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// Ignora
			}
		}
		try {
			boolean mov;
			if(server != null) {
				mov = server.accoppia(giocatore.getPastoreCorrente(), regionePremuta);
			} else {
				mov = core.accoppia(giocatore.getPastoreCorrente(), regionePremuta);
			}
			if(!mov) { 
				msg = "Impossibile eseguire l'accoppiamento sulla regione selezionata!";
				JOptionPane.showMessageDialog(this, msg);
				updateAzione(msg);
			} else {
				msg = "Accoppiamento eseguito con successo!";
				JOptionPane.showMessageDialog(this, msg);
				updateAzione(msg);
				getViewRegione(regionePremuta).getAgnello().update();
			}
		} catch (DadoException e) {
			msg = "Accoppiamento fallito. Sul dado è uscito: "+e.getMessage();
			JOptionPane.showMessageDialog(this, msg);
			updateAzione(msg);
		} catch (RemoteException e) {
			throw new MyRuntimeException(e);
		}
		reset(core);
	}
	
	/***
	 * effettua l'azione di acquisto di un terreno
	 * @param giocatore il giocatore che effettua l'acquisto
	 * @param core il gamecore di gioco
	 * @param server il server di gioco
	 */
	public final void compraTerreno(Giocatore giocatore, GameCore core, ServerInterface server) {
		List<Terreno> terreni = core.terreniComprabili(giocatore.getPastoreCorrente());
		int n;
		if(terreni.size() == 2) {
			Object[] options = {terreni.get(0), terreni.get(1), "Annulla"};
			n = JOptionPane.showOptionDialog(this, "Che terreno vuoi comprare?", "Compra Terreno", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[2]);
		} else {
			Object[] options = {terreni.get(0), "Annulla"};
			n = JOptionPane.showOptionDialog(this, "Che terreno vuoi comprare?", "Compra Terreno", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
		}
		if( n >= terreni.size() || n < 0) {
			reset(core);
			return;
		}
		boolean mov;
		if(server != null) {
			try {
				mov = server.compraTerreno(giocatore.getPastoreCorrente(), terreni.get(n));
			} catch (RemoteException e) {
				throw new MyRuntimeException(e);
			}
		} else {
			mov = core.compraTerreno(giocatore.getPastoreCorrente(), terreni.get(n));
		}
		if(!mov) {
			String msg = "Impossibile comprare il terreno selezionato!";
			JOptionPane.showMessageDialog(this, msg);
			updateAzione(msg);
		}
		updateAzione("Terreno comprato!");
		reset(core);
	}
	
	/***
	 * effettua sparatoria su una regione e su un animale
	 * @param giocatore il giocatore che effettua la sparatoria
	 * @param core il gamecore di gioco
	 * @param server il server di gioco
	 */
	public final void sparatoria(Giocatore giocatore, GameCore core, ServerInterface server) {
		String msg = "Seleziona la regione su cui effettuare la sparatoria";
		JOptionPane.showMessageDialog(this, msg);
		updateAzione(msg);
		regionePremuta = null;
		while(regionePremuta == null) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// Ignora
			}
		}
		Object[] options = {"Pecora","Ariete", "Annulla"};
		int n = JOptionPane.showOptionDialog(this, "A quale animale vuoi sparare?", "Sparatoria", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[2]);
		Animale animale;
		if(n == -1) {
			reset(core);
			return;
		} else if(n == 0) {
			animale = Animale.PECORA;
		} else {
			animale = Animale.ARIETE;
		}	
		try {
			int corrotti;
			if(server != null) {
				corrotti = server.sparatoria(giocatore.getPastoreCorrente(), regionePremuta, animale);
			} else {
				corrotti = core.sparatoria(giocatore.getPastoreCorrente(), regionePremuta, animale);
			}
			
			if(corrotti == -1) {
				msg = "Impossibile sparare a "+animale+" in questa regione!";
				JOptionPane.showMessageDialog(this, msg);
				updateAzione(msg);
			} else {
				msg = "Hai sparato a "+animale+"!";
				if(corrotti > 0) {
					msg += corrotti+" pastori hanno assistito alla scena e li hai corrotti con "+(corrotti*2)+" monete!";
				}
				JOptionPane.showMessageDialog(this, msg);
				updateAzione(msg);
			}
			getViewRegione(regionePremuta).update();
			updateContatori(giocatore, core);
			reset(core);
		} catch (DadoException e) {
			msg = "Sparatoria non eseguita, sul dado è uscito "+e.getMessage()+"!";
			JOptionPane.showMessageDialog(this, msg);
			updateAzione(msg);
			reset(core);
		} catch (RemoteException e) {
			throw new MyRuntimeException(e);
		}
	}
	
	/***
	 * setta la fine del gioco, da qui si inizieranno i conteggi per il vincitore
	 * @param core il gamecore di gioco
	 * @param server il server di gioco
	 */
	public final void endGame(GameCore core, ServerInterface server) {
		String[] winnerString;
		if(server != null) {
			try {
				winnerString = server.core().decretaVincitore();
			} catch (RemoteException e) {
				throw new MyRuntimeException(e);
			}
		} else {
			winnerString = core.decretaVincitore();
		}
		JOptionPane.showMessageDialog(this, winnerString[0]);
		if(server != null) {
			try {
				server.setGameEnd(true);
			} catch (RemoteException e) {
				throw new MyRuntimeException(e);
			}
		} else {
			core.getStatus().setGameEnd(true);
		}
		this.dispose();
	}
	
	/***
	 * resetta l'azione di gioco se non valida
	 * @param core il gamecore corrente
	 */
	public final void reset(GameCore core) {
		regionePremuta = null;
		regionePremuta = null;
		pastorePremuto = null;
		if(reteSocket != null && core != null) {
			reteSocket.setNotCore(core);
		}
		azione = Azione.NO;
	}
	
	/***
	 * aggiorna i bottoni rendendo di volta in volta visibili solo quelli associati ad azioni disponibili
	 * @param current il giocatore corrente
	 * @param core gamceore di gioco
	 */
	public final void updateButtons(Giocatore current, GameCore core) {
		try {
			Pastore tmp = pastoreEffettivo(current.getPastoreCorrente(), core, null);
			if(tmp == null) {
				return;
			}
			boolean check = core.puoEseguire(tmp);
			btnMuoviAnimale.setEnabled(!tmp.isPecoraMossa() && check);
			btnAccoppia.setEnabled(!tmp.isPecoraAccoppiata() && check);
			btnSparatoria.setEnabled(!tmp.isHaSparato() && check);
			btnCompraTerreno.setEnabled(!tmp.isTerrenoComprato() && check);
			btnMuoviPastore.setEnabled(tmp.getNumeroMossa() < 3);
			btnTerminaTurno.setEnabled(tmp.getNumeroMossa() == 3);
			btnCompravendita.setEnabled(true);
		} catch (RemoteException e) {
			throw new MyRuntimeException(e);
		}
	}
	
	/***
	 * disabilita tutti i bottoni perche e' finito il turno
	 */
	public final void allButtonsDisabled() {
		btnMuoviAnimale.setEnabled(false);
		btnCompravendita.setEnabled(false);
		btnAccoppia.setEnabled(false);
		btnSparatoria.setEnabled(false);
		btnCompraTerreno.setEnabled(false);
		btnMuoviPastore.setEnabled(false);
		btnTerminaTurno.setEnabled(false);
	}
	
	/***
	 * ritorna il pannello di gioco
	 */
	public final JPanel getContentPane() {
		return contentPane;
	}

	/***
	 * imposta il server di gioco
	 * @param reteSocket la retesocket di gioco
	 */
	public final void setServer(ReteSocket reteSocket) {
		this.reteSocket = reteSocket;
	}
}
