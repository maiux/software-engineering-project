package it.polimi.lamalfa_maioli.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.net.InetAddress;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;

/***
 * classe che imposta il server di gioco
 * @author lamalfa_maioli
 *
 */
public class ReteServer implements ActionListener, Serializable {
	private static final long serialVersionUID = 2338915327423803620L;
	private JFrame frame;
	private JPanel contentPane;
	private JLabel lblNewLabel;
	private boolean start = false;
	private JButton btnNewButton;
	private int clients = 0;
	/***
	 * fa partire la label per la connessione al server
	 * @return true se la connessione va a buon fine
	 */
	public boolean getStart() {
		return start;
	}
	
	/***
	 * inizia connessione
	 */
	public void inizia() {
		start = true;
		frame.dispose();
	}
	
	/***
	 * aggiorna ilnumero di client connessi
	 * @param num il numero di giocatori connessi
	 */
	public void updateClientConnessi(int num) {
		lblNewLabel.setText("Client connessi: "+num);
		btnNewButton.setEnabled(num > 0);
		this.clients = num;
	}

	/**
	 * Crea il frame di connessione
	 */
	public ReteServer() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			// Ignora
		} catch (InstantiationException e) {
			// Ignora
		} catch (IllegalAccessException e) {
			// Ignora
		} catch (UnsupportedLookAndFeelException e) {
			// Ignora
		}
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 152, 119);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblNewLabel = new JLabel("Client connessi: 0");
		lblNewLabel.setBounds(10, 11, 227, 14);
		contentPane.add(lblNewLabel);
		
		btnNewButton = new JButton("Inizia Partita");
		btnNewButton.addActionListener(this);
		btnNewButton.setEnabled(false);
		btnNewButton.setBounds(10, 45, 112, 23);
		contentPane.add(btnNewButton);
		frame.setVisible(true);
	}

	/***
	 * imposta address di gioco e porta di gioco
	 * @param inetAddress l'address di gioco
	 * @param localPort la porta di gioco
	 */
	public ReteServer(InetAddress inetAddress, int localPort) {
		this();
	}
	
	/***
	 * override su funzione che esegue azione per fare partire il server di gioco
	 */	
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton button = (JButton) e.getSource();
		if(button.equals(btnNewButton) && clients > 0) {
			start = true;
			frame.dispose();
		}
	}
}
