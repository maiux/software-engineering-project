package it.polimi.lamalfa_maioli.view;

import java.io.Serializable;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/***
 * classe view animale
 * @author lamalfa_maioli
 *
 */
public class AnimazioneAnimale implements Runnable, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3220372079171143969L;
	private JLabel labelTmp = new JLabel();
	private int labelTmpX;
	private int labelTmpY;
	private int labelDestX;
	private int labelDestY;
	private JPanel contentPane;
	
	/***
	 * imposta animale
	 * @param contentPane pannello
	 */
	public AnimazioneAnimale(JPanel contentPane) { 
		this.labelTmp.setBounds(20, 20, 30, 30);
		this.labelTmp.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/pecora.png")));
		this.labelTmp.setVisible(false);
		contentPane.add(labelTmp);
	}
	
	/***
	 * muove pecora bianca
	 * @param contentPane pannello	principale
	 * @param regSource regione partenza
	 * @param regDest regione destinazione
	 * @return la label dell animale mosso
	 */
	public JLabel muoviPecoraBianca(JPanel contentPane, ViewRegione regSource, ViewRegione regDest) {
		this.contentPane = contentPane;
		this.labelTmpX = regSource.getPecoraBianca().getX();
		this.labelTmpY = regSource.getPecoraBianca().getY();
		this.labelDestX = regDest.getPecoraBianca().getX();
		this.labelDestY = regDest.getPecoraBianca().getY();
		this.labelTmp.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/pecora.png")));
		this.labelTmp.setBounds(labelTmpX, labelTmpY, 30, 30);
		return this.labelTmp;
	}
	
	/***
	 * muove pecora nera
	 * @param contentPane pannello	principale
	 * @param regSource regione partenza
	 * @param regDest regione destinazione
	 * @return la label dell animale mosso
	 */
	public JLabel muoviPecoraNera(JPanel contentPane, ViewRegione regSource, ViewRegione regDest) {
		this.contentPane = contentPane;
		this.labelTmpX = regSource.getPecoraNera().getX();
		this.labelTmpY = regSource.getPecoraNera().getY();
		this.labelDestX = regDest.getPecoraNera().getX();
		this.labelDestY = regDest.getPecoraNera().getY();
		this.labelTmp.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/pecoranera.png")));
		this.labelTmp.setBounds(labelTmpX, labelTmpY, 30, 30);
		return this.labelTmp;
	}
	
	/***
	 * muove lupo
	 * @param contentPane pannello	principale
	 * @param regSource regione partenza
	 * @param regDest regione destinazione
	 * @return la label dell animale mosso
	 */
	public JLabel muoviLupo(JPanel contentPane, ViewRegione regSource, ViewRegione regDest) {
		this.contentPane = contentPane;
		this.labelTmpX = regSource.getLupo().getX();
		this.labelTmpY = regSource.getLupo().getY();
		this.labelDestX = regDest.getLupo().getX();
		this.labelDestY = regDest.getLupo().getY();
		this.labelTmp.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/lupo.png")));
		this.labelTmp.setBounds(labelTmpX, labelTmpY, 30, 30);
		return this.labelTmp;
	}
	
	/***
	 * muove ariete
	 * @param contentPane pannello	principale
	 * @param regSource regione partenza
	 * @param regDest regione destinazione
	 * @return la label dell animale mosso
	 */
	public JLabel muoviAriete(JPanel contentPane, ViewRegione regSource, ViewRegione regDest) {
		this.contentPane = contentPane;
		this.labelTmpX = regSource.getAriete().getX();
		this.labelTmpY = regSource.getAriete().getY();
		this.labelDestX = regDest.getAriete().getX();
		this.labelDestY = regDest.getAriete().getY();
		this.labelTmp.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/ariete.png")));
		this.labelTmp.setBounds(labelTmpX, labelTmpY, 30, 30);
		return this.labelTmp;
	}
	
/***
 * fa partire thread separato per animazione
 */
	@Override
	public void run() {
		try {
		this.labelTmp.setVisible(true);
			while(labelTmpX != labelDestX || labelTmpY != labelDestY) {
				if(labelTmpX < labelDestX) {
					this.labelTmpX++;
				} else if(labelTmpX > labelDestX){
					this.labelTmpX--;
				} 
				if(labelTmpY < labelDestY) {
					this.labelTmpY++;
				} else if(labelTmpY > labelDestY) {
					this.labelTmpY--;
				}
				this.labelTmp.setBounds(labelTmpX, labelTmpY, 30, 30);
				contentPane.validate();
	            contentPane.repaint(); 
	            Thread.sleep(1);
				}
			} catch(InterruptedException e) {
			// Ignora
		} finally {
			this.labelTmp.setBounds(20, 20, 30, 30);
			this.labelTmp.setVisible(false);
		}


	}
	
}
