package it.polimi.lamalfa_maioli.view;

import java.io.Serializable;

import javax.swing.JPanel;

/***
 * classe per la visualizzazione a schermo degli animali nelle regioni
 * @author lamalfa_maioli
 *
 */
public class ControllerAnimali implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3763646053483632707L;
	/***
 * imposta pecore bianche iniziali su ogni regione	
 * @param contentPane panello della mappa
 * @param pecoraBianca l'oggetto che gestisce la label della pecora
 * @param regione ViewRegione, regione dove si trova la pecora
 */
	public void impostaPecoreBianche(JPanel contentPane, ViewPecoraBianca [] pecoraBianca, ViewRegione [] regione) {
		pecoraBianca[0]  = new ViewPecoraBianca(contentPane, regione[0], 60, 149, 30, 30);
		pecoraBianca[1]  = new ViewPecoraBianca(contentPane, regione[1], 100, 284, 30, 30);
		pecoraBianca[2]  = new ViewPecoraBianca(contentPane, regione[2], 183, 354, 30, 30);
		pecoraBianca[3]  = new ViewPecoraBianca(contentPane, regione[3], 103, 433, 30, 30);
		pecoraBianca[4]  = new ViewPecoraBianca(contentPane, regione[4], 177, 495, 30, 30);
		pecoraBianca[5]  = new ViewPecoraBianca(contentPane, regione[5], 258, 412, 30, 30);
		pecoraBianca[6]  = new ViewPecoraBianca(contentPane, regione[6], 251, 558, 30, 30);
		pecoraBianca[7]  = new ViewPecoraBianca(contentPane, regione[7], 330, 495, 30, 30);
		pecoraBianca[8]  = new ViewPecoraBianca(contentPane, regione[8], 332, 360, 30, 30);
		pecoraBianca[9]  = new ViewPecoraBianca(contentPane, regione[9], 401, 417, 30, 30);
		pecoraBianca[10]  = new ViewPecoraBianca(contentPane, regione[10], 415, 295, 30, 30);
		pecoraBianca[11]  = new ViewPecoraBianca(contentPane, regione[11], 343, 248, 30, 30);
		pecoraBianca[12]  = new ViewPecoraBianca(contentPane, regione[12], 415, 198, 30, 30);
		pecoraBianca[13]  = new ViewPecoraBianca(contentPane, regione[13], 378, 124, 30, 30);
		pecoraBianca[14]  = new ViewPecoraBianca(contentPane, regione[14], 268, 237, 30, 30);
		pecoraBianca[15]  = new ViewPecoraBianca(contentPane, regione[15], 287, 130, 30, 30);
		pecoraBianca[16]  = new ViewPecoraBianca(contentPane, regione[16], 183, 149, 30, 30);
		pecoraBianca[17]  = new ViewPecoraBianca(contentPane, regione[17], 183, 238, 30, 30);
		pecoraBianca[18]  = new ViewPecoraBianca(contentPane, regione[18], 226, 319, 30, 30);
		
		for(int i=0; i<=18; i++) {
			regione[i].setPecoraBianca(pecoraBianca[i]);
		}
	}
	
	/***
	 * imposta la pecora nera su schermo	
	 * @param contentPane panello della mappa
	 * @param pecoraBianca l'oggetto che gestisce la label della pecora
	 * @param regione ViewRegione, regione dove si trova la pecora
	 */
	public void impostaPecoreNere(JPanel contentPane, ViewPecoraNera [] pecoraNera, ViewRegione [] regione) {
		pecoraNera[0]  = new ViewPecoraNera(contentPane, regione[0], 75, 179, 30, 30);
		pecoraNera[1]  = new ViewPecoraNera(contentPane, regione[1], 100, 344, 30, 30);
		pecoraNera[2]  = new ViewPecoraNera(contentPane, regione[2], 183, 384, 30, 30);
		pecoraNera[3]  = new ViewPecoraNera(contentPane, regione[3], 103, 460, 30, 30);
		pecoraNera[4]  = new ViewPecoraNera(contentPane, regione[4], 177, 520, 30, 30);
		pecoraNera[5]  = new ViewPecoraNera(contentPane, regione[5], 258, 440, 30, 30);
		pecoraNera[6]  = new ViewPecoraNera(contentPane, regione[6], 235, 588, 30, 30);
		pecoraNera[7]  = new ViewPecoraNera(contentPane, regione[7], 330, 525, 30, 30);
		pecoraNera[8]  = new ViewPecoraNera(contentPane, regione[8], 332, 385, 30, 30);
		pecoraNera[9]  = new ViewPecoraNera(contentPane, regione[9], 401, 442, 30, 30);
		pecoraNera[10]  = new ViewPecoraNera(contentPane, regione[10], 405, 320, 30, 30);
		pecoraNera[11]  = new ViewPecoraNera(contentPane, regione[11], 323, 265, 30, 30);
		pecoraNera[12]  = new ViewPecoraNera(contentPane, regione[12], 445, 208, 30, 30);
		pecoraNera[13]  = new ViewPecoraNera(contentPane, regione[13], 358, 150, 30, 30);
		pecoraNera[14]  = new ViewPecoraNera(contentPane, regione[14], 260, 201, 30, 30);
		pecoraNera[15]  = new ViewPecoraNera(contentPane, regione[15], 297, 110, 30, 30);
		pecoraNera[16]  = new ViewPecoraNera(contentPane, regione[16], 153, 175, 30, 30);
		pecoraNera[17]  = new ViewPecoraNera(contentPane, regione[17], 183, 263, 30, 30);
		pecoraNera[18]  = new ViewPecoraNera(contentPane, regione[18], 258, 347, 30, 30);
		for(int i=0; i<=18; i++) {
			regione[i].setPecoraNera(pecoraNera[i]);
		}
	}
	
	/***
	 * imposta gli arieti schermo	
	 * @param contentPane panello della mappa
	 * @param pecoraBianca l'oggetto che gestisce la label dell'ariete
	 * @param regione ViewRegione, regione dove si trova l'ariete
	 */
	public void impostaArieti(JPanel contentPane,ViewAriete[] ariete, ViewRegione[] regione) {
		ariete[0]  = new ViewAriete(contentPane, regione[0], 90, 209, 30, 30);
		ariete[1]  = new ViewAriete(contentPane, regione[1], 110, 311, 30, 30);
		ariete[2]  = new ViewAriete(contentPane, regione[2], 183, 414, 30, 30);
		ariete[3]  = new ViewAriete(contentPane, regione[3], 103, 488, 30, 30);
		ariete[4]  = new ViewAriete(contentPane, regione[4], 157, 540, 30, 30);
		ariete[5]  = new ViewAriete(contentPane, regione[5], 258, 460, 30, 30);
		ariete[6]  = new ViewAriete(contentPane, regione[6], 223, 608, 30, 30);
		ariete[7]  = new ViewAriete(contentPane, regione[7], 330, 545, 30, 30);
		ariete[8]  = new ViewAriete(contentPane, regione[8], 332, 410, 30, 30);
		ariete[9]  = new ViewAriete(contentPane, regione[9], 401, 465, 30, 30);
		ariete[10]  = new ViewAriete(contentPane, regione[10], 415, 343, 30, 30);
		ariete[11]  = new ViewAriete(contentPane, regione[11], 333, 288, 30, 30);
		ariete[12]  = new ViewAriete(contentPane, regione[12], 425, 228, 30, 30);
		ariete[13]  = new ViewAriete(contentPane, regione[13], 378, 175, 30, 30);
		ariete[14]  = new ViewAriete(contentPane, regione[14], 298, 197, 30, 30);
		ariete[15]  = new ViewAriete(contentPane, regione[15], 297, 84, 30, 30);
		ariete[16]  = new ViewAriete(contentPane, regione[16], 183, 177, 30, 30);
		ariete[17]  = new ViewAriete(contentPane, regione[17], 183, 290, 30, 30);
		ariete[18]  = new ViewAriete(contentPane, regione[18], 286, 319, 30, 30);
		for(int i=0; i<=18; i++) {
			regione[i].setAriete(ariete[i]);
		}
	}
	
	/***
	 * imposta gli agnelli su schermo	
	 * @param contentPane panello della mappa
	 * @param pecoraBianca l'oggetto che gestisce la label dell'agnello
	 * @param regione ViewRegione, regione dove si trova l'agnello
	 */
	public void impostaAgnelli(JPanel contentPane, ViewAgnello[] agnello, ViewRegione[] regione) {
		agnello[0]  = new ViewAgnello(contentPane, regione[0], 130, 209, 30, 30);
		agnello[1]  = new ViewAgnello(contentPane, regione[1], 145, 311, 30, 30);
		agnello[2]  = new ViewAgnello(contentPane, regione[2], 213, 383, 30, 30);
		agnello[3]  = new ViewAgnello(contentPane, regione[3], 133, 433, 30, 30);
		agnello[4]  = new ViewAgnello(contentPane, regione[4], 207, 500, 30, 30);
		agnello[5]  = new ViewAgnello(contentPane, regione[5], 288, 412, 30, 30);
		agnello[6]  = new ViewAgnello(contentPane, regione[6], 274, 558, 30, 30);
		agnello[7]  = new ViewAgnello(contentPane, regione[7], 370, 540, 30, 30);
		agnello[8]  = new ViewAgnello(contentPane, regione[8], 362, 350, 30, 30);
		agnello[9]  = new ViewAgnello(contentPane, regione[9], 431, 445, 30, 30);
		agnello[10]  = new ViewAgnello(contentPane, regione[10], 430, 320, 30, 30);
		agnello[11]  = new ViewAgnello(contentPane, regione[11], 358, 272, 30, 30);
		agnello[12]  = new ViewAgnello(contentPane, regione[12], 465, 188, 30, 30);
		agnello[13]  = new ViewAgnello(contentPane, regione[13], 408, 124, 30, 30);
		agnello[14]  = new ViewAgnello(contentPane, regione[14], 298, 237, 30, 30);
		agnello[15]  = new ViewAgnello(contentPane, regione[15], 340, 84, 30, 30);
		agnello[16]  = new ViewAgnello(contentPane, regione[16], 242, 149, 30, 30);
		agnello[17]  = new ViewAgnello(contentPane, regione[17], 213, 258, 30, 30);
		agnello[18]  = new ViewAgnello(contentPane, regione[18], 256, 320, 30, 30);
		for(int i=0; i<=18; i++) {
			regione[i].setAgnello(agnello[i]);
		}
	}
	
	/***
	 * imposta il lupo schermo	
	 * @param contentPane panello della mappa
	 * @param pecoraBianca l'oggetto che gestisce la label del lupo
	 * @param regione ViewRegione, regione dove si trova il lupo
	 */
	public void impostaLupo(JPanel contentPane, ViewLupo[] lupo, ViewRegione[] regione) {
		lupo[0]  = new ViewLupo(contentPane, regione[0], 90, 149, 30, 30);
		lupo[1]  = new ViewLupo(contentPane, regione[1], 130, 284, 30, 30);
		lupo[2]  = new ViewLupo(contentPane, regione[2], 213, 427, 30, 30);
		lupo[3]  = new ViewLupo(contentPane, regione[3], 133, 460, 30, 30);
		lupo[4]  = new ViewLupo(contentPane, regione[4], 213, 520, 30, 30);
		lupo[5]  = new ViewLupo(contentPane, regione[5], 288, 440, 30, 30);
		lupo[6]  = new ViewLupo(contentPane, regione[6], 274, 528, 30, 30);
		lupo[7]  = new ViewLupo(contentPane, regione[7], 360, 510, 30, 30);
		lupo[8]  = new ViewLupo(contentPane, regione[8], 362, 375, 30, 30);
		lupo[9]  = new ViewLupo(contentPane, regione[9], 431, 415, 30, 30);
		lupo[10]  = new ViewLupo(contentPane, regione[10], 415, 270, 30, 30);
		lupo[11]  = new ViewLupo(contentPane, regione[11], 373, 248, 30, 30);
		lupo[12]  = new ViewLupo(contentPane, regione[12], 445, 178, 30, 30);
		lupo[13]  = new ViewLupo(contentPane, regione[13], 388, 150, 30, 30);
		lupo[14]  = new ViewLupo(contentPane, regione[14], 282, 217, 30, 30);
		lupo[15]  = new ViewLupo(contentPane, regione[15], 317, 130, 30, 30);
		lupo[16]  = new ViewLupo(contentPane, regione[16], 213, 149, 30, 30);
		lupo[17]  = new ViewLupo(contentPane, regione[17], 213, 225, 30, 30);
		lupo[18]  = new ViewLupo(contentPane, regione[18], 256, 289, 30, 30);
		for(int i=0; i<=18; i++) {
			regione[i].setLupo(lupo[i]);
		}
		
	}
	
	/***
	 * imposta label animale
	 * @param contentPane il pannello dell'animale passato
	 */
	public void impostaLabelAnimazione(JPanel contentPane) {
		new AnimazioneAnimale(contentPane);
	}
	
}