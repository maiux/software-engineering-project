package it.polimi.lamalfa_maioli.view;

import java.io.Serializable;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/***
 * classe che gestisce la visualizzazione sulla mappa del lupo
 * @author lamalfa_maioli
 *
 */
public class ViewLupo implements Serializable {
	
	private static final long serialVersionUID = -615925618690275196L;
	private JLabel labelLupo;
	private ViewRegione viewRegione;
	private int labelX;
	private int labelY;
	private int altezza;
	private int larghezza;
	
	/***
	 * creatore classe viewlupo, imposta i parametri per la visualizzazione sulla mappa
	 * @param contentPane il pannello della mappa
	 * @param viewReg la label della regione
	 * @param x posizione label ascisse
	 * @param y posizione label ordinate
	 * @param alt altezza label
	 * @param lar larghezza label
	 */
	public ViewLupo(JPanel contentPane, ViewRegione viewReg, int x, int y, int alt,int lar) {
		this.labelLupo = new JLabel();
		this.viewRegione = viewReg;
		this.labelX = x;
		this.labelY = y;
		this.altezza = alt;
		this.larghezza = lar;
		this.labelLupo.setBounds(labelX, labelY, altezza+5, larghezza+5);
		this.labelLupo.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/lupo.png")));
		update();
		contentPane.add(labelLupo);
	}
	
	/***
	 * ritorna la viewregione con su il lupo
	 * @return la viewregione con su il lupo
	 */
	public ViewRegione getRegione() {
		return viewRegione;
	}
	
	/***
	 * mostra il lupo sula regione, se presente
	 */
	public final void update() {
		this.labelLupo.setVisible(viewRegione.getRegione().hasLupo());
	}
	
	/***
	 * 
	 * @return coordinata x della label
	 */
	public int getX() {
		return labelX;
	}
		
	/***
	 * 
	 * @return coordinata y della label
	 */
	public int getY() {
		return labelY;
	}
}
