package it.polimi.lamalfa_maioli.view;

import it.polimi.lamalfa_maioli.model.Casella;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/***
 * classe per visualizzare su schermo ogni casella
 * @author lamalfa_maioli
 *
 */
public class ViewCasella implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8288231812438808485L;
	private JLabel label; 
	private int labelX;
	private int labelY;
	private int altezza;
	private int larghezza;
	private Casella casella;
	private static ControllerMappa controller;
	private boolean isFinal = false;
	/***
	 * istanzia una ViewCasella che serve a vedere la casella sulla mappa di gioco
	 * @param contentPane pannello della mappa di gioco
	 * @param x posizione ascisse
	 * @param y posizione ordinate
	 * @param alt altezza casella
	 * @param lar larghezza casella
	 */
	public ViewCasella(JPanel contentPane, int x, int y, int alt, int lar) {
		this.label = new JLabel(" ");
		this.labelX = x;
		this.labelY = y;
		this.altezza = alt;
		this.larghezza = lar;
		this.label.setBounds(labelX, labelY-15, altezza, larghezza+20);
		contentPane.add(label);
	}
	
	public void setController(ControllerMappa controller) {
		ViewCasella.controller = controller;
	}
		
	/***
	 * imposta i recinti e li fa vedere su schermo se presenti
	 */
	public void update(boolean finale) {
		if(casella.hasRecinto()) {
			if(!finale) {
				this.label.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/icorecinto.png")));
			} else {
				this.label.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/icorecintofinale.png")));
				isFinal = finale;
			}
		}
	}
	
	/***
	 * imposta la presenza del recinto
	 */
	public void rUpdate() {
		if(casella.hasRecinto()) {
			update(isFinal);
		}
	}
	
	/***
	 * imposta la presenza di un recinto finale
	 * @return true se e' presente un recinto finale
	 */
	public boolean isFinale() {
		return isFinal;
	}
	
	/***
	 * 
	 * @return casella della ViewCasella
	 */
	public Casella getCasella() {
		return this.casella;
	}
	
	/***
	 * imposta casella della ViewCasella
	 * @param casella la casella da impostare
	 */
	public void setCasella(Casella casella) {
		this.casella = casella;
		label.addMouseListener ( new MyMouseEvent (casella, controller) {
			private static final long serialVersionUID = 2885927127634537148L;

			public void mousePressed ( MouseEvent e) {
            	this.getController().setCasellaPremuta((Casella) this.getVertex());
            }
        } );
	}
	
	/***
	 * imposta label della ViewCasella
	 * @param label label della casella
	 */
	public void setLabel(JLabel label) {
		this.label = label;
	}
	
	/***
	 * 
	 * @return la label della casella
	 */
	public JLabel getLabel() {
		return this.label;
	}
	
	/***
	 * 
	 * @return coordinata x della label
	 */
	public int getX() {
		return labelX;
	}
		
	/***
	 * 
	 * @return coordinata y della label
	 */
	public int getY() {
		return labelY;
	}
	
	/***
	 * @see Object#equals()
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj.getClass() != this.getClass()) {
			return false;
		}
		Casella rem = ((ViewCasella)obj).getCasella();
		if(casella.equals(rem)) {
			return true;
		}
		return false;
	}
	
	/***
	 * @see Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return 31337+casella.getId();
	}
}
