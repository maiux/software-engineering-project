package it.polimi.lamalfa_maioli.view;

import java.awt.Color;
import java.awt.Font;
import java.io.Serializable;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/***
 * classe che gestisce la visualizzazione a schermo dell'agnello
 * @author lamalfa_maioli
 *
 */
public class ViewAgnello implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 672477342634828731L;
	private ViewRegione viewRegione;
	private JLabel labelAgnello;
	private JLabel labelNumeroAgnelli;	
	private int labelX;
	private int labelY;
	private int altezza;
	private int larghezza;
	
	/***
	 * costruttore Ariete, inizializza l'immagine sulla mappa
	 * @param contentPane il pannello della mappa
	 * @param viewReg la regione su cui e' la Ariete
	 * @param x coordinata ascisse
	 * @param y coordinata ordinate
	 * @param alt grandezza label Ariete
	 * @param lar larghezza label Ariete
	 */
	public ViewAgnello(JPanel contentPane, ViewRegione viewReg, int x, int y, int alt,int lar) {
		this.labelAgnello = new JLabel(" ");
		this.labelNumeroAgnelli = new JLabel();
		this.viewRegione = viewReg;
		this.labelX = x;
		this.labelY = y;
		this.altezza = alt;
		this.larghezza = lar;
		this.labelAgnello.setBounds(labelX, labelY, altezza, larghezza);
		this.labelNumeroAgnelli.setForeground(Color.RED);
		this.labelNumeroAgnelli.setFont(new Font("Tahoma", Font.BOLD, 12));
		this.labelNumeroAgnelli.setBounds(labelX, labelY, 10, 10);
		this.labelAgnello.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/agnello.png")));
		update();
		contentPane.add(labelNumeroAgnelli);
		contentPane.add(labelAgnello);
	
	}
		
	/***
	 * 
	 * @return coordinata x della label
	 */
	public int getX() {
		return labelX;
	}
		
	/***
	 * 
	 * @return coordinata y della label
	 */
	public int getY() {
		return labelY;
	}
	
	/***
	 * aggiorna i contatori sull'animale e in caso lo rende invisibile
	 */
	public final void update() {
		int num = viewRegione.getRegione().getAgnelli().size();
		if(num == 0) {
			labelAgnello.setVisible(false);
			labelNumeroAgnelli.setVisible(false);
		} else {
			labelAgnello.setVisible(true);
			labelNumeroAgnelli.setVisible(true);
			labelNumeroAgnelli.setText(num+"");
		}
	}

	
}
