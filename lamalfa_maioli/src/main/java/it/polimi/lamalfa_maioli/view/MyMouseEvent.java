package it.polimi.lamalfa_maioli.view;

import it.polimi.lamalfa_maioli.model.Vertex;

import java.awt.event.MouseAdapter;
import java.io.Serializable;

/***
 * classe che gestisce il muose event su regioni e caselle
 * @author lamalfa_maioli
 *
 */
public class MyMouseEvent extends MouseAdapter  implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4610236877084906445L;
	private Vertex v;
	private ControllerMappa controller;
	
	/***
	 * muose event sulla casella
	 * @param v il vertice-casella
	 * @param controller il controller mappa che contiene la mappa
	 */
	public MyMouseEvent(Vertex v, ControllerMappa controller) {
		this.v = v;
		this.controller = controller;
	}
	
	/***
	 * ritorna il controllermappa corrente
	 * @param controller il controllermappa corrente
	 */
	public MyMouseEvent(ControllerMappa controller) {
		this.controller = controller;
	}
	
	/***
	 * ritorna la casella corrente 
	 * @return il vertice-casella
	 */
	public Vertex getVertex() {
		return v;
	}
	
	/***
	 * ritorna il controllermappa di gioco
	 * @return il controllermappa di gioco su cui avviene il gicoo
	 */
	public ControllerMappa getController() {
		return controller;
	}
}
