package it.polimi.lamalfa_maioli.view;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/***
 * Classe rappresentate le impostazioni della partita, mostra menu dove il giocatore sceglie i metodi di connessione e di gioco
 * @author lamalfa_maioli
 */
public class ImpostazioniPartita implements ActionListener, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3991886659874486772L;
	private JFrame frame;
    private JPanel panel, panel1, panel2, panelType, panelRole, panelTipoGioco, panelButton;
    private ButtonGroup ruoloConnessione, tipoConnessione, tipoGioco;
    private JRadioButton radioLocalButton, radioNetButton;
    private JRadioButton radioSocketButton, radioRMIButton;
    private JRadioButton radioHostButton, radioClientButton;
    private JButton nextButton, nextButton2, backButton, backButton2, startGameButton;
    private JTextField text, text1;
    private JLabel label, label1;
    final JSpinner spinner = new JSpinner();
    public enum TipoPartita {
    	LOCALE, RMI, SOCKET;
    }
    public enum TipoConnessione {
    	CLIENT, SERVER;
    }
    private TipoPartita gameType;
    private TipoConnessione connectionType;
    private int numeroGiocatori;
    private String host;
    private int port;
    private boolean userOk = false;
   /***
    * costruttore classe, crea la prima finestra
    */
    public ImpostazioniPartita() {
    	try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			// Ignora
		} catch (InstantiationException e) {
			// Ignora
		} catch (IllegalAccessException e) {
			// Ignora
		} catch (UnsupportedLookAndFeelException e) {
			// Ignora
		}
    	frame = new JFrame("Avvio Partita");
    	frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        panel = new JPanel();
        panelTipoGioco = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panelButton = new JPanel(new FlowLayout(FlowLayout.CENTER));
        
        radioNetButton = new JRadioButton("Internet");
        radioLocalButton = new JRadioButton("Locale");
        
        nextButton = new JButton("NEXT");
        nextButton.addActionListener(this);
        
        tipoGioco = new ButtonGroup();
        tipoGioco.add(radioLocalButton);
        tipoGioco.add(radioNetButton);
      
        radioLocalButton.setSelected(false);
        radioNetButton.setSelected(true);
    
        backButton = new JButton("BACK");
        startGameButton = new JButton("CONNECT");

        panelButton.add(nextButton);
        panelTipoGioco.add(radioNetButton);
        panelTipoGioco.add(radioLocalButton);
        
        panel.add(panelTipoGioco);
        panel.add(panelButton);
               
        frame.setContentPane(panel);
	    frame = this.impostaFrame(this.frame, 340, 200, false, true);
    }
        
    /***
     * Imposta vari parametri di un frame
     * @param frame il frame dell'applicazione
     * @param altezza altezza frame
     * @param larghezza larghezza frame
     * @param resizable se modificabile nelle dimensioni
     * @param visible se visibile
     * @return il frame modificato
     */
    public final JFrame impostaFrame(JFrame frame, int altezza, int larghezza, boolean resizable, boolean visible) {
        frame.setSize(altezza,larghezza);
        frame.setResizable(resizable);
        frame.setVisible(visible);
        return frame;
    }
    
	/***
	 * prepara impostazioni avanzate di scelta di connessione
	 * @param panel1 il pannello principale su cui si dispongono gli elementi
	 * @param panelRole pannello su cui si dispongono i radioButton client/host
	 * @param panelType pannello su cui si dispongono i radioButton socket/rmi
	 * @return pannello con impostazioni avanzate di partita (client/host, rmi/socket, e ip)
	 */
    public JPanel preparaImpostazioniAvanzate(JPanel panel1, JPanel panelRole, JPanel panelType) {
        panel1 = new JPanel();
        panelRole = new JPanel(new FlowLayout(FlowLayout.CENTER));
        panelType = new JPanel(new FlowLayout(FlowLayout.CENTER));
        
        radioSocketButton = new JRadioButton("Socket");
        radioRMIButton = new JRadioButton("RMI");
        radioClientButton = new JRadioButton("Client");
        radioHostButton = new JRadioButton("Server");
        
        ruoloConnessione = new ButtonGroup();
        tipoConnessione = new ButtonGroup();
        
        label = new JLabel("Insert Server IP:    ");
        label1 = new JLabel("Insert Server Port: ");
        text = new JTextField("127.0.0.1");
        text1 = new JTextField("1337");
        
         
        text.setPreferredSize( new Dimension( 200, 24 ) );
        text1.setPreferredSize( new Dimension( 200, 24 ) );

        ruoloConnessione.add(radioHostButton);
        ruoloConnessione.add(radioClientButton);
        tipoConnessione.add(radioSocketButton);
        tipoConnessione.add(radioRMIButton);
        
        radioClientButton.setSelected(true);
        radioHostButton.setSelected(false);
        radioSocketButton.setSelected(true);
        radioRMIButton.setSelected(false);
        
        backButton.addActionListener(this);
        startGameButton.addActionListener(this);
        
        panelType.add(radioSocketButton);
        panelType.add(radioRMIButton);
        panelRole.add(radioClientButton);
        panelRole.add(radioHostButton);
        
        panel1.add(panelType);
        panel1.add(panelRole);
        panel1.add(label);
        panel1.add(text);
        panel1.add(label1);
        panel1.add(text1);
        panel1.add(backButton);
        panel1.add(startGameButton);

        return panel1;
    }
    
    /***
     * mostra cio' che viene preparato nella funzione preparaImpostazioniAvanzate
     */
    public void mostraOpzioniAvanzate() {    	
    	if(radioNetButton.isSelected()) {
    		if(panel1 == null) {
        		panel1 = this.preparaImpostazioniAvanzate(this.panel1, panelRole, panelType);
    		}
       		frame.remove(panel);
            frame.setContentPane(panel1);
    	} else {
			if(panel2 == null) {
				panel2 = this.impostaNumeroGiocatori();
			}
           	frame.remove(panel);
            frame.setContentPane(panel2);


    	}
    	
    }
    
    /***
     * mostra le opzioni del pannello di connessione socket/rmi
     */
    public void mostraOpzioniPrimarie() {
    	if(panel1 != null) {
    		frame.remove(panel1);
    	}
    	if(panel2 != null) {
    		frame.remove(panel2);
    	}
    	frame.setContentPane(panel);


    }

    /***
     * funzione invocata sul click del bottone connect, si occupa delle connessioni
     */
    public void iniziaPartita() {
        	try {
        		this.gameType = radioSocketButton.isSelected() ? TipoPartita.SOCKET : TipoPartita.RMI;
            	this.connectionType = radioClientButton.isSelected() ? TipoConnessione.CLIENT : TipoConnessione.SERVER;
            	this.host = text.getText().toString();
            	this.port = Integer.parseInt(text1.getText().trim());
            	this.userOk = true;
            	closeGui();
        	} catch(NumberFormatException e) {
        		JOptionPane.showMessageDialog(frame, "Porta non valida");
        		text1.setText("");
        	}
    }
    
    /***
     * mostra panel con selezione numero giocatori per gioco locale
     * @return pannello della selezione giocatori
     */
    public JPanel impostaNumeroGiocatori() {
       	JPanel contentPane = new JPanel();
    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);
		
		spinner.setBounds(207, 32, 29, 20);
		spinner.setValue(Integer.valueOf(2));
		spinner.addChangeListener(new ChangeListener() {      
			  @Override
			  public void stateChanged(ChangeEvent e) {
				  	int value = (Integer) spinner.getValue();
				  	if(value < 2) {
				  		spinner.setValue(Integer.valueOf(2));
				  	} else if(value > 4) {
				  		spinner.setValue(Integer.valueOf(4));
				  	}
			    
			  }
			});
		contentPane.add(spinner);
		
		JLabel lblNewLabel = new JLabel("Numero Giocatori:");
		lblNewLabel.setBounds(85, 35, 98, 14);
		contentPane.add(lblNewLabel);
		
		nextButton2 = new JButton("Avanti");
		nextButton2.setBounds(187, 98, 89, 23);
		contentPane.add(nextButton2);
		
		backButton2 = new JButton("Indietro");
		backButton2.setBounds(46, 98, 89, 23);
		contentPane.add(backButton2);
		
		nextButton2.addActionListener(this);
		backButton2.addActionListener(this);
		
		return contentPane;
    }
    
    /***
     * chiude gui, agendo sul frame
     */
    private void closeGui() {
    	frame.dispose();
    }
    
    /***
     * funzione importata dall' implements Action Listener, prepara e attiva eventi all'occorrenza di eventi sui bottoni della classe
     *@param ae � il gestore di evento, si occupa di controllare se il determinato bottone viene premuto (previa connessione bottone-gestore_evento)
     */
    public void actionPerformed(ActionEvent ae) {
        JButton button = (JButton) ae.getSource();
        
        if(button.equals(nextButton)) {
        	mostraOpzioniAvanzate();
             
        } else if(button.equals(backButton)) {
        	mostraOpzioniPrimarie();
        	
        } else if(button.equals(startGameButton)) {
        	iniziaPartita();
        	
        } else if(button.equals(backButton2)) {
        	mostraOpzioniPrimarie();
        	
        } else if(button.equals(nextButton2)) {
    		this.gameType = TipoPartita.LOCALE;
    		this.numeroGiocatori = (Integer) spinner.getValue();
    		this.userOk = true;
    		closeGui();
        }
        
        
            frame.validate();
            frame.repaint(); 
        
    } 
    
    /***
     * ritorna il numero di giocatori
     * @return il numero di giocatori
     */
    public int getNumeroGiocatori() {
    	return this.numeroGiocatori;
    }
    
    /***
     * imposta true se il giocatore e' pronto
     * @return la boolean che avvisa se il giocatore e' pronto ad agire
     */
    public boolean getUserOk() {
    	return userOk;
    }
    
    /***
     * ritorna la stringa dell'host corrente di gioco
     * @return l'host di gioco
     */
    public String getHost() {
    	return host;
    }
    
    /***
     * ritorna la porta su cui avviene la connessione di gioco
     * @return la porta di gioco
     */
    public int getPort() {
    	return port;
    }
    
    /***
     * imposta il tipo di partita
     * @return il tipo di partita che si vuole giocare
     */
    public TipoPartita getGameType() {
    	return this.gameType;
    }
    
    /***
     * imposta il tipo di connessione
     * @return il tipo di connessione
     */
    public TipoConnessione getConnectionType() {
    	return this.connectionType;
    }
}
