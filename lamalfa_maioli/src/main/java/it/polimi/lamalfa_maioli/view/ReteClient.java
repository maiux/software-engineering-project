package it.polimi.lamalfa_maioli.view;

import java.io.Serializable;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

/***
 * classe che gestisce il client sulla rete
 * @author lamalfa_maioli
 *
 */
public class ReteClient extends JFrame implements Serializable {

	private static final long serialVersionUID = 5544040944574595901L;
	private JPanel contentPane;

	/**
	 * Creail frame per la connessione
	 */
	public ReteClient(String msg) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			// Ignora
		} catch (InstantiationException e) {
			// Ignora
		} catch (IllegalAccessException e) {
			// Ignora
		} catch (UnsupportedLookAndFeelException e) {
			// Ignora
		}
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 198, 77);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblInAttesaDellavvio = new JLabel(msg);
		lblInAttesaDellavvio.setBounds(10, 11, 208, 14);
		contentPane.add(lblInAttesaDellavvio);
		setVisible(true);
	}

}
