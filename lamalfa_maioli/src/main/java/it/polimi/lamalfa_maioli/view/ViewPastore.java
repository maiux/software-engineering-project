package it.polimi.lamalfa_maioli.view;

import java.awt.event.MouseEvent;
import java.io.Serializable;

import it.polimi.lamalfa_maioli.model.Pastore;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/***
 * classe che si  occupa di creare su schermo il pastore e gestirne posizione e altra amenità,
 * se ne chiama un'istanza in Pastore.java
 * @author lamalfa_maioli
 *
 */
public class ViewPastore  implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 10936257061258081L;
	private JLabel label;
	private int labelX;
	private int labelY;
	private int altezza;
	private int larghezza;
	private Pastore pastore;
	private ViewCasella viewCasella;
	
	/***
	 * imposta label del pastore (si richiama da Pastore.java)
	 * @param contentPane pannello di gioco
	 * @param casella casella su cui e' il pastore
	 * @param x posizione ascisse
	 * @param y posizione ordinate
	 * @param alt altezza label
	 * @param lar larghezza label
	 */
	public ViewPastore(JLabel label, JPanel contentPane, ViewCasella casella, int alt, int lar, final Pastore pastore, ControllerMappa controller) {
		this.pastore = pastore;
		this.label = label;
		this.altezza = alt;
		this.larghezza = lar;
		this.viewCasella = casella;
		label.setBounds(0, 0, altezza, larghezza);
		this.spostaPastoreSuCasella(casella);
		label.setIcon(new ImageIcon(ControllerMappa.class.getResource(pastore.getGiocatore().getColore().getPath())));
		label.addMouseListener ( new MyMouseEvent (controller) {
            private static final long serialVersionUID = -4720454891571514482L;

			public void mousePressed ( MouseEvent e) {
            	this.getController().setPastorePremuto(pastore);
            }
        } );
	}
	
	/***
	 * ritorna il pastore corrente
	 * @return il pastore corrente
	 */
	public Pastore getPastore() {
		return pastore;
	}
	
	/***
	 * imposta la casella della viewpastore
	 * @param casella la casella su cui mettere il pastore
	 */
	public void setCasella(ViewCasella casella) {
		this.viewCasella = casella;
	}
	
	/***
	 * imposta il pastore corrente
	 * @param pastore il pastore corrente
	 */
	public void setPastore(Pastore pastore) {
		this.pastore = pastore;
	}
	
	 /***
	  * 
	  * @return posizione pastore ascisse
	  */
	public int getX() {
		return labelX;
	}
	
	/***
	 * 
	 * @return posizione pastore ordinate
	 */
	public int getY() {
		return labelY;
	}
	
	/***
	 * imposta casella su cui si trova il pastore
	 * @param casella la casella sulla quale si vuole il pastore
	 */
	public final void spostaPastoreSuCasella(ViewCasella casella) {
		this.viewCasella = casella;
		this.labelX = casella.getX();
		this.labelY = casella.getY();
		label.setBounds(labelX, labelY-7, altezza, larghezza);
	}
	
	/***
	 * funzione che permette spostamento pastore asse x
	 * @param increment  quanto si incrementa/decrementa posizione asse x
	 */
	public void setX(int increment) {
		labelX += increment;
		label.setBounds(labelX, labelY, 30, 30);
	}
	
	/***
	 * funzione che permette spostamento pastore asse y
	 * @param increment quanto si incrementa/decrementa posizione asse y
	 */
	public void setY(int increment) {
		labelY += increment;
		label.setBounds(labelX, labelY, 30, 30);
	}
	
	/***
	 * ritorna la viewcasella del pastore
	 * @return la viewcasella del pastore corrente
	 */
	public ViewCasella getViewCasella() {
		return viewCasella;
	}
	
}
