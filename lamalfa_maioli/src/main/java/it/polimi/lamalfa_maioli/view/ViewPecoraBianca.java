package it.polimi.lamalfa_maioli.view;

import java.awt.Color;
import java.awt.Font;
import java.io.Serializable;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/***
 * classe che gestisce a schermo la visualizzazione delle pecore bianche
 * @author lamalfa_maioli
 *
 */
public class ViewPecoraBianca implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3534209357608258687L;
	private ViewRegione viewRegione;
	private JLabel labelPecoraBianca;
	private JLabel labelNumPecoreBianche;	
	private int labelX;
	private int labelY;
	private int altezza;
	private int larghezza;
	
	/***
	 * costruttore PecoraBianca, inizializza l'immagine sulla mappa
	 * @param contentPane il pannello della mappa
	 * @param viewReg la regione su cui e' la pecora
	 * @param x coordinata ascisse
	 * @param y coordinata ordinate
	 * @param alt grandezza label Pecora
	 * @param lar larghezza label Pecora
	 */
	public ViewPecoraBianca(JPanel contentPane, ViewRegione viewReg, int x, int y, int alt,int lar) {
		this.labelPecoraBianca = new JLabel(" ");
		this.labelNumPecoreBianche = new JLabel();
		this.viewRegione = viewReg;
		this.labelX = x;
		this.labelY = y;
		this.altezza = alt;
		this.larghezza = lar;
		this.labelPecoraBianca.setBounds(labelX, labelY, altezza, larghezza);
		this.labelNumPecoreBianche.setForeground(Color.BLACK);
		this.labelNumPecoreBianche.setFont(new Font("Tahoma", Font.BOLD, 12));
		this.labelNumPecoreBianche.setBounds(labelX, labelY, 10, 10);
		this.labelPecoraBianca.setIcon(new ImageIcon(ControllerMappa.class.getResource("/it/polimi/lamalfa_maioli/view/pecora.png")));
		this.update();
		contentPane.add(labelNumPecoreBianche);
		contentPane.add(labelPecoraBianca);

	}
	
	/***
	 * imposta il parametro che consente poi di stampare a schermo il numero di pecore bianche
	 * @param num il numero di pecore bianche
	 */
	public final void update() {
		String tmp = String.format("%d", viewRegione.getRegione().getPecore());
		this.labelNumPecoreBianche.setText(tmp);
	}
		
	/***
	 * 
	 * @return coordinata x della label
	 */
	public int getX() {
		return labelX;
	}
		
	/***
	 * 
	 * @return coordinata y della label
	 */
	public int getY() {
		return labelY;
	}
		
	/***
	 * funzione che permette spostamento pecoraBianca asse x
	 * @param increment  quanto si incrementa/decrementa posizione asse x
	 */
	public void setX(int increment) {
		labelX += increment;
		labelPecoraBianca.setBounds(labelX, labelY, 30, 30);
	}
	
	/***
	 * funzione che permette spostamento pecoraBianca asse y
	 * @param increment quanto si incrementa/decrementa posizione asse y
	 */
	public void setY(int increment) {
		labelY += increment;
		labelPecoraBianca.setBounds(labelX, labelY, 30, 30);
	}

}
