package it.polimi.lamalfa_maioli.view;

import it.polimi.lamalfa_maioli.controller.GameCore;
import it.polimi.lamalfa_maioli.controller.exceptions.MyRuntimeException;
import it.polimi.lamalfa_maioli.controller.rete.rmi.ServerInterface;
import it.polimi.lamalfa_maioli.model.Pastore;
import it.polimi.lamalfa_maioli.model.Terreno;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/***
 * classe per gestire la vendita di tessere terrno da parte del giocatore
 * @author lamalfa_maioli
 *
 */
public class CompravenditaVendi extends JFrame implements ActionListener, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3002672610099106L;
	private JPanel contentPane;
	private JButton sellButton = new JButton("Vendi");
	private JButton lblTerreno1, lblTerreno2, lblTerreno3, lblTerreno4, lblTerreno5, lblTerreno6;
	private Map<Terreno, JSpinner> mapQuantita = new HashMap<>();
	private Map<Terreno, JSpinner> mapPrezzo = new HashMap<>(); 
	private GameCore core;
	private String alert = "";
	private boolean end = false;
	private ServerInterface server = null;
	
	/***
	 * costruttore che inizializza panel vendita
	 * @param core
	 * @param server
	 */
	public CompravenditaVendi(GameCore core, ServerInterface server) {
		this.core = core;
		this.server = server;
		final Map<Terreno, Integer> quantitaMax = core.getStatus().getGiocatoreCorrente().getTerreniComprati();
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 479, 417);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.contentPane.setLayout(null);
		
		JLabel lblCosto1 = new JLabel("Quantita:");
		lblCosto1.setBounds(61, 131, 46, 14);
		contentPane.add(lblCosto1);
		
		JLabel lblNumero = new JLabel("Prezzo:");
		lblNumero.setBounds(61, 159, 46, 14);
		contentPane.add(lblNumero);
		
		JLabel lblGiocatoreI = new JLabel("Seleziona le tessere da vendere");
		lblGiocatoreI.setFont(new Font("Tahoma", Font.ITALIC, 12));
		lblGiocatoreI.setBounds(143, 11, 175, 14);
		contentPane.add(lblGiocatoreI);
		
		lblTerreno1 = new JButton("Terreno1");
		lblTerreno1.setIcon(new ImageIcon(CompravenditaVendi.class.getResource("/it/polimi/lamalfa_maioli/view/tesseraTerreno1.png")));
		lblTerreno1.setBounds(61, 40, 80, 80);
		lblTerreno1.addActionListener(this);
		contentPane.add(lblTerreno1);
		
		lblTerreno2 = new JButton("Terreno2");
		lblTerreno2.setIcon(new ImageIcon(CompravenditaVendi.class.getResource("/it/polimi/lamalfa_maioli/view/tesseraTerreno2.png")));
		lblTerreno2.setBounds(188, 40, 80, 80);
		lblTerreno2.addActionListener(this);
		contentPane.add(lblTerreno2);
		
		lblTerreno3 = new JButton("Terreno3");
		lblTerreno3.setIcon(new ImageIcon(CompravenditaVendi.class.getResource("/it/polimi/lamalfa_maioli/view/tesseraTerreno3.png")));
		lblTerreno3.setBounds(315, 40, 80, 80);
		lblTerreno3.addActionListener(this);
		contentPane.add(lblTerreno3);
		
		lblTerreno4 = new JButton("Terreno4");
		lblTerreno4.setIcon(new ImageIcon(CompravenditaVendi.class.getResource("/it/polimi/lamalfa_maioli/view/tesseraTerreno4.png")));
		lblTerreno4.setBounds(61, 189, 80, 80);
		lblTerreno4.addActionListener(this);
		contentPane.add(lblTerreno4);
		
		lblTerreno5 = new JButton("Terreno5");
		lblTerreno5.setIcon(new ImageIcon(CompravenditaVendi.class.getResource("/it/polimi/lamalfa_maioli/view/tesseraTerreno5.png")));
		lblTerreno5.setBounds(188, 189, 80, 80);
		lblTerreno5.addActionListener(this);
		contentPane.add(lblTerreno5);
		
		lblTerreno6 = new JButton("Terreno6");
		lblTerreno6.setIcon(new ImageIcon(CompravenditaVendi.class.getResource("/it/polimi/lamalfa_maioli/view/tesseraTerreno6.png")));
		lblTerreno6.setBounds(315, 189, 80, 80);
		lblTerreno6.addActionListener(this);
		contentPane.add(lblTerreno6);
		
		sellButton.setBounds(366, 344, 89, 23);
		sellButton.addActionListener(this);
		contentPane.add(sellButton);
		
		setContentPane(this.contentPane);
		
		final JSpinner spinner1 = new JSpinner();
		spinner1.setBounds(112, 128, 29, 20);
		spinner1.addChangeListener(new ChangeListener() {      
			  @Override
			  public void stateChanged(ChangeEvent e) {
				  	int value = (Integer) spinner1.getValue();
				  	int max = quantitaMax.get(Terreno.PASCOLO);
				  	if(value < 0) {
				  		spinner1.setValue(Integer.valueOf(0));
				  	} else if(value > max) {
				  		spinner1.setValue(Integer.valueOf(max));
				  	}		    
			  }
		});
		contentPane.add(spinner1);
		mapQuantita.put(Terreno.PASCOLO, spinner1);
		
		final JSpinner spinner2 = new JSpinner();
		spinner2.setValue(1);
		spinner2.setBounds(112, 156, 29, 20);
		spinner2.addChangeListener(new ChangeListener() {      
			  @Override
			  public void stateChanged(ChangeEvent e) {
				  	int value = (Integer) spinner2.getValue();
				  	if(value < 1) {
				  		spinner2.setValue(Integer.valueOf(1));
				  	} else if(value > 4) {
				  		spinner2.setValue(Integer.valueOf(4));
				  	}		    
			  }
		});
		contentPane.add(spinner2);
		mapPrezzo.put(Terreno.PASCOLO, spinner2);
		
		JLabel label = new JLabel("Quantita:");
		label.setBounds(188, 128, 46, 14);
		contentPane.add(label);
		
		final JSpinner spinner3 = new JSpinner();
		spinner3.setBounds(239, 125, 29, 20);
		spinner3.addChangeListener(new ChangeListener() {      
			  @Override
			  public void stateChanged(ChangeEvent e) {
				  	int value = (Integer) spinner3.getValue();
				  	int max = quantitaMax.get(Terreno.FORESTA);
				  	if(value < 0) {
				  		spinner3.setValue(Integer.valueOf(0));
				  	} else if(value > max) {
				  		spinner3.setValue(Integer.valueOf(max));
				  	}		    
			  }
		});
		contentPane.add(spinner3);
		mapQuantita.put(Terreno.FORESTA, spinner3);
		
		JLabel label1 = new JLabel("Prezzo:");
		label1.setBounds(188, 156, 46, 14);
		contentPane.add(label1);
		
		final JSpinner spinner4 = new JSpinner();
		spinner4.setValue(1);
		spinner4.setBounds(239, 153, 29, 20);
		contentPane.add(spinner4);
		spinner4.addChangeListener(new ChangeListener() {      
			  @Override
			  public void stateChanged(ChangeEvent e) {
				  	int value = (Integer) spinner4.getValue();
				  	if(value < 1) {
				  		spinner4.setValue(Integer.valueOf(1));
				  	} else if(value > 4) {
				  		spinner4.setValue(Integer.valueOf(4));
				  	}		    
			  }
		});
		mapPrezzo.put(Terreno.FORESTA, spinner4);
		
		JLabel label2 = new JLabel("Quantita:");
		label2.setBounds(315, 128, 46, 14);
		contentPane.add(label2);
		
		final JSpinner spinner5 = new JSpinner();
		spinner5.setBounds(366, 125, 29, 20);
		spinner5.addChangeListener(new ChangeListener() {      
			  @Override
			  public void stateChanged(ChangeEvent e) {
				  	int value = (Integer) spinner5.getValue();
				  	int max = quantitaMax.get(Terreno.PALUDE);
				  	if(value < 0) {
				  		spinner5.setValue(Integer.valueOf(0));
				  	} else if(value > max) {
				  		spinner5.setValue(Integer.valueOf(max));
				  	}		    
			  }
		});
		contentPane.add(spinner5);
		mapQuantita.put(Terreno.PALUDE, spinner5);
		
		JLabel label3 = new JLabel("Prezzo:");
		label3.setBounds(315, 156, 46, 14);
		contentPane.add(label3);
		
		final JSpinner spinner6 = new JSpinner();
		spinner6.setValue(1);
		spinner6.setBounds(366, 153, 29, 20);
		spinner6.addChangeListener(new ChangeListener() {      
			  @Override
			  public void stateChanged(ChangeEvent e) {
				  	int value = (Integer) spinner6.getValue();
				  	if(value < 1) {
				  		spinner6.setValue(Integer.valueOf(1));
				  	} else if(value > 4) {
				  		spinner6.setValue(Integer.valueOf(4));
				  	}		    
			  }
		});
		contentPane.add(spinner6);
		mapPrezzo.put(Terreno.PALUDE, spinner6);
		
		JLabel label4 = new JLabel("Quantita:");
		label4.setBounds(61, 283, 46, 14);
		contentPane.add(label4);
		
		final JSpinner spinner7 = new JSpinner();
		spinner7.setBounds(112, 280, 29, 20);
		spinner7.addChangeListener(new ChangeListener() {      
			  @Override
			  public void stateChanged(ChangeEvent e) {
				  	int value = (Integer) spinner7.getValue();
				  	int max = quantitaMax.get(Terreno.COLLINA);
				  	if(value < 0) {
				  		spinner7.setValue(Integer.valueOf(0));
				  	} else if(value > max) {
				  		spinner7.setValue(Integer.valueOf(max));
				  	}		    
			  }
		});
		contentPane.add(spinner7);
		mapQuantita.put(Terreno.COLLINA, spinner7);
		
		JLabel label5 = new JLabel("Prezzo:");
		label5.setBounds(61, 311, 46, 14);
		contentPane.add(label5);
		
		final JSpinner spinner8 = new JSpinner();
		spinner8.setValue(1);
		spinner8.setBounds(112, 308, 29, 20);
		contentPane.add(spinner8);
		spinner8.addChangeListener(new ChangeListener() {      
			  @Override
			  public void stateChanged(ChangeEvent e) {
				  	int value = (Integer) spinner8.getValue();
				  	if(value < 1) {
				  		spinner8.setValue(Integer.valueOf(1));
				  	} else if(value > 4) {
				  		spinner8.setValue(Integer.valueOf(4));
				  	}		    
			  }
		});
		mapPrezzo.put(Terreno.COLLINA, spinner8);
		
		JLabel label6 = new JLabel("Quantita:");
		label6.setBounds(188, 283, 46, 14);
		contentPane.add(label6);
		
		final JSpinner spinner9 = new JSpinner();
		spinner9.setBounds(239, 280, 29, 20);
		spinner9.addChangeListener(new ChangeListener() {      
			  @Override
			  public void stateChanged(ChangeEvent e) {
				  	int value = (Integer) spinner9.getValue();
				  	int max = quantitaMax.get(Terreno.SPIAGGIA);
				  	if(value < 0) {
				  		spinner9.setValue(Integer.valueOf(0));
				  	} else if(value > max) {
				  		spinner9.setValue(Integer.valueOf(max));
				  	}		    
			  }
		});
		contentPane.add(spinner9);
		mapQuantita.put(Terreno.SPIAGGIA, spinner9);
		
		JLabel label7 = new JLabel("Prezzo:");
		label7.setBounds(188, 311, 46, 14);
		contentPane.add(label7);
		
		final JSpinner spinner10 = new JSpinner();
		spinner10.setValue(1);
		spinner10.setBounds(239, 308, 29, 20);
		contentPane.add(spinner10);
		spinner10.addChangeListener(new ChangeListener() {      
			  @Override
			  public void stateChanged(ChangeEvent e) {
				  	int value = (Integer) spinner10.getValue();
				  	if(value < 1) {
				  		spinner10.setValue(Integer.valueOf(1));
				  	} else if(value > 4) {
				  		spinner10.setValue(Integer.valueOf(4));
				  	}		    
			  }
		});
		mapPrezzo.put(Terreno.SPIAGGIA, spinner10);
		
		JLabel label8 = new JLabel("Quantita:");
		label8.setBounds(315, 283, 46, 14);
		contentPane.add(label8);
		
		final JSpinner spinner11 = new JSpinner();
		spinner11.setBounds(366, 280, 29, 20);
		spinner11.addChangeListener(new ChangeListener() {      
			  @Override
			  public void stateChanged(ChangeEvent e) {
				  	int value = (Integer) spinner11.getValue();
				  	int max = quantitaMax.get(Terreno.MONTAGNA);
				  	if(value < 0) {
				  		spinner11.setValue(Integer.valueOf(0));
				  	} else if(value > max) {
				  		spinner11.setValue(Integer.valueOf(max));
				  	}		    
			  }
		});
		contentPane.add(spinner11);
		mapQuantita.put(Terreno.MONTAGNA, spinner11);
		
		JLabel label9 = new JLabel("Prezzo:");
		label9.setBounds(315, 311, 46, 14);
		contentPane.add(label9);
		
		final JSpinner spinner12 = new JSpinner();
		spinner12.setValue(1);
		spinner12.setBounds(366, 308, 29, 20);
		spinner12.addChangeListener(new ChangeListener() {      
			  @Override
			  public void stateChanged(ChangeEvent e) {
				  	int value = (Integer) spinner12.getValue();
				  	if(value < 1) {
				  		spinner12.setValue(Integer.valueOf(1));
				  	} else if(value > 4) {
				  		spinner12.setValue(Integer.valueOf(4));
				  	}		    
			  }
		});
		contentPane.add(spinner12);
		mapPrezzo.put(Terreno.MONTAGNA, spinner12);
		
		this.setVisible(true);
		this.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		        end = true;
		    }
		});
	}
	
	 /***
     * funzione importata dall' implements Action Listener, prepara e attiva eventi all'occorrenza di eventi sui bottoni della classe
     *@param ae  il gestore di evento, si occupa di controllare se il determinato bottone viene premuto (previa connessione bottone-gestore_evento)
     */
    public void actionPerformed(ActionEvent ae) {
        JButton button = (JButton) ae.getSource();
        if(button.equals(sellButton)) {
        	Pastore corrente = core.getStatus().getGiocatoreCorrente().getPastoreCorrente();
        	alert = "Status vendita:\n";
        	for(int i = 0; i < Terreno.values().length-1; i++) {
        		Terreno tmp = Terreno.values()[i];
        		int quantita = (int) mapQuantita.get(tmp).getValue();
        		int prezzo = (int) mapPrezzo.get(tmp).getValue();
        		if(quantita > 0) {
        			boolean mov;
        			if(server != null) {
        				try {
							mov = server.vendi(corrente, tmp, quantita, prezzo);
						} catch (RemoteException e) {
							throw new MyRuntimeException(e);
						}
        			} else {
        				mov = core.vendi(corrente, tmp, quantita, prezzo);
        			}
        			if(mov) {
        				alert +=  quantita+" casell"+(quantita == 1 ? "a" : "e")+" di tipo "+tmp.toString()+" messe in vendita!\n";
        			} else {
        				alert += "Impossibile mettere in vendita "+quantita+" casell"+(quantita == 1 ? "a" : "e")+" di tipo "+tmp.toString();
        			}
        		}
        	}
        	end = true;
        	this.dispose();        	
        }
    }
    
	/**
	 * @return se le operazioni di vendita sono state terminate
	 */
	public boolean getEnd() {
		return this.end;
	}
	
	
	/**
	 * @return una stringa contenente il risultato delle operazioni di vendita
	 */
	public String getStatus() {
		return this.alert;
	}
}
