package it.polimi.lamalfa_maioli.model;

import java.io.Serializable;

/**
 * Extra rule: compravendita
 * Classe rappresentante l'annuncio di vendita di una tipologia di terreno.
 * Vanno specificata la quantità e il prezzo per unità.
 * E' associata direttamente al giocatore.
 * @author lamalfa_maioli
 *
 */
public class OggettoVendita implements Serializable {
	private static final long serialVersionUID = 5394509756170982098L;
	private Terreno tipoTerreno;
	private int quantita;
	private int prezzo;
	private Giocatore giocatore;
	
	/**
	 * Costruttore
	 * @param tipoTerreno tipologia del terreno
	 * @param quantita quantità di terreni di questo tipo da vendere
	 * @param prezzo prezzo per unità di terreno
	 */
	public OggettoVendita(Giocatore giocatore, Terreno tipoTerreno, int quantita, int prezzo) {
		this.giocatore = giocatore;
		this.tipoTerreno = tipoTerreno;
		this.quantita = quantita;
		this.prezzo = prezzo;
	}
	
	/**
	 * @return giocatore associato a questa vendita
	 */
	public Giocatore getGiocatore() {
		return giocatore;
	}

	/**
	 * @return tipo del terreno in vendita
	 */
	public Terreno getTipoTerreno() {
		return tipoTerreno;
	}
	
	/**
	 * @return quantità dei terreni del tipo corrente in vendità
	 */
	public int getQuantita() {
		return quantita;
	}

	/**
	 * Imposta la quantità di terreni in vendita.
	 * @param quantita quantità di terreni in vendita
	 */
	public void setQuantita(int quantita) {
		this.quantita = quantita;
	}

	/**
	 * @return prezzo di vendita per singola unità
	 */
	public int getPrezzo() {
		return prezzo;
	}

	/**
	 * Imposta il prezzo di un'unità di terreno
	 * @param prezzo prezzo per unità del terreno da vendere
	 */
	public void setPrezzo(int prezzo) {
		this.prezzo = prezzo;
	}

	
}