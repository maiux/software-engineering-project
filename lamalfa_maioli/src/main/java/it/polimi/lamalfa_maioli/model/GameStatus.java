package it.polimi.lamalfa_maioli.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe rappresentante lo stato della partita
 * @author lamalfa_maioli
 *
 */
public class GameStatus implements Serializable {
	private static final long serialVersionUID = 8379183772122520286L;
	private final List<Giocatore> giocatori = new ArrayList<Giocatore>();
	private final List<Agnello> agnelli = new ArrayList<Agnello>();
	private Giocatore giocatoreIniziale;
	private int locker = 0;
	private Giocatore giocatoreCorrente;
	private int turno = 0;
	private Regione posizionePecoraNera;
	private boolean ultimoGiro = false;
	private int numeroRecinti = 20;
	private final GraphXML mappa;
	private Regione posizioneLupo;
	private final List<OggettoVendita> compravendita = new ArrayList<>();
	private int numeroGiocatori = 0;
	private boolean gameEnd = false;
	
	/**
	 * Costruttore: inizializza lo stato della partita
	 * @param mappa mappa xml generata
	 */
	public GameStatus(GraphXML mappa) {
		this.mappa = mappa;
	}
	
	/**
	 * @return se il gioco è finito
	 */
	public boolean gameEnd() {
		return gameEnd;
	}
	
	/**
	 * Imposta la fine del gioco	
	 * @param status se il gioco è finito
	 */
	public void setGameEnd(Boolean status) {
		this.gameEnd = status;
	}
	
	/**
	 * @return il giocatore successivo
	 */
	public Giocatore prossimoGiocatore() {
		int i = giocatori.indexOf(giocatoreCorrente);
		if(i >= giocatori.size() - 1) {
			i = 0;
		} else {
			i++;
		}
		return giocatori.get(i);
	}
	
	/**
	 * Imposta il numero dei giocatori per l'inizializzazione (prima della popolazione di giocatori)
	 * @param numeroGiocatori numero dei giocatori della partita
	 */
	public void setNumeroGiocatori(int numeroGiocatori) {
		this.numeroGiocatori = numeroGiocatori;
	}
	
	/**
	 * Serve per l'inizializzazione (prima di popolare giocatori)
	 * @return numero dei giocatori
	 */
	public int getNumeroGiocatori() {
		return numeroGiocatori;
	}
	
	/**
	 * @return ArrayList dei giocatori
	 */
	public List<Giocatore> getGiocatori() {
		return giocatori;
	}

	/**
	 * @return ArrayList degli agnelli
	 */
	public List<Agnello> getAgnelli() {
		return agnelli;
	}
	
	/**
	 * @return giocatore iniziale
	 */
	public Giocatore getGiocatoreIniziale() {
		return giocatoreIniziale;
	}

	/**
	 * Imposta il giocatore iniziale
	 * @param giocatoreIniziale giocatore iniziale
	 */
	public void setGiocatoreIniziale(Giocatore giocatoreIniziale) {
		if(locker == 0) {
			for(int i = 0; i < giocatori.size(); i++) {
				if(giocatori.get(i).equals(giocatoreIniziale)) {
					this.giocatoreIniziale = giocatori.get(i);
					this.giocatoreCorrente = giocatori.get(i);
					locker++;
					break;
				}
			}
		}
	}

	/**
	 * @return giocatore corrente
	 */
	public Giocatore getGiocatoreCorrente() {
		return giocatoreCorrente;
	}

	/**
	 * Imposta il giocatore corrente
	 * @param giocatoreCorrente giocatore corrente
	 */
	public void setGiocatoreCorrente(Giocatore giocatoreCorrente) {
		for(int i = 0; i < giocatori.size(); i++) {
			if(giocatori.get(i).equals(giocatoreCorrente)) {
				this.giocatoreCorrente = giocatori.get(i);
				break;
			}
		}
	}

	/**
	 * @return il numero del turno corrente
	 */
	public int getTurno() {
		return turno;
	}

	/**
	 * Imposta il numero del turno corrente
	 * @param turno numero del turno corrente
	 */
	public void setTurno(int turno) {
		this.turno = turno;
	}

	/**
	 * @return la regione in cui è la pecora nera
	 */
	public Regione getPosizionePecoraNera() {
		return posizionePecoraNera;
	}

	/**
	 * Imposta la regione in cui è la pecora nera
	 * @param posizionePecoraNera regione in cui è la pecora nera
	 */
	public void setPosizionePecoraNera(Regione posizionePecoraNera) {
		this.posizionePecoraNera = (Regione) mappa.trovaRegione(posizionePecoraNera.getId());
	}

	/**
	 * @return se è arrivato il turno finale
	 */
	public boolean isUltimoGiro() {
		return ultimoGiro;
	}

	/**
	 * Imposta se è arrivato il turno finale
	 * @param ultimoGiro se è arrivato il turno finale
	 */
	public void setUltimoGiro(boolean ultimoGiro) {
		this.ultimoGiro = ultimoGiro;
	}

	/**
	 * @return il numero di recinti totali usati
	 */
	public int getNumeroRecinti() {
		return numeroRecinti;
	}

	/**
	 * Imposta il numero di recinti totali usati
	 * @param numeroRecinti numero di recinti totali usati
	 */
	public void setNumeroRecinti(int numeroRecinti) {
		this.numeroRecinti = numeroRecinti;
	}

	/**
	 * @return la mappa
	 */
	public GraphXML getMappa() {
		return mappa;
	}

	/**
	 * @return la regione in cui è presente il lupo
	 */
	public Regione getPosizioneLupo() {
		return posizioneLupo;
	}

	/**
	 * Imposta la regione in cui è presente il lupo
	 * @param posizioneLupo regione in cui è presente il lupo
	 */
	public void setPosizioneLupo(Regione posizioneLupo) {
		this.posizioneLupo = (Regione) mappa.trovaRegione(posizioneLupo.getId());
	}

	/**
	 * @return ArrayList della compravendita
	 */
	public List<OggettoVendita> getCompravendita() {
		return compravendita;
	}

	/**
	 * @see Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((agnelli == null) ? 0 : agnelli.hashCode());
		result = prime * result + ((compravendita == null) ? 0 : compravendita.hashCode());
		result = prime * result	+ ((giocatoreCorrente == null) ? 0 : giocatoreCorrente.hashCode());
		result = prime * result + ((giocatoreIniziale == null) ? 0 : giocatoreIniziale.hashCode());
		result = prime * result	+ ((giocatori == null) ? 0 : giocatori.hashCode());
		result = prime * result + locker;
		result = prime * result + ((mappa == null) ? 0 : mappa.hashCode());
		result = prime * result + numeroGiocatori;
		result = prime * result + numeroRecinti;
		result = prime * result	+ ((posizioneLupo == null) ? 0 : posizioneLupo.hashCode());
		result = prime * result	+ ((posizionePecoraNera == null) ? 0 : posizionePecoraNera.hashCode());
		result = prime * result + turno;
		result = prime * result + (ultimoGiro ? 1231 : 1237);
		return result;
	}

	/**
	 * @see Object#equals(Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		GameStatus other = (GameStatus) obj;
		if (!agnelli.equals(other.agnelli)) {
			return false;
		}
		if (!compravendita.equals(other.compravendita)) {
			return false;
		}
		if (giocatoreCorrente == null) {
			if(other.getGiocatoreCorrente() != null) {
				return false;
			}
		} else if (!giocatoreCorrente.equals(other.getGiocatoreCorrente())) {
			return false;
		}
		if (giocatoreIniziale == null) {
			if(other.getGiocatoreIniziale() != null) {
				return false;
			}
		} else if (!giocatoreIniziale.equals(other.getGiocatoreIniziale())) {
			return false;
		}
		if (!giocatori.equals(other.getGiocatori())) {
			return false;
		}
		if (!mappa.getGraph().equals(other.getMappa().getGraph())) {
			return false;
		}
		if (numeroGiocatori != other.getNumeroGiocatori()) {
			return false;
		}
		if (numeroRecinti != other.getNumeroRecinti()) {
			return false;
		}
		if (posizioneLupo == null) {
			if(other.getPosizioneLupo() != null) {
				return false;
			}
		} else if (!posizioneLupo.equals(other.getPosizioneLupo())) {
			return false;
		}
		if (posizionePecoraNera == null) {
			if(other.getPosizionePecoraNera() != null) {
				return false;
			}
		} else if (!posizionePecoraNera.equals(other.getPosizionePecoraNera())) {
			return false;
		}
		if (turno != other.getTurno()) {
			return false;
		}
		if (ultimoGiro != other.isUltimoGiro()) {
			return false;
		}
		return true;
	}
}
