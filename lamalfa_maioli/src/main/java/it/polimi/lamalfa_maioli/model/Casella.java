package it.polimi.lamalfa_maioli.model;

/**
 * Classe rappresentante le caselle del gioco.
 * E' una tipologia di vertice del grafo.
 * @author lamalfa_maioli
 *
 */
public class Casella implements Vertex {
	private static final long serialVersionUID = -214573643349782999L;
	private int id;
	private int value;
	private boolean recinto = false;
	private Pastore pastore = null;
	
	/**
	 * Costruttore
	 * @param id identificativo univoco della casella
	 * @param value valore della casella
	 */
	public Casella(int id, int value) {
		this.id = id;
		this.value = value;
	}
	
	/**
	 * @return identificativo univoco della casella
	 */
	public int getId() {
		return this.id;
	}
	
	/**
	 * @return valore della casella
	 */
	public int getValue() {
		return this.value;
	}
	
	/**
	 * @see Object#toString()
	 */
	public String toString() {
		return "Casella("+id+"): "+value;
	}

	/**
	 * @see Vertex#getClassString()
	 */
	@Override
	public String getClassString() {
		return "Casella";
	}
	
	/**
	 * @return se la casella ha un recinto
	 */
	public boolean hasRecinto() {
		return recinto;
	}

	/**
	 * Imposta la presenza di un recinto sulla casella
	 * @param recinto presenza del recinto
	 */
	public void setRecinto(boolean recinto) {
		this.recinto = recinto;
	}
	
	/**
	 * @return se sulla casella c'è un pastore
	 */
	public boolean hasPastore() {
		return pastore != null;
	}

	/**
	 * Imposta la presenza di un pastore sulla casella
	 * @param pastore presenza del pastore
	 */
	public void setPastore(Pastore pastore) {
		this.pastore = pastore;
	}
	
	/**
	 * @return pastore posizionato sulla casella corrente
	 */
	public Pastore getPastore() {
		return pastore;
	}
	
	/**
	 * @see Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return 2337+id;
	}

	/**
	 * @see Object#equals()
	 */
	@Override
	public boolean equals(Object v) {
		if(getClass() != v.getClass()) {
			return false;
		}
		Casella c = (Casella)v;
		if(c.getId() != id) {
			return false;
		}
		return true;
	}
}
