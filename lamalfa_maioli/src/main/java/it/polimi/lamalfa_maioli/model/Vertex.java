package it.polimi.lamalfa_maioli.model;

import java.io.Serializable;

/**
 * Interfaccia rappresentante il vertice generico del grafo.
 * @author lamalfa_maioli
 *
 */
public interface Vertex extends Serializable {
	/**
	 * Utilizzata per distinguere il tipo di vertice.
	 * @return tipo di vertice
	 */
	String getClassString();
	
	/**
	 * @see Object#equals()
	 */
	boolean equals(Object r);
	
	/**
	 * @see Object#hashCode()
	 */
	public int hashCode();
}
