package it.polimi.lamalfa_maioli.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Enum dei terreni.
 * Contiene tutte le tipologie dei terreni, indicandone l'identificativo e il relativo costo.
 * @author lamalfa_maioli
 *
 */
public enum Terreno implements Serializable {
	PASCOLO(0), FORESTA(1), PALUDE(2), SPIAGGIA(3), MONTAGNA(4), COLLINA(5), SHEEPSBURG(6);
	
	private int id;
	private int costo;
	/**
	 * Costruttore
	 * @param id identificativo univoco del terreno
	 */
	private Terreno(int id) {
		this.id = id;
		this.costo = (id == 6) ? -1 : 1;
	}
	
	/**
	 * @return identificativo univoco del terreno
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Il costo del terreno varia a seconda di quanti terreni sono stati comprati di quella tipologia.
	 * @return costo corrente del terreno
	 */
	public int getCosto() {
		if(id == 6) {
			return -1;
		}
		return costo;
	}
	
	/**
	 * Il costo del terreno varia a seconda di quanti terreni sono stati comprati di quella tipologia.
	 * @param costo nuovo costo del terreno
	 */
	public void setCosto(int costo) {
		this.costo = costo;
	}
	
	/**
	 * @return arraylist di tutti i terreni, messi in ordine sparso
	 */
	public static List<Terreno> getRandoms() {
		List<Terreno> tmp = new ArrayList<>();
		List<Terreno> random = new ArrayList<>();
		int max = values().length-1;
		for(int i = 0; i < max; i++) {
			tmp.add(values()[i]);
		}
		int i = max;
		while(i > 0) {
			int rnd = (int)(Math.random()*i);
			Terreno current = tmp.get(rnd);
			random.add(current);
			tmp.remove(current);
			i--;
		}
		return random;
	}
	
	/**
	 * @return un terreno a caso
	 */
	public static Terreno getRandom() {
		return values()[(int) (Math.random() * values().length)];
	}
}
