package it.polimi.lamalfa_maioli.model;

import java.io.Serializable;

/**
 * Enum contenente la definizione della sintassi da usare nella mappa in xml
 * @author lamalfa_maioli
 *
 */
public enum TipoXML implements Serializable {
	CASELLA, REGIONE, CONNESSIONE;
	
	/**
	 * @return nome del tag container del tipo corrispondente
	 */
	public String getContainerName() {
		switch(this) {
			case CASELLA:
				return "caselle";
			case CONNESSIONE:
				return "connessioni";
			case REGIONE:
				return "regioni";
			default:
				return "";
		}
	}
	
	/**
	 * @return nome del tag elemento del tipo corrispondente
	 */
	public String getElementName() {
		switch(this) {
			case CASELLA:
				return "casella";
			case CONNESSIONE:
				return "connessione";
			case REGIONE:
				return "regione";
			default:
				return "";
		}
	}
	
	/**
	 * @return Array dei nomi ordinati degli attributi del tipo corrispondente
	 */
	public String[] getElementData() {
		switch(this) {
			case CASELLA:
				return new String[] { "id", "value" };
			case CONNESSIONE:
				return new String[] { "el", "id", "type", "casella", "regione" };
			case REGIONE:
				return new String[] { "id", "type" };
			default:
				return new String[0];
		}
	}
}