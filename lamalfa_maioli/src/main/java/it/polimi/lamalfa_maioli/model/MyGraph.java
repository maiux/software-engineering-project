package it.polimi.lamalfa_maioli.model;

import java.io.Serializable;

import org.jgrapht.graph.SimpleGraph;

/**
 * Classe per assicurarsi che il grafo di jgrapht sia serializzabile
 * @author lamalfa_maioli
 *
 * @param <V> Vertex
 * @param <E> Edge
 */
public class MyGraph<V, E> extends SimpleGraph<V, E> implements Serializable {

	private static final long serialVersionUID = -6993234269823103175L;

	@SuppressWarnings("unchecked")
	public MyGraph() {
		super((Class<? extends E>) Arco.class);
	}
	
}
