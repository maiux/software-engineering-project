package it.polimi.lamalfa_maioli.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Enum dei colori scelti dai giocatori
 * @author lamalfa_maioli
 *
 */
public enum Colore implements Serializable { 
	ROSSO("btrn_red.png"), BLU("btrn_blue.png"), VERDE("btrn_green.png"), GIALLO("btrn_yellow.png");
	public static Colore getRandom() {
		return values()[(int) (Math.random() * values().length)];
	}
	
	String path;
	
	/**
	 * Imposta il percorso dell'immagine del pastore del colore selezionato
	 * @param path
	 */
	private Colore(String path) {
		this.path = "/it/polimi/lamalfa_maioli/view/"+path;
	}
	
	/**
	 * @return il percorso dell'immagine associata al pastore
	 */
	public String getPath() {
		return path;
	}
	
	/**
	 * @return arraylist di tutti i colori, messi in ordine sparso
	 */
	public static List<Colore> getRandoms() {
		List<Colore> tmp = new ArrayList<>();
		List<Colore> random = new ArrayList<>();
		for(int i = 0; i < 4; i++) {
			tmp.add(values()[i]);
		}
		int i = 4;
		while(i > 0) {
			int rnd = (int)(Math.random()*i);
			Colore current = tmp.get(rnd);
			random.add(current);
			tmp.remove(current);
			i--;
		}
		return random;
	}
	
	/**
	 * Restituisce un colore a partire da una stringa
	 * @param colore stringa rappresentante un colore
	 * @return colore
	 */
	public static Colore getColore(String colore) {
		switch(colore) {
			case "ROSSO":
				return Colore.ROSSO;
			case "BLU":
				return Colore.BLU;
			case "VERDE":
				return Colore.VERDE;
			case "GIALLO":
				return Colore.GIALLO;
			default:
				return null;
		}
	}
}