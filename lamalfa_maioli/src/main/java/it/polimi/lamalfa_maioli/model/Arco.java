package it.polimi.lamalfa_maioli.model;

import java.io.Serializable;

import org.jgrapht.graph.DefaultEdge;

/**
 * Classe rappresentante un arco del grafo.
 * Connette tra loro due vertici di tipo Vertex.
 * @author lamalfa_maioli
 *
 */
public class Arco extends DefaultEdge implements Serializable {

	private static final long serialVersionUID = -5968722067693258845L;

	/**
	 * Restituisce il vertice collegato al vertice selezionato
	 * @param v vertice
	 * @return vertice collegato al vertice selezionato 
	 */
	public Vertex getOther(Vertex v) {
		Vertex r = (Vertex)super.getSource();
		if(r.equals(v)) {
			r = (Vertex)super.getTarget();
		}
		return r;
	}
}
