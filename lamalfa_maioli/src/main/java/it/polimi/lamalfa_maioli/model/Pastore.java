package it.polimi.lamalfa_maioli.model;

import java.io.Serializable;

/**
 * Classe rappresentate il pastore.
 * Associata direttamente al Giocatore.
 * @author lamalfa_maioli
 *
 */
public class Pastore implements Serializable {
	private static final long serialVersionUID = -3730171025265907858L;
	private Casella casella;
	private Giocatore giocatore;
	private int numeroMossa = 0;
	private boolean pastoreMosso = false;
	private boolean terrenoComprato = false;
	private boolean pecoraMossa = false;
	private boolean pecoraAccoppiata = false;
	private boolean haSparato = false;
	private int id;
	
	/**
	 * Costruttore
	 * @param giocatore giocatore associato al pastore
	 */
	public Pastore(Giocatore giocatore, int id) {
		this.giocatore = giocatore;
		this.id = id;
	}
	
	/**
	 * Reimposta lo status del pastore per il turno successivo
	 */
	public void reset() {
		numeroMossa = 0;
		pastoreMosso = false;
		terrenoComprato = false;
		pecoraMossa = false;
		pecoraAccoppiata = false;
		haSparato = false;
	}
	
	/**
	 * Reimposta la casella e lo status del pastore (fix di rete)
	 * @param casella nuova casella
	 */
	public void reset(Casella casella) {
		this.casella = casella;
		reset();
	}
	
	/**
	 * @return casella dove si trova il pastore
	 */
	public Casella getCasella() {
		return casella;
	}
	
	/**
	 * @return giocatore associato al pastore
	 */
	public Giocatore getGiocatore() {
		return giocatore;
	}
	
	/**
	 * Imposta la casella dove si trova il pastore
	 * @param casella casella dove si sposta il pastore
	 */
	public void setCasella(Casella casella) {
		this.casella = casella;
	}

	/**
	 * @return numero di mosse eseguite dal pastore nel turno corrente
	 */
	public int getNumeroMossa() {
		return numeroMossa;
	}

	/**
	 * Imposta il numero di mosse eseguite dal pastore nel turno corrente
	 * @param numeroMossa numero di mosse eseguite nel turno corrente
	 */
	public void setNumeroMossa(int numeroMossa) {
		this.numeroMossa = numeroMossa;
	}

	/**
	 * @return se il pastore è stato mosso in questo turno
	 */
	public boolean isPastoreMosso() {
		return pastoreMosso;
	}
	
	/**
	 * Imposta se il pastore è stato mosso in questo turno e resetta ogni altro stato di azione
	 * @param pastoreMosso se il pastore è stato mosso
	 */
	public void setPastoreMosso(boolean pastoreMosso) {
		this.pastoreMosso = pastoreMosso;
		this.haSparato = false;
		this.terrenoComprato = false;
		this.pecoraMossa = false;
		this.pecoraAccoppiata = false;
	}

	/**
	 * @return se il pastore ha comprato un terreno in questo turno
	 */
	public boolean isTerrenoComprato() {
		return terrenoComprato;
	}

	/**
	 * Imposta se il pastore ha comprato un terreno in questo turno
	 * @param terrenoComprato se il pastore ha acquistato un terreno  in questo turno
	 */
	public void setTerrenoComprato(boolean terrenoComprato) {
		this.terrenoComprato = terrenoComprato;
	}

	/**
	 * @return se il pastore ha mosso una pecora in questo turno
	 */
	public boolean isPecoraMossa() {
		return pecoraMossa;
	}

	/**
	 * Imposta se il pastore ha mosso una pecora in questo turno
	 * @param pecoraMossa se il pastore ha mosso una pecora in questo turno
	 */
	public void setPecoraMossa(boolean pecoraMossa) {
		this.pecoraMossa = pecoraMossa;
	}

	/**
	 * Extra rule: accoppiamento
	 * @return se il pastore ha fatto accoppiare una pecora e un ariete in questo turno
	 */
	public boolean isPecoraAccoppiata() {
		return pecoraAccoppiata;
	}

	/**
	 * Extra rule: accoppiamento
	 * Imposta e il pastore ha fatto accoppiare una pecora e un ariete in questo turno
	 * @param pecoraAccoppiata se il pastore ha eseguito un accoppiamento in questo turno
	 */
	public void setPecoraAccoppiata(boolean pecoraAccoppiata) {
		this.pecoraAccoppiata = pecoraAccoppiata;
	}

	/**
	 * Extra rule: sparatoria
	 * @return se il pastore ha sparato in questo turno
	 */
	public boolean isHaSparato() {
		return haSparato;
	}

	/**
	 * Extra rule: sparatoria
	 * Imposta se il pastore ha sparato in questo turno
	 * @param haSparato  se il pastore ha sparato in questo turno
	 */
	public void setHaSparato(boolean haSparato) {
		this.haSparato = haSparato;
	}
	
	/**
	 * @see Object#equals(Object)
	 */
	@Override
	public boolean equals(Object object) {
		if(object.getClass() != this.getClass()) {
			return false;
		}
		Pastore tmp = (Pastore) object;
		if(!tmp.getGiocatore().equals(giocatore)) {
			return false;
		}
		if(tmp.getId() != id) {
			return false;
		}
		return true;
	}
	
	/**
	 * @see Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return 1337+giocatore.hashCode()+id;
	}
	
	/**
	 * @return id del pastore
	 */
	public int getId() {
		return id;
	}
}
