package it.polimi.lamalfa_maioli.model;

import it.polimi.lamalfa_maioli.controller.exceptions.MapSyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe rappresentate una regione.
 * E' una tipologia di vertice del grafo.
 * @author lamalfa_maioli
 *
 */
public class Regione implements Vertex {
	private static final long serialVersionUID = 8791813507050755185L;
	private int id;
	private Terreno type;
	private int pecore = 1;
	private int arieti = 1;
	private List<Agnello> agnelli = new ArrayList<>();
	private boolean pecoraNera = false;
	private boolean lupo = false;
	
	/**
	 * Costruttore.
	 * @param id identificativo univoco della regione
	 * @param type tipologia della regione
	 * @throws MapSyntaxException 
	 */
	public Regione(int id, int type) throws MapSyntaxException {
		this.id = id;
		switch(type) {
			case 0:
				this.type = Terreno.PASCOLO;
				break;
			case 1:
				this.type = Terreno.FORESTA;
				break;
			case 2:
				this.type = Terreno.PALUDE;
				break;
			case 3:
				this.type = Terreno.SPIAGGIA;
				break;
			case 4:
				this.type = Terreno.MONTAGNA;
				break;
			case 5:
				this.type = Terreno.COLLINA;
				break;
			case 6:
				this.type = Terreno.SHEEPSBURG;
				break;
			default:
				throw new MapSyntaxException("Regione non definita");
		}
	}
	
	/**
	 * @return tipo della regione
	 */
	public Terreno getType() {
		return type;
	}
	
	/**
	 * @return identificativo univoco della regione
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * @see Object#toString()
	 */
	@Override
	public String toString() {
		return "Regione("+id+"): "+type;
	}
	
	/**
	 * @see Vertex#getClassString()
	 */
	@Override
	public String getClassString() {
		return "Regione";
	}

	/**
	 * @return numero di pecore presenti nella regione
	 */
	public int getPecore() {
		return pecore;
	}

	/**
	 * Imposta il numero di pecore presenti nella regione
	 * @param pecore numero di pecore presenti nella regione
	 */
	public void setPecore(int pecore) {
		this.pecore = pecore;
	}
	
	/**
	 * @return se la regione ha la pecora nera al suo interno
	 */
	public boolean hasPecoraNera() {
		return pecoraNera;
	}
	
	/**
	 * Imposta se la regione ha la pecora nera al suo interno
	 * @param pecoraNera se la regione ha la pecora nera al suo interno
	 */
	public void setPecoraNera(boolean pecoraNera) {
		this.pecoraNera = pecoraNera;
	}

	/**
	 * Extra rule: lupo
	 * @return se la regione ha il lupo al suo interno.
	 */
	public boolean hasLupo() {
		return lupo;
	}

	/**
	 * Imposta se la regione ha il lupo al suo interno.s
	 * @param lupo se la regione ha il lupo al suo interno.
	 */
	public void setLupo(boolean lupo) {
		this.lupo = lupo;
	}

	/**
	 * Extra rule: accoppiamento
	 * @return numero di arieti presenti nella regione
	 */
	public int getArieti() {
		return arieti;
	}

	/**
	 * Extra rule: accoppiamento
	 * @param arieti numero di arieti presenti nella regione
	 */
	public void setArieti(int arieti) {
		this.arieti = arieti;
	}
	
	/**
	 * Extra rule: accoppiamento
	 * @return ArrayList degli agnelli presenti nella regione
	 */
	public List<Agnello> getAgnelli() {
		return agnelli;
	}
	
	/**
	 * @see Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return 1337+id;
	}

	/**
	 * @see Object#equals()
	 */
	@Override
	public boolean equals(Object v) {
		if(getClass() != v.getClass()) {
			return false;
		}
		Regione r = (Regione)v;
		if(r.getId() != id) {
			return false;
		}
		return true;
	}	
	
}