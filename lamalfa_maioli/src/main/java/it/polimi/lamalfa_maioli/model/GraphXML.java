package it.polimi.lamalfa_maioli.model;

import it.polimi.lamalfa_maioli.controller.exceptions.MapSyntaxException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.jgrapht.UndirectedGraph;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Classe che compone il grafo a partire dalla mappa in xml.
 * @author lamalfa_maioli
 *
 */
public class GraphXML implements Serializable {
	private static final long serialVersionUID = -3492607566215503585L;
	private List<Casella> caselleTotali = new ArrayList<>();
	private List<Regione> regioniTotali = new ArrayList<>();
	private UndirectedGraph<Vertex, Arco> grafo = new MyGraph<>();
	
	
	/**
	 * Costruttore
	 * @param fileName nome del file contenente la mappa in xml
	 * @throws MapSyntaxException errore della mappa nel file xml
	 */
	public GraphXML(String fileName) throws MapSyntaxException {
		Document xml;
		try {
			xml = openFile(fileName);
			parseElements(xml, TipoXML.CASELLA, TipoXML.CASELLA.getContainerName(), TipoXML.CASELLA.getElementName(), TipoXML.CASELLA.getElementData());
			parseElements(xml, TipoXML.REGIONE, TipoXML.REGIONE.getContainerName(), TipoXML.REGIONE.getElementName(), TipoXML.REGIONE.getElementData());
			parseElements(xml, TipoXML.CONNESSIONE, TipoXML.CONNESSIONE.getContainerName(), TipoXML.CONNESSIONE.getElementName(), TipoXML.CONNESSIONE.getElementData());
			List<Regione> tmp = trovaRegioni(Terreno.SHEEPSBURG);
			if(tmp.isEmpty()) {
				throw new MapSyntaxException("Errore nella mappa: è necessario inserire la regione di tipo Sheepsburg!");
			} else if(tmp.size() > 1) {
				throw new MapSyntaxException("Errore nella mappa: può esistere solo una regione di tipo Sheepsburg!");
			}
		} catch (ParserConfigurationException e) {
			throw new MapSyntaxException("Impossibile inizializzare il document builder:", e);
		} catch (SAXException e) {
			throw new MapSyntaxException("Impossibile eseguire parse() sul document builder:", e);
		} catch (FileNotFoundException e) {
			throw new MapSyntaxException("Impossibile trovare il file mappa.xml:", e);
		} catch (IOException e) {
			throw new MapSyntaxException("Errore di Input:", e);
		}
	}
	
	/**
	 * @param id identificativo univoco della casella da trovare 
	 * @return casella con identificativo = id
	 */
	public final Vertex trovaCasella(int id) {
		for(int i = 0; i < caselleTotali.size(); i++) {
			if(caselleTotali.get(i).getId() == id) {
				return caselleTotali.get(i);
			}
		}
		return null;
	}
	
	/**
	 * @param id identificativo univoco della regione da trovare
	 * @return regione con identificativo = id
	 */
	public final Vertex trovaRegione(int id) {
		for(int i = 0; i < regioniTotali.size(); i++) {
			if(regioniTotali.get(i).getId() == id) {
				return regioniTotali.get(i);
			}
		}
		return null;
	}
	
	/**
	 * @return ArrayList di tutti i vertici del grafo (sia caselle che regioni)
	 */
	public List<Vertex> allVertex() {
		List<Vertex> tmp = new ArrayList<>();
		
		for(int i = 0; i < caselleTotali.size(); i++) {
			tmp.add(caselleTotali.get(i));
		}
		
		for(int i = 0; i < regioniTotali.size(); i++) {
			tmp.add(regioniTotali.get(i));
		}
		
		return tmp;
	}
	
	/**
	 * @return ArrayList di tutti i vertici di tipo casella
	 */
	public List<Casella> allCasella() {
		return caselleTotali;
	}
	
	/**
	 * @return ArrayList di tutti i vertici di tipo regione
	 */
	public final List<Regione> allRegione() {
		return regioniTotali;
	}
	
	/**
	 * @return restituisce il grafo
	 */
	public UndirectedGraph<Vertex, Arco> getGraph() {
		return grafo;
	}
	
	/**
	 * Apre e imposta per la scansione degli elementi un documento in xml
	 * @param fileName nome del file da aprire
	 * @return contenuto del documento xml
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 */
	private Document openFile(String fileName) throws ParserConfigurationException, SAXException, IOException {
		InputStream mappa = GraphXML.class.getResourceAsStream(fileName);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document xml = dBuilder.parse(mappa);
		xml.getDocumentElement().normalize();
		return xml;
	}
	
	/**
	 * Scansiona tutti gli elementi del documento xml, cercandone una tipologia specifica.
	 * Trovati gli elementi li aggiunge al grafo, differenziandoli tra vertici (casella o regione) o archi.
	 * La scansione viene effettuata in due cicli, per garantire una maggiore compatibilità: se vengono creati più contenitori di elementi, non ho perdita di dati.
	 * Es:
	 * <contenitore><elemento ...></elemento><elemento ...></elemento></contenitore>
	 * <contenitore>elemento ...></elemento></contenitore>
	 * Vengono individuati e inseriti nel grafo tutti e 3 gli elementi, anche se posizionati in due contenitori differenti, ma con lo stesso nome. 
	 * @param xml documento xml
	 * @param elementType tipo di elementi da trovare
	 * @param containerName nome del contenitore xml dei singoli elementi
	 * @param elementName nome del singolo elemento xml
	 * @param elementData Array degli attributi dei singoli elementi xml
	 * @throws MapSyntaxException errore di sintassi della mappa nel file xml
	 */
	private void parseElements(Document xml, TipoXML elementType, String containerName, String elementName, String[] elementData) throws MapSyntaxException {
		NodeList listaContainer = xml.getElementsByTagName(containerName);
		for(int i = 0; i < listaContainer.getLength(); i++) {
			NodeList lista = ((Element)listaContainer.item(i)).getElementsByTagName(elementName);
			for(int j = 0; j < lista.getLength(); j++) {
				Element elemento = (Element)lista.item(j);
				parseElement(elemento, elementType, elementData);
			}
		}
	}
	
	/**
	 * Verifica e inserisce nel grafo un singolo elemento
	 * @param elemento elemento nel quale cercare
	 * @param elementType tipo di elemento da trovare
	 * @param elementData Array degli attributi dei singoli elementi xml
	 * @throws MapSyntaxException  errore di sintassi della mappa nel file xml
	 */
	private void parseElement(Element elemento, TipoXML elementType, String[] elementData) throws MapSyntaxException {
		if(elementType.equals(TipoXML.CONNESSIONE)) {			
			NodeList childs = elemento.getElementsByTagName(elementData[0]);
			Vertex[] link = new Vertex[2] ;
			for(int k = 0; k < childs.getLength(); k++) {
				Element currentElement = (Element)childs.item(k);
				int id = Integer.parseInt(currentElement.getAttribute(elementData[1]));
				String type = currentElement.getAttribute(elementData[2]);
				link[k] = type.equals(elementData[3]) ? trovaCasella(id) : (type.equals(elementData[4]) ? trovaRegione(id) : null);
			}
			checkConnessione(link);
			grafo.addEdge(link[0], link[1]);
		} else {
			int[] element = {
					Integer.parseInt(elemento.getAttribute(elementData[0])),
					Integer.parseInt(elemento.getAttribute(elementData[1]))
			};

			if(elementType.equals(TipoXML.CASELLA)) {
				checkCasella(element[0],element[1]);
				Casella vertice = new Casella(element[0],element[1]);
				grafo.addVertex(vertice);
				caselleTotali.add(vertice);
			} else if(elementType.equals(TipoXML.REGIONE)) {
				checkRegione(element[0],element[1]);
				Regione vertice = new Regione(element[0],element[1]);
				grafo.addVertex(vertice);
				regioniTotali.add(vertice);
			}
		}
	}
	
	/**
	 * Ricerca i vertici adiacenti al vertice corrente (sia regioni, che caselle) 
	 * @param vertice vertice di cui voglio individuare i vertici adiacenti
	 * @return ArrayList dei vertici adiacenti al vertice selezionato (sia regioni, che caselle)
	 */
	public List<Vertex> nearBy(Vertex vertice) {
		List<Vertex> tmp = new ArrayList<>();
		Object[] nearbyElements = grafo.edgesOf(vertice).toArray();
		for(int i = 0; i < nearbyElements.length; i++) {
			Arco vertex = (Arco)nearbyElements[i];
			tmp.add(vertex.getOther(vertice));
		}
		return tmp;
	}
	
	/**
	 * Restituisce la casella presente nella regione con valore = value
	 * @param regione regione di cui voglio trovare la casella specifica
	 * @param value valore della casella da trovare
	 * @return casella confinante con regione il cui valore è uguale a value
	 */
	public Casella casellaNearBy(Regione regione, int value) {
		List<Casella> nearby = casellaNearBy(regione);
		for(int i = 0; i < nearby.size(); i++) {
			Casella tmp = nearby.get(i);
			if(tmp.getValue() == value) {
				return tmp;
			}
		}
		return null;
	}
	
	/**
	 * Restituisce le caselle che fanno parte della regione
	 * @param regione regione di cui voglio individuare le caselle adiacenti
	 * @return ArrayList delle caselle adiacenti alla casella selezionata
	 */
	public List<Casella> casellaNearBy(Regione regione) {
		List<Casella> tmp = new ArrayList<>();
		List<Vertex> nearby = nearBy(regione);
		for(int i = 0; i < nearby.size(); i++) {
			if("Casella".equals(nearby.get(i).getClassString())) {
				tmp.add((Casella)nearby.get(i));
			}
		}
		return tmp;
	}
	
	/**
	 * Restituisce le caselle adiacenti a quella considerata
	 * @param casella casella di cui voglio individuare le caselle adiacenti
	 * @return ArrayList delle caselle adiacenti alla casella selezionata
	 */
	public List<Casella> casellaNearBy(Casella casella) {
		List<Casella> tmp = new ArrayList<>();
		List<Vertex> nearby = nearBy(casella);
		for(int i = 0; i < nearby.size(); i++) {
			if("Casella".equals(nearby.get(i).getClassString())) {
				tmp.add((Casella)nearby.get(i));
			}
		}
		return tmp;
	}
	
	/**
	 * Restituisce la regione confinante con la regione selezionata rispetto alla casella selezionata
	 * @param casella casella
	 * @param regione regione
	 * @return regione confinante con la regione selezionata rispetto alla casella selezionata
	 */
	public Regione regioneNearBy(Casella casella, Regione regione) {
		List<Regione> nearby = regioneNearBy(casella);
		for(int i = 0; i < nearby.size(); i++) {
			Regione tmp = nearby.get(i);
			if(!tmp.equals(regione)) {
				return tmp;
			}
		}
		return null;
	}
	
	/**
	 * Restituisce le regioni confinanti rispetto alla casella considerata
	 * @param casella casella di cui voglio individuare le regioni che connette
	 * @return ArrayList delle regioni confinanti rispetto alla casella selezionata
	 */
	public List<Regione> regioneNearBy(Casella casella) {
		List<Regione> tmp = new ArrayList<>();
		List<Vertex> nearby = nearBy(casella);
		for(int i = 0; i < nearby.size(); i++) {
			if("Regione".equals(nearby.get(i).getClassString())) {
				tmp.add((Regione)nearby.get(i));
			}
		}
		return tmp;
	}
	
	/**
	 * Verifica se due vertici sono adiacenti tra loro
	 * @param vertice1 vertice
	 * @param vertice2 vertice 
	 * @return se i due vertici sono adiacenti
	 */
	public boolean areNear(Vertex vertice1, Vertex vertice2) {
		List<Vertex> tmp = nearBy(vertice1);
		return tmp.contains(vertice2);
	}

	/**
	 * Verifica se due regioni sono confinanti rispetto alla casella data
	 * @param regione1 regione
	 * @param regione2 regione
	 * @param casella casella 
	 * @return se le due regioni sono confinanti rispetto alla casella data
	 */
	public boolean areNear(Regione regione1, Regione regione2, Casella casella) {
		List<Vertex> tmp = nearBy(casella);
		return tmp.contains(regione1) && tmp.contains(regione2);
	}
	/**
	 * @see Object#toString()
	 */
	@Override
	public String toString() {
		String tmp = "Regioni:\n";
		for(int i = 0; i < regioniTotali.size(); i++) {
			Regione current = regioniTotali.get(i);
			tmp += "Regione #"+current.getId()+" ("+current.getType()+")\n";
		}
		tmp += "\nCaselle:\n";
		for(int i = 0; i < caselleTotali.size(); i++) {
			Casella current = caselleTotali.get(i);
			tmp += "Casella #"+current.getId()+": "+current.getValue()+"\n";
		}
		tmp += "\nConnessioni caselle:\n";
		for(int i = 0; i < caselleTotali.size(); i++) {
			Casella current = caselleTotali.get(i);
			tmp += "Casella #"+current.getId()+": ";
			List<Casella> conn = casellaNearBy(current);
			for(int j = 0; j < conn.size(); j++) {
				tmp += "Casella #"+conn.get(j).getId()+(((j+1) < conn.size()) ? ", " : "\n");
			}
		}
		tmp += "\nConnessioni regioni:\n";
		for(int i = 0; i < caselleTotali.size(); i++) {
			Casella current = caselleTotali.get(i);
			tmp += "Casella #"+current.getId()+": ";
			List<Regione> conn = regioneNearBy(current);
			for(int j = 0; j < conn.size(); j++) {
				tmp += "Regione #"+conn.get(j).getId()+(((j+1) < conn.size()) ? ", " : "\n");
			}
		}
		return tmp;
	}
	
	/**
	 * Controlla se la regione ha un tipo valido e se ha un identificativo valido
	 * @param id identificativo univoco regione
	 * @param type tipo regione in intero
	 * @return se la regione ha un tipo valido
	 * @throws MapSyntaxException errore di sintassi della mappa nel file xml
	 */
	private void checkRegione(int id, int type) throws MapSyntaxException {
		if(id < 0) {
			throw new MapSyntaxException("ID non valido per Regione! ("+id+" < 0)");
		}
		if(trovaRegione(id) != null) {
			throw new MapSyntaxException("ID non valido per Regione! ("+id+" già esistente)");
		}
		for(Terreno tipo : Terreno.class.getEnumConstants()) {
			if(tipo.getId() == type) {
				return;
			}
		}
		throw new MapSyntaxException("Tipo Terreno sbagliato per Regione! ("+type+")");
	}
	
	/**
	 * Controlla se la casella ha un valore valido
	 * @param id identificativo univoco casella
	 * @param value valore della casella in intero
	 * @return se la casella ha un tipo valido
	 * @throws MapSyntaxException errore di sintassi della mappa nel file xml
	 */
	private void checkCasella(int id, int value) throws MapSyntaxException {
		if(id < 0) {
			throw new MapSyntaxException("ID non valido per Casella! ("+id+" < 0)");
		}
		if(trovaCasella(id) != null) {
			throw new MapSyntaxException("ID non valido per Casella! ("+id+" già esistente)");
		}
		if(value < 0) {
			throw new MapSyntaxException("Valore sbagliato per Casella! ("+value+")");
		}
	}
	
	/**
	 * Verifica che la connessione non sia tra due regione, e che non ci sia un vertice nullo
	 * @param vertice Array di Vertex contenente i vertici della connessione
	 * @throws MapSyntaxException errore di sintassi della mappa nel file xml
	 */
	private void checkConnessione(Vertex[] vertice) throws MapSyntaxException {
		int regioneCount = 0;
		for(int i = 0; i < vertice.length; i++) {
			if("Regione".equals(vertice[i].getClassString())) {
				regioneCount++;
			} else if(vertice[i] == null) {
				throw new MapSyntaxException("I vertici della connessione non possono essere nulli!");
			}
		}
		if(regioneCount > 1) {
			throw new MapSyntaxException("La connessione può essere solo tra due caselle o tra una casella e una regione!");
		}
	}
	
	/**
	 * Restituisce tutte le regioni di un dato tipo di terreno
	 * @param terreno tipo di terreno da trovare
	 * @return regioni del tipo di terreno selezionato
	 */
	public final List<Regione> trovaRegioni(Terreno terreno) {
		List<Regione> all = allRegione();
		List<Regione> found = new ArrayList<>();
		for(int i = 0; i < all.size(); i++) {
			Regione tmp = all.get(i);
			if(terreno.equals(tmp.getType())) {
				found.add(tmp);
			}
		}
		return found;
	}
	
	/**
	 * @return il numero totale delle regioni
	 */
	public int numeroRegioni() {
		return regioniTotali.size();
	}

}
