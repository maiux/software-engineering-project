package it.polimi.lamalfa_maioli.model;

import java.io.Serializable;


/**
 * Classe rappresentante un agnello.
 * Dopo 2 turni di vita diventerà una pecora o un ariete.
 * @author lamalfa_maioli
 *
 */
public class Agnello implements Serializable {
	private static final long serialVersionUID = 7424828926847378753L;
	private int turnoCreazione;
	private Giocatore giocatore;
	private Regione regione;

	/**
	 * Costruttore
	 * @param turnoCreazione turno in cui l'agnello è stato creato
	 * @param giocatore giocatore che ha creato l'agnello
	 * @param regione regione in cui è l'agnello
	 */
	public Agnello(int turnoCreazione, Giocatore giocatore, Regione regione) {
		this.turnoCreazione = turnoCreazione;
		this.giocatore = giocatore;
		this.regione = regione;
	}

	/**
	 * 
	 * @return turnoCrazione turno in cui l'agnello è stato creato
	 */
	public int getAge() {
		return turnoCreazione;
	}
	
	/**
	 * @return giocatore giocatore che ha creato l'agnello, serve a verificare i turni effettivi di vita
	 */
	public Giocatore getGiocatore() {
		return giocatore;
	}

	/**
	 * @return regione in cui si trova l'agnello
	 */
	public Regione getRegione() {
		return regione;
	}
}
