package it.polimi.lamalfa_maioli.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe rappresentante un giocatore.
 * @author lamalfa_maioli
 *
 */
public class Giocatore implements Serializable {
	private static final long serialVersionUID = 8779540480082832281L;
	private List<Pastore> pastori = new ArrayList<>();
	private Pastore pastoreCorrente;
	private int monete = 0;
	private Map<Terreno, Integer> terreniComprati = new HashMap<>();
	private List<OggettoVendita> terreniVendita = new ArrayList<>();
	private Colore colore;
	
	/**
	 * Costruttore
	 * @param monete monete iniziali del giocatore
	 * @param colore colore scelto dal giocatore
	 */
	public Giocatore(int monete, Colore colore) {
		this.monete = monete;
		this.colore = colore;
	}
	
	/**
	 * Imposta il colore del giocatre
	 * @param colore colore
	 */
	public void setColore(Colore colore) {
		this.colore = colore;
	}
	
	/**
	 * Aggiorna un dato pastore presente nell'arraylist pastori
	 * @param pastore
	 */
	public void updatePastore(Pastore pastore) {
		for(int i = 0; i < pastori.size(); i++) {
			if(pastori.get(i).equals(pastore)) {
				pastori.set(i, pastore);
				return;
			}
		}
	}
	
	/**
	 * Extra rule: compravendita
	 * @return terreni messi in vendita dal il giocatore
	 */
	public List<OggettoVendita> getTerreniVendita() {
		return terreniVendita;
	}
	
	/**
	 * Popola la mappa con tutte le tipologie di terreni, e imposta a 1 il numero di terreni del tipo di quello iniziale.
	 * @param terrenoIniziale terreno iniziale del giocatore
	 */
	public void setTerrenoIniziale(Terreno terrenoIniziale) {
		for(Terreno tipo : Terreno.class.getEnumConstants()) {
			terreniComprati.put(tipo, 0);
		}
		terreniComprati.put(terrenoIniziale, 1);
	}
	
	/**
	 * @return numero di monete che ha il giocatore
	 */
	public int getMonete() {
		return monete;
	}

	/**
	 * Imposta il numero di monete del giocatore
	 * @param monete numero di monete del giocatore
	 */
	public void setMonete(int monete) {
		this.monete = monete;
	}
	
	/**
	 * @return colore del giocatore
	 */
	public Colore getColore() {
		return colore;
	}

	/**
	 * In partite a due giocatori, ognuno controlla due pastori.
	 * @return i pastori controllati dal giocatore
	 */
	public List<Pastore> getPastori() {
		return pastori;
	}
	
	/**
	 * In partite a due giocatori, ognuno controlla due pastori.
	 * @return pastore pastore selezionato dal giocatore per il turno corrente
	 */
	public Pastore getPastoreCorrente() {
		return pastoreCorrente;
	}
	
	
	/**
	 * In partite a due giocatori, ognuno controlla due pastori.
	 * @param pastoreCorrente imposta il pastore selezionato dal giocatore da utilizzare per il turno corrente
	 */
	public boolean setPastoreCorrente(Pastore pastoreCorrente) {
		if(pastoreCorrente == null) {
			this.pastoreCorrente = null;
			return true;
		} else if(this.equals(pastoreCorrente.getGiocatore())) {
			this.pastoreCorrente = pastoreCorrente;
			return true;
		}
		return false;
	}

	/**
	 * @return tipo e numero dei terreni comprati dal giocatore 
	 */
	public Map<Terreno, Integer> getTerreniComprati() {
		return terreniComprati;
	}
	
	/**
	 * Aggiunge o rimuove un numero di terreni a/da quelli posseduti dal giocatore.
	 * @param terreno tipo di terreno da incrementare/decrementare.
	 * @param num incrementatore/decrementatore
	 */
	public void addTerreno(Terreno terreno, int num) {
		terreniComprati.put(terreno, terreniComprati.get(terreno) + num);
	}

	/**
	 * @see Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((colore == null) ? 0 : colore.hashCode());
		result = prime * result + monete;
		result = prime * result	+ ((pastoreCorrente == null) ? 0 : pastoreCorrente.hashCode());
		result = prime * result + ((pastori == null) ? 0 : pastori.hashCode());
		result = prime * result + ((terreniComprati == null) ? 0 : terreniComprati.hashCode());
		result = prime * result + ((terreniVendita == null) ? 0 : terreniVendita.hashCode());
		return result;
	}

	/**
	 * @see Object#equals()
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		Giocatore other = (Giocatore) obj;
		if(other.getColore().equals(colore)) {
			return true;
		}
		return false;
	}
}
